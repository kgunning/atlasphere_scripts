%
function [iau_s06result]=IAU_S06(date1,date2,x,y)

iau_s06result=[];
persistent a das2r dj00 djc fa firstCall i j ks0 ks1 ks2 ks3 ks4 ns0 ns1 ns2 ns3 ns4 nsp s0 s1 s2 s3 s4 s5 sp ss0 ss1 ss2 ss3 ss4 t ; if isempty(firstCall),firstCall=1;end; 

;
%+
%  - - - - - - - -
%   i a u _ S 0 6
%  - - - - - - - -
%
%  The CIO locator s, positioning the Celestial Intermediate Origin on
%  the equator of the Celestial Intermediate Pole, given the CIP's X,Y
%  coordinates.  Compatible with IAU 2006/2000A precession-nutation.
%
%  This routine is part of the International Astronomical Union's
%  SOFA (Standards of Fundamental Astronomy) software collection.
%
%  Status:  canonical model.
%
%  Given:
%     DATE1,DATE2    d      TT as a 2-part Julian Date (Note 1)
%     X,Y            d      CIP coordinates (Note 3)
%
%  Returned:
%     iau_S06        d      the CIO locator s in radians (Note 2)
%
%  Notes:
%
%  1) The TT date DATE1+DATE2 is a Julian Date, apportioned in any
%     convenient way between the two arguments.  For example,
%     JD(TT)=2450123.7 could be expressed in any of these ways,
%     among others:
%
%            DATE1          DATE2
%
%         2450123.7D0        0D0        (JD method)
%          2451545D0      -1421.3D0     (J2000 method)
%         2400000.5D0     50123.2D0     (MJD method)
%         2450123.5D0       0.2D0       (date & time method)
%
%     The JD method is the most natural and convenient to use in
%     cases where the loss of several decimal digits of resolution
%     is acceptable.  The J2000 method is best matched to the way
%     the argument is handled internally and will deliver the
%     optimum resolution.  The MJD method and the date & time methods
%     are both good compromises between resolution and convenience.
%
%  2) The CIO locator s is the difference between the right ascensions
%     of the same point in two systems:  the two systems are the GCRS
%     and the CIP,CIO, and the point is the ascending node of the
%     CIP equator.  The quantity s remains below 0.1 arcsecond
%     throughout 1900-2100.
%
%  3) The series used to compute s is in fact for s+XY/2, where X and Y
%     are the x and y components of the CIP unit vector;  this series is
%     more compact than a direct series for s would be.  This routine
%     requires X,Y to be supplied by the caller, who is responsible for
%     providing values that are consistent with the supplied date.
%
%  4) The model is consistent with the 'P03' precession (Capitaine et
%     al. 2003), adopted by IAU 2006 Resolution 1, 2006, and the
%     IAU 2000A nutation (with P03 adjustments).
%
%  Called:
%     iau_FAL03    mean anomaly of the Moon
%     iau_FALP03   mean anomaly of the Sun
%     iau_FAF03    mean argument of the latitude of the Moon
%     iau_FAD03    mean elongation of the Moon from the Sun
%     iau_FAOM03   mean longitude of the Moon's ascending node
%     iau_FAVE03   mean longitude of Venus
%     iau_FAE03    mean longitude of Earth
%     iau_FAPA03   general accumulated precession in longitude
%
%  References:
%
%     Capitaine, N., Wallace, P.T. & Chapront, J., 2003, Astron.
%     Astrophys. 432, 355
%
%     McCarthy, D.D., Petit, G. (eds.) 2004, IERS Conventions (2003),
%     IERS Technical Note No. 32, BKG
%
%  This revision:   2009 December 15
%
%  SOFA release 2019-07-22
%
%  Copyright (C) 2019 IAU SOFA Board.  See notes at end.
%
%-----------------------------------------------------------------------



% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

%  Arcseconds to radians
 if isempty(das2r), das2r=4.848136811095359935899141d-6 ; end;

%  Reference epoch (J2000.0), JD
 if isempty(dj00), dj00=2451545d0 ; end;

%  Days per Julian century
 if isempty(djc), djc=36525d0 ; end;

%  Time since J2000.0, in Julian centuries
 if isempty(t), t=0; end;

%  Miscellaneous
 if isempty(i), i=0; end;
 if isempty(j), j=0; end;
 if isempty(a), a=0; end;
 if isempty(s0), s0=0; end;
 if isempty(s1), s1=0; end;
 if isempty(s2), s2=0; end;
 if isempty(s3), s3=0; end;
 if isempty(s4), s4=0; end;
 if isempty(s5), s5=0; end;

%  Fundamental arguments
 if isempty(fa), fa=zeros(1,8); end;

%  ---------------------
%  The series for s+XY/2
%  ---------------------

%  Number of terms in the series
 if isempty(nsp), nsp=6; end;
 if isempty(ns0), ns0=33; end;
 if isempty(ns1), ns1=3; end;
 if isempty(ns2), ns2=25; end;
 if isempty(ns3), ns3=4; end;
 if isempty(ns4), ns4=1 ; end;

%  Polynomial coefficients

%  Coefficients of l,l',F,D,Om,LVe,LE,pA

%  Sine and cosine coefficients

%  Polynomial coefficients
sp=[94d-6,3808.65d-6,-122.68d-6,-72574.11d-6,27.98d-6,15.62d-6];

%  Argument coefficients for t^0
[ ks0([1:8],[1:10])]=reshape([0,0,0,0,1,0,0,0,0,0,0,0,2,0,0,0,0,0,2,-2,3,0,0,0,0,0,2,-2,1,0,0,0,0,0,2,-2,2,0,0,0,0,0,2,0,3,0,0,0,0,0,2,0,1,0,0,0,0,0,0,0,3,0,0,0,0,1,0,0,1,0,0,0,0,1,0,0,-1,0,0,0],8,10);
[ ks0([1:8],[11:20])]=reshape([1,0,0,0,-1,0,0,0,1,0,0,0,1,0,0,0,0,1,2,-2,3,0,0,0,0,1,2,-2,1,0,0,0,0,0,4,-4,4,0,0,0,0,0,1,-1,1,-8,12,0,0,0,2,0,0,0,0,0,0,0,2,0,2,0,0,0,1,0,2,0,3,0,0,0,1,0,2,0,1,0,0,0],8,10);
[ ks0([1:8],[21:30])]=reshape([0,0,2,-2,0,0,0,0,0,1,-2,2,-3,0,0,0,0,1,-2,2,-1,0,0,0,0,0,0,0,0,8,-13,-1,0,0,0,2,0,0,0,0,2,0,-2,0,-1,0,0,0,0,1,2,-2,2,0,0,0,1,0,0,-2,1,0,0,0,1,0,0,-2,-1,0,0,0,0,0,4,-2,4,0,0,0],8,10);
[ ks0([1:8],[31:ns0])]=reshape([0,0,2,-2,4,0,0,0,1,0,-2,0,-3,0,0,0,1,0,-2,0,-1,0,0,0],8,3);

%  Argument coefficients for t^1
[ ks1([1:8],[1:ns1])]=reshape([0,0,0,0,2,0,0,0,0,0,0,0,1,0,0,0,0,0,2,-2,3,0,0,0],8,ns1);

%  Argument coefficients for t^2
[ ks2([1:8],[1:10])]=reshape([0,0,0,0,1,0,0,0,0,0,2,-2,2,0,0,0,0,0,2,0,2,0,0,0,0,0,0,0,2,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,2,-2,2,0,0,0,0,0,2,0,1,0,0,0,1,0,2,0,2,0,0,0,0,1,-2,2,-2,0,0,0],8,10);
[ ks2([1:8],[11:20])]=reshape([1,0,0,-2,0,0,0,0,0,0,2,-2,1,0,0,0,1,0,-2,0,-2,0,0,0,0,0,0,2,0,0,0,0,1,0,0,0,1,0,0,0,1,0,-2,-2,-2,0,0,0,1,0,0,0,-1,0,0,0,1,0,2,0,1,0,0,0,2,0,0,-2,0,0,0,0,2,0,-2,0,-1,0,0,0],8,10);
[ ks2([1:8],[21:ns2])]=reshape([0,0,2,2,2,0,0,0,2,0,2,0,2,0,0,0,2,0,0,0,0,0,0,0,1,0,2,-2,2,0,0,0,0,0,2,0,0,0,0,0],8,5);

%  Argument coefficients for t^3
[ ks3([1:8],[1:ns3])]=reshape([0,0,0,0,1,0,0,0,0,0,2,-2,2,0,0,0,0,0,2,0,2,0,0,0,0,0,0,0,2,0,0,0],8,ns3);

%  Argument coefficients for t^4
[ ks4([1:8],[1:ns4])]=reshape([0,0,0,0,1,0,0,0],8,ns4);

%  Sine and cosine coefficients for t^0
[ ss0([1:2],[1:10])]=reshape([-2640.73d-6,+0.39d-6,-63.53d-6,+0.02d-6,-11.75d-6,-0.01d-6,-11.21d-6,-0.01d-6,+4.57d-6,0.00d-6,-2.02d-6,0.00d-6,-1.98d-6,0.00d-6,+1.72d-6,0.00d-6,+1.41d-6,+0.01d-6,+1.26d-6,+0.01d-6],2,10);
[ ss0([1:2],[11:20])]=reshape([+0.63d-6,0.00d-6,+0.63d-6,0.00d-6,-0.46d-6,0.00d-6,-0.45d-6,0.00d-6,-0.36d-6,0.00d-6,+0.24d-6,+0.12d-6,-0.32d-6,0.00d-6,-0.28d-6,0.00d-6,-0.27d-6,0.00d-6,-0.26d-6,0.00d-6],2,10);
[ ss0([1:2],[21:30])]=reshape([+0.21d-6,0.00d-6,-0.19d-6,0.00d-6,-0.18d-6,0.00d-6,+0.10d-6,-0.05d-6,-0.15d-6,0.00d-6,+0.14d-6,0.00d-6,+0.14d-6,0.00d-6,-0.14d-6,0.00d-6,-0.14d-6,0.00d-6,-0.13d-6,0.00d-6],2,10);
[ ss0([1:2],[31:ns0])]=reshape([+0.11d-6,0.00d-6,-0.11d-6,0.00d-6,-0.11d-6,0.00d-6],2,3);

%  Sine and cosine coefficients for t^1
[ ss1([1:2],[1:ns1])]=reshape([-0.07d-6,+3.57d-6,+1.73d-6,-0.03d-6,0.00d-6,+0.48d-6],2,ns1);

%  Sine and cosine coefficients for t^2
 [ ss2([1:2],[1:10])]=reshape([+743.52d-6,-0.17d-6,+56.91d-6,+0.06d-6,+9.84d-6,-0.01d-6,-8.85d-6,+0.01d-6,-6.38d-6,-0.05d-6,-3.07d-6,0.00d-6,+2.23d-6,0.00d-6,+1.67d-6,0.00d-6,+1.30d-6,0.00d-6,+0.93d-6,0.00d-6],2,10); 
 [ ss2([1:2],[11:20])]=reshape([+0.68d-6,0.00d-6,-0.55d-6,0.00d-6,+0.53d-6,0.00d-6,-0.27d-6,0.00d-6,-0.27d-6,0.00d-6,-0.26d-6,0.00d-6,-0.25d-6,0.00d-6,+0.22d-6,0.00d-6,-0.21d-6,0.00d-6,+0.20d-6,0.00d-6],2,10); 
 [ ss2([1:2],[21:ns2])]=reshape([+0.17d-6,0.00d-6,+0.13d-6,0.00d-6,-0.13d-6,0.00d-6,-0.12d-6,0.00d-6,-0.11d-6,0.00d-6],2,5); 
 
%  Sine and cosine coefficients for t^3
[ ss3([1:2],[1:ns3])]=reshape([+0.30d-6,-23.42d-6,-0.03d-6,-1.46d-6,-0.01d-6,-0.25d-6,0.00d-6,+0.23d-6],2,ns3);

%  Sine and cosine coefficients for t^4
[ ss4([1:2],[1:ns4])]=reshape([-0.26d-6,-0.01d-6],2,ns4);
firstCall=0;

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

%  Interval between fundamental epoch J2000.0 and current date (JC).
t =((date1-dj00)+date2)./djc;

%  Fundamental Arguments (from IERS Conventions 2003)

%  Mean anomaly of the Moon.
fa(1) = IAU_FAL03(t);

%  Mean anomaly of the Sun.
fa(2) = IAU_FALP03(t);

%  Mean longitude of the Moon minus that of the ascending node.
fa(3) = IAU_FAF03(t);

%  Mean elongation of the Moon from the Sun.
fa(4) = IAU_FAD03(t);

%  Mean longitude of the ascending node of the Moon.
fa(5) = IAU_FAOM03(t);

%  Mean longitude of Venus.
fa(6) = IAU_FAVE03(t);

%  Mean longitude of Earth.
fa(7) = IAU_FAE03(t);

%  General precession in longitude.
fa(8) = IAU_FAPA03(t);

%  Evaluate s.
s0 = sp(1);
s1 = sp(2);
s2 = sp(3);
s3 = sp(4);
s4 = sp(5);
s5 = sp(6);

for i = ns0 : -1: 1 ;
a = 0d0;
for j = 1 : 8;
a = a + real(ks0(j,i)).*fa(j);
end; j = fix(8+1);
s0 = s0 +(ss0(1,i).*sin(a)+ss0(2,i).*cos(a));
end; i = fix(1 + -1);

for i = ns1 : -1: 1 ;
a = 0d0;
for j = 1 : 8;
a = a + real(ks1(j,i)).*fa(j);
end; j = fix(8+1);
s1 = s1 +(ss1(1,i).*sin(a)+ss1(2,i).*cos(a));
end; i = fix(1 + -1);

for i = ns2 : -1: 1 ;
a = 0d0;
for j = 1 : 8;
a = a + real(ks2(j,i)).*fa(j);
end; j = fix(8+1);
s2 = s2 +(ss2(1,i).*sin(a)+ss2(2,i).*cos(a));
end; i = fix(1 + -1);

for i = ns3 : -1: 1 ;
a = 0d0;
for j = 1 : 8;
a = a + real(ks3(j,i)).*fa(j);
end; j = fix(8+1);
s3 = s3 +(ss3(1,i).*sin(a)+ss3(2,i).*cos(a));
end; i = fix(1 + -1);

for i = ns4 : -1: 1 ;
a = 0d0;
for j = 1 : 8;
a = a + real(ks4(j,i)).*fa(j);
end; j = fix(8+1);
s4 = s4 +(ss4(1,i).*sin(a)+ss4(2,i).*cos(a));
end; i = fix(1 + -1);

iau_s06result =(s0+(s1+(s2+(s3+(s4+s5.*t).*t).*t).*t).*t).*das2r - x.*y./2d0;

%  Finished.

%+----------------------------------------------------------------------
%
%  Copyright (C) 2019
%  Standards Of Fundamental Astronomy Board
%  of the International Astronomical Union.
%
%  =====================
%  SOFA Software License
%  =====================
%
%  NOTICE TO USER:
%
%  BY USING THIS SOFTWARE YOU ACCEPT THE FOLLOWING SIX TERMS AND
%  CONDITIONS WHICH APPLY TO ITS USE.
%
%  1. The Software is owned by the IAU SOFA Board ('SOFA').
%
%  2. Permission is granted to anyone to use the SOFA software for any
%     purpose, including commercial applications, free of charge and
%     without payment of royalties, subject to the conditions and
%     restrictions listed below.
%
%  3. You (the user) may copy and distribute SOFA source code to others,
%     and use and adapt its code and algorithms in your own software,
%     on a world-wide, royalty-free basis.  That portion of your
%     distribution that does not consist of intact and unchanged copies
%     of SOFA source code files is a 'derived work' that must comply
%     with the following requirements:
%
%     a) Your work shall be marked or carry a statement that it
%        (i) uses routines and computations derived by you from
%        software provided by SOFA under license to you; and
%        (ii) does not itself constitute software provided by and/or
%        endorsed by SOFA.
%
%     b) The source code of your derived work must contain descriptions
%        of how the derived work is based upon, contains and/or differs
%        from the original SOFA software.
%
%     c) The names of all routines in your derived work shall not
%        include the prefix 'iau' or 'sofa' or trivial modifications
%        thereof such as changes of case.
%
%     d) The origin of the SOFA components of your derived work must
%        not be misrepresented;  you must not claim that you wrote the
%        original software, nor file a patent application for SOFA
%        software or algorithms embedded in the SOFA software.
%
%     e) These requirements must be reproduced intact in any source
%        distribution and shall apply to anyone to whom you have
%        granted a further right to modify the source code of your
%        derived work.
%
%     Note that, as originally distributed, the SOFA software is
%     intended to be a definitive implementation of the IAU standards,
%     and consequently third-party modifications are discouraged.  All
%     variations, no matter how minor, must be explicitly marked as
%     such, as explained above.
%
%  4. You shall not cause the SOFA software to be brought into
%     disrepute, either by misuse, or use for inappropriate tasks, or
%     by inappropriate modification.
%
%  5. The SOFA software is provided 'as is' and SOFA makes no warranty
%     as to its use or performance.   SOFA does not and cannot warrant
%     the performance or results which the user may obtain by using the
%     SOFA software.  SOFA makes no warranties, express or implied, as
%     to non-infringement of third party rights, merchantability, or
%     fitness for any particular purpose.  In no event will SOFA be
%     liable to the user for any consequential, incidental, or special
%     damages, including any lost profits or lost savings, even if a
%     SOFA representative has been advised of such damages, or for any
%     claim by any third party.
%
%  6. The provision of any version of the SOFA software under the terms
%     and conditions specified herein does not imply that future
%     versions will also be made available under the same terms and
%     conditions.
%
%  In any published work or commercial product which uses the SOFA
%  software directly, acknowledgement (see www.iausofa.org) is
%  appreciated.
%
%  Correspondence concerning SOFA software should be addressed as
%  follows:
%
%      By email:  sofa@ukho.gov.uk
%      By post:   IAU SOFA Center
%                 HM Nautical Almanac Office
%                 UK Hydrographic Office
%                 Admiralty Way, Taunton
%                 Somerset, TA1 2DN
%                 United Kingdom
%
%-----------------------------------------------------------------------

end

