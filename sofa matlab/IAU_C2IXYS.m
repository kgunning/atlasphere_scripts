%
function rc2i = IAU_C2IXYS(x,y,s)
%+
%  - - - - - - - - - - -
%   i a u _ C 2 I X Y S
%  - - - - - - - - - - -
%
%  Form the celestial to intermediate-frame-of-date matrix given the CIP
%  X,Y and the CIO locator s.
%
%  This routine is part of the International Astronomical Union's
%  SOFA (Standards of Fundamental Astronomy) software collection.
%
%  Status:  support routine.
%
%  Given:
%     X,Y        d       Celestial Intermediate Pole (Note 1)
%     S          d       the CIO locator s (Note 2)
%
%  Returned:
%     RC2I     d(3,3)    celestial-to-intermediate matrix (Note 3)
%
%  Notes:
%
%  1) The Celestial Intermediate Pole coordinates are the x,y components
%     of the unit vector in the Geocentric Celestial Reference System.
%
%  2) The CIO locator s (in radians) positions the Celestial
%     Intermediate Origin on the equator of the CIP.
%
%  3) The matrix RC2I is the first stage in the transformation from
%     celestial to terrestrial coordinates:
%
%        [TRS]  =  RPOM * R_3(ERA) * RC2I * [CRS]
%
%               =  RC2T * [CRS]
%
%     where [CRS] is a vector in the Geocentric Celestial Reference
%     System and [TRS] is a vector in the International Terrestrial
%     Reference System (see IERS Conventions 2003), ERA is the Earth
%     Rotation Angle and RPOM is the polar motion matrix.
%
%  Called:
%     iau_IR       initialize r-matrix to identity
%     iau_RZ       rotate around Z-axis
%     iau_RY       rotate around Y-axis
%
%  Reference:
%
%     McCarthy, D. D., Petit, G. (eds.), IERS Conventions (2003),
%     IERS Technical Note No. 32, BKG (2004)
%
%  This revision:  2014 November 7
%
%  SOFA release 2019-07-22
%
%  Copyright (C) 2019 IAU SOFA Board.  See notes at end.
%
%-----------------------------------------------------------------------



persistent d e r2 ; 

% rc2i_orig=rc2i;rc2i_shape=[3,3];rc2i=reshape([rc2i_orig(1:min(prod(rc2i_shape),numel(rc2i_orig))),zeros(1,max(0,prod(rc2i_shape)-numel(rc2i_orig)))],rc2i_shape);
% rc2i = zeros(3,3);

 if isempty(r2), r2=0; end;
 if isempty(e), e=0; end;
 if isempty(d), d=0; end;

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

%  Obtain the spherical angles E and d.
r2 = x.*x + y.*y;
if( r2>0d0 );
e = atan2(y,x);
else;
e = 0d0;
end;
d = atan(sqrt(r2./(1d0-r2)));

%  Form the matrix.
rc2i = eye(3);
[e,rc2i]=IAU_RZ(e,rc2i);
[d,rc2i]=IAU_RY(d,rc2i);
[~,rc2i]=IAU_RZ(-(e+s),rc2i);

%  Finished.

%+----------------------------------------------------------------------
%
%  Copyright (C) 2019
%  Standards Of Fundamental Astronomy Board
%  of the International Astronomical Union.
%
%  =====================
%  SOFA Software License
%  =====================
%
%  NOTICE TO USER:
%
%  BY USING THIS SOFTWARE YOU ACCEPT THE FOLLOWING SIX TERMS AND
%  CONDITIONS WHICH APPLY TO ITS USE.
%
%  1. The Software is owned by the IAU SOFA Board ('SOFA').
%
%  2. Permission is granted to anyone to use the SOFA software for any
%     purpose, including commercial applications, free of charge and
%     without payment of royalties, subject to the conditions and
%     restrictions listed below.
%
%  3. You (the user) may copy and distribute SOFA source code to others,
%     and use and adapt its code and algorithms in your own software,
%     on a world-wide, royalty-free basis.  That portion of your
%     distribution that does not consist of intact and unchanged copies
%     of SOFA source code files is a 'derived work' that must comply
%     with the following requirements:
%
%     a) Your work shall be marked or carry a statement that it
%        (i) uses routines and computations derived by you from
%        software provided by SOFA under license to you; and
%        (ii) does not itself constitute software provided by and/or
%        endorsed by SOFA.
%
%     b) The source code of your derived work must contain descriptions
%        of how the derived work is based upon, contains and/or differs
%        from the original SOFA software.
%
%     c) The names of all routines in your derived work shall not
%        include the prefix 'iau' or 'sofa' or trivial modifications
%        thereof such as changes of case.
%
%     d) The origin of the SOFA components of your derived work must
%        not be misrepresented;  you must not claim that you wrote the
%        original software, nor file a patent application for SOFA
%        software or algorithms embedded in the SOFA software.
%
%     e) These requirements must be reproduced intact in any source
%        distribution and shall apply to anyone to whom you have
%        granted a further right to modify the source code of your
%        derived work.
%
%     Note that, as originally distributed, the SOFA software is
%     intended to be a definitive implementation of the IAU standards,
%     and consequently third-party modifications are discouraged.  All
%     variations, no matter how minor, must be explicitly marked as
%     such, as explained above.
%
%  4. You shall not cause the SOFA software to be brought into
%     disrepute, either by misuse, or use for inappropriate tasks, or
%     by inappropriate modification.
%
%  5. The SOFA software is provided 'as is' and SOFA makes no warranty
%     as to its use or performance.   SOFA does not and cannot warrant
%     the performance or results which the user may obtain by using the
%     SOFA software.  SOFA makes no warranties, express or implied, as
%     to non-infringement of third party rights, merchantability, or
%     fitness for any particular purpose.  In no event will SOFA be
%     liable to the user for any consequential, incidental, or special
%     damages, including any lost profits or lost savings, even if a
%     SOFA representative has been advised of such damages, or for any
%     claim by any third party.
%
%  6. The provision of any version of the SOFA software under the terms
%     and conditions specified herein does not imply that future
%     versions will also be made available under the same terms and
%     conditions.
%
%  In any published work or commercial product which uses the SOFA
%  software directly, acknowledgement (see www.iausofa.org) is
%  appreciated.
%
%  Correspondence concerning SOFA software should be addressed as
%  follows:
%
%      By email:  sofa@ukho.gov.uk
%      By post:   IAU SOFA Center
%                 HM Nautical Almanac Office
%                 UK Hydrographic Office
%                 Admiralty Way, Taunton
%                 Somerset, TA1 2DN
%                 United Kingdom
%
%-----------------------------------------------------------------------

end

