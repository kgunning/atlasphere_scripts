% script for running the estimator with precomputed measurements

%% Step 3: Add Estimator(s)
%
%   For this example there will be only a single estimator present whos job
%   is to estimate the state of the satellite at all times.
%
%   See the estimator class for a lot more detail on what is going on here

% using Kaz's format here for passing parameters to the filter

% Measurement sigmas
PARAMS.sigRange = 0.01; % std dev
PARAMS.sigDopp  = 0.01; % std dev
PARAMS.sigBearing = 10e-6*180/pi; % std dev radians

% Initial uncertainties
PARAMS.sigPos0 = 10; % m std dev
PARAMS.sigVel0 = 1; % m/s std dev
PARAMS.sigAtt0 = 1; % deg std dev

% Process noise sigmas
PARAMS.qPos = 0.01;  % m std dev
PARAMS.qVel = 0.001; % m/s std dev
PARAMS.qAtt = 0.01;  % deg std dev

% for this example, the initial state and covariance as the initial
% position and velocity of the satellite and this covariance
%
% create the estimator given these terms
% TODO: pass in the sensor model to use...
% TODO: work out how best to pass in the sensor model to use
estimator = atlas.filter.KazSimple(t(1),world, PARAMS);


%% Step 3: Simulate!
%
%   In this example, the measurement data will not be pre-computed,
%   everything will be done in one shot

% before we can do the full simulation, we actually need to build out
% simulator...

% assemble the simulation desired
% NOTE: this wrapper is very much in development and is really aimed at
% being able to quickly run a parfor loop across many different simulations
% (see example code a few lines down)
%
%   ideally this will allow specifying additional models, etc to allow for
%   more flexibility and all that good stuff.

% limit the number of time steps
indsUse = 1:length(t);
tShort = t(indsUse);
sim = atlas.Simulation(tShort, world, allSensors, ...
                          'Estimator', estimator, ...
                          'MeasurementData', measurementData(indsUse));


% run the sim -> and that's it!
%
% this steps through every time step and calls the Estimator's `simStep()`
% function at every time step, allowing for you to change up what should
% happen in the main simulation loop.  (Additionally the sim handles the
% getting of measurements at each time step, etc).
sim.run();

%% plot results- very simple for now
satEstUniqueID = unique(sim.Estimator.stateUniqueID);

satPlot = 1;

estPosEci = sim.StateHistory(sim.Estimator.stateUniqueID == satEstUniqueID(satPlot) ...
    & sim.Estimator.stateType == sim.Estimator.PositionStateTypeID,:);
estVelEci = sim.StateHistory(sim.Estimator.stateUniqueID == satEstUniqueID(satPlot) ...
    & sim.Estimator.stateType == sim.Estimator.VelocityStateTypeID,:);

estPosEcef = nan(size(estPosEci));
estVelEcef = nan(size(estPosEci));
truePos = nan(size(estPosEcef));
trueVel = nan(size(estPosEcef));

% convert estimated satellite state to ecef
for idx = 1:length(tShort)
    [estPosEcef(:,idx), estVelEcef(:,idx)] = atlas.tools.eci2ecef(t(idx),estPosEci(:,idx),estVelEci(:,idx));
    
    trueStatei = sim.World.getState(t(idx), satEstUniqueID(satPlot) , {'Position', 'Velocity'});

    truePos(:,idx) = trueStatei.Position;
    trueVel(:,idx) = trueStatei.Velocity;
end

estPos =  estPosEcef;
estVel = estVelEcef;

dPos = estPos-truePos;
dVel = estVel-trueVel;

%%
figure(1); clf; hold on;
plot3(truePos(1,:),truePos(2,:),truePos(3,:))
plot3(estPos(1,:),estPos(2,:),estPos(3,:))
axis equal

figure(2); clf; hold on;
plot(dPos')
































