%% 


alfa = [3 4 5]*pi/180;

R1 = [1 0 0;
    0 cos(alfa(1)) sin(alfa(1));
    0 -sin(alfa(1)) cos(alfa(1))];

R2 = [cos(alfa(2)) 0 -sin(alfa(2));
    0 1 0;
    sin(alfa(2)) 0 cos(alfa(2))];

R3 = [cos(alfa(3)) sin(alfa(3)) 0;
    -sin(alfa(3)) cos(alfa(3)) 0;
    0 0 1];

R = R1*R2*R3;



% convert to euler angles
[phi,theta,psi] = atlas.tools.dcm2euler123(R);

alfa2 = [phi theta psi]*180/pi;

R2 = atlas.tools.euler2dcm123(phi,theta,psi);






