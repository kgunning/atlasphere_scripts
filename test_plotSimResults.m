%

% filenameSim = 'C:\Users\kazgu\Desktop\sim0.mat';
% filenameSim = 'C:\Users\Kaz\Documents\atlasphere data\sim0_18sats_1orbit.mat';
% 
% if ~exist('sim','var')
%     data = load(filenameSim);
%     
%     sim = data.sim;
%     
%     clear data
% end

estimator = sim.Estimator;

epochs = sim.Time;

% Rotate the position error to RAC domain
nSats = length(estimator.refTrajUniqueID);
nEpochs = length(epochs);

posEcef = nan(nSats,nEpochs,3);
errEcef = nan(nSats,nEpochs,3);
errRac  = nan(nSats,nEpochs,3);
stdEcef = nan(nSats,nEpochs,3);
stdRac  = nan(nSats,nEpochs,3);


satEstUniqueID = unique(sim.Estimator.refTrajUniqueID);

% Loop through satellites
h = waitbar(0,'pct complete');
nSatsMax = nSats;
for sdx = 1   :nSatsMax 
    estPosEci = sim.StateHistory(sim.Estimator.stateUniqueID == satEstUniqueID(sdx) ...
        & sim.Estimator.stateType == sim.Estimator.PositionStateTypeID,:);
    estVelEci = sim.StateHistory(sim.Estimator.stateUniqueID == satEstUniqueID(sdx) ...
        & sim.Estimator.stateType == sim.Estimator.VelocityStateTypeID,:);
    estR_body_eci = sim.StateHistory(sim.Estimator.stateUniqueID == satEstUniqueID(sdx) ...
        & sim.Estimator.stateType == sim.Estimator.AttitudeStateTypeID,:);
    
    covPosEci = sim.CovarianceHistory(sim.Estimator.stateUniqueID == satEstUniqueID(sdx) ...
        & sim.Estimator.stateType == sim.Estimator.PositionStateTypeID,sim.Estimator.stateUniqueID == satEstUniqueID(sdx) ...
        & sim.Estimator.stateType == sim.Estimator.PositionStateTypeID,:);
    
    covVelEci = sim.CovarianceHistory(sim.Estimator.stateUniqueID == satEstUniqueID(sdx) ...
        & sim.Estimator.stateType == sim.Estimator.VelocityStateTypeID,sim.Estimator.stateUniqueID == satEstUniqueID(sdx) ...
        & sim.Estimator.stateType == sim.Estimator.VelocityStateTypeID,:);
    
    covAtt = sim.CovarianceHistory(sim.Estimator.stateUniqueID == satEstUniqueID(sdx) ...
        & sim.Estimator.stateType == sim.Estimator.AttitudeStateTypeID,sim.Estimator.stateUniqueID == satEstUniqueID(sdx) ...
        & sim.Estimator.stateType == sim.Estimator.AttitudeStateTypeID,:);
        
    % convert estimated satellite state to ecef
    for tdx = 1:nEpochs
        [estPosEcefi, estVelEcefi,R_eci_ecef] = atlas.tools.eci2ecef(epochs(tdx),estPosEci(:,tdx),estVelEci(:,tdx));
        
%         covPosEcefi = R_eci_ecef*squeeze(covPosEci(:,:,tdx))*R_eci_ecef';
%         covVelEcefi = R_eci_ecef*squeeze(covVelEci(:,:,tdx))*R_eci_ecef';
        
        trueStatei = sim.World.getState(epochs(tdx), satEstUniqueID(sdx) , {'Position', 'Velocity','Attitude'});
        
        posEcefi = trueStatei.Position;
        velEcefi = trueStatei.Velocity;
        
        % Attitude
        % Estimated body to eci DCM
        R_est_eci = atlas.tools.euler2dcm123(estR_body_eci(1,tdx),estR_body_eci(2,tdx),estR_body_eci(3,tdx));
        R_est_ecef = R_eci_ecef*R_est_eci;
        
        R_true_ecef = atlas.tools.q2dcm(trueStatei.Attitude);
        
        R_true_est = R_true_ecef'*R_est_ecef;
        
        attErri = nan(3,1);
        [attErri(1),attErri(2),attErr(3)] = atlas.tools.dcm2euler123(R_true_est);
        
        
        % RAC position error
        R_ecef_rac = atlas.tools.ecef2rac(posEcefi,velEcefi);
        
        errEcefi = estPosEcefi-posEcefi;
        
        errPosRaci  = R_ecef_rac*errEcefi;
%         covPosRaci  = R_ecef_rac*covPosEcefi*R_ecef_rac';
        
        
        % Save values
        posEcef(sdx,tdx,:) = posEcefi;
        errEcef(sdx,tdx,:) = estPosEcefi-posEcefi;
%         stdEcef(sdx,tdx,:) = sqrt(diag(covPosEcefi));
        errRac(sdx,tdx,:)  = errPosRaci;
%         stdRac(sdx,tdx,:)  = sqrt(diag(covPosRaci));
        
    end
    
    waitbar(sdx/nSatsMax,h);
        
end

close(h);


%%
% normErr = errEcef./stdEcef;

% hist(reshape(normErr(:,:,3),size(normErr,1)*size(normErr,2),1),100)



%%
close all;
f1 = figure; clf; hold on;
titles = {'Radial','Along-Track','Cross-Track'};
xlims = 12;
yMax = 0;
stds = nan(3,1);
pct95 = nan(3,1);
for idx = 1:3
    subplot(1,6,(idx*2-1):(idx*2));
%     subplot(1,3,idx);
    errRaci = errRac(:,1:5000,idx);
    
    stds(idx) = std(errRaci(:));
    
    pct95(idx) = prctile(abs(errRaci(:)),95);
    hist(errRaci(:),-xlims:0.1:xlims);
    xlim([-xlims xlims])
    axlims = axis;
    yMax = max([yMax axlims(4)]);
end
f1.Position = [496   352   967   420];
for idx = 1:3
     subplot(1,6,(idx*2-1):(idx*2));
     if idx == 1
        ylabel('Counts')
     end
     ylim([0 yMax])
%      yticklabels({''})    
     title(titles{idx})
     xlabel('Error [m]')
     
     text(-xlims*0.9,yMax*0.9,['\sigma      = ' num2str(stds(idx),'%02.2f') ' m'])
     text(-xlims*0.9,yMax*0.85,['95% = ' num2str(pct95(idx),'%02.2f') ' m'])
end

























