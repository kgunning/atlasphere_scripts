%% Developing ECI2ECEF (and ECEF2ECI) for real data

if ~exist('datai','var')
    filename = 'C:\Users\Kaz\Documents\atlasphere data\gmat\rpt\ReportFile_PokeSat_00257_full.rpt';
    
    datai = importdata(filename);
    
    posTrueEci = datai.data(:,11:13)*1000;
    velTrueEci = datai.data(:,14:16)*1000;
    
    posTrueEcef = datai.data(:,1:3)*1000;
    velTrueEcef = datai.data(:,4:6)*1000;
    
    t = datenum(datai.textdata(2:end));
end

Nepochs = length(t);

pos0 = posTrueEci(1,:)';
vel0 = velTrueEci(1,:)';
%% Propagate

if 1
    [t2, satPosProp, satVelProp]  = simpleOrbitProp(pos0,vel0,round(t*86400));  % ECI data here!
    satPosProp = squeeze(satPosProp)';
    satVelProp = squeeze(satVelProp)';
else
    satPosProp = nan(size(posTrueEci));
    satVelProp = nan(size(velTrueEci));
    for tdx = 2:100
%         [t2, satPosPropi, satVelPropi]  = simpleOrbitProp(posTrueEci(tdx-1,:)',velTrueEci(tdx-1,:)',floor([t(tdx-1) mean([t(tdx-1) t(tdx)]) t(tdx)]*86400));  % ECI data here!
        [t2, satPosPropi, satVelPropi]  = simpleOrbitProp(posTrueEci(tdx-1,:)',velTrueEci(tdx-1,:)',round([t(tdx-1) t(tdx)]*86400));  % ECI data here!
        
        satPosProp(tdx,:) = satPosPropi(:,:,end);
        satVelProp(tdx,:) = satVelPropi(:,:,end);
    end
end

%% Propagate with STM
[t3,satPosStm,satVelStm] = orbitPropStm(pos0,vel0,round(t*86400));

satPosStm = squeeze(satPosStm)';
satVelStm = squeeze(satVelStm)';

%% Compare
dPos = satPosProp-posTrueEci;
dVel = satVelProp-velTrueEci;

dPosStm = satPosStm-posTrueEci;
dVelStm = satVelStm-velTrueEci;

% Rotate to RAC
dPosRac = nan(size(dPos));
dVelRac = nan(size(dPos));
dPosRacStm = nan(size(dPos));
dVelRacStm = nan(size(dPos));
for idx = 1:Nepochs
    R_global_rac = atlas.tools.ecef2rac(satPosProp(idx,:)',satVelProp(idx,:)');
    
    
    dPosRac(idx,:) = R_global_rac*dPos(idx,:)';
    dVelRac(idx,:) = R_global_rac*dVel(idx,:)';
    
    dPosRacStm(idx,:) = R_global_rac*dPosStm(idx,:)';
    dVelRacStm(idx,:) = R_global_rac*dVelStm(idx,:)';
end





figure(1); clf;
subplot(1,2,1); hold on;
plot(dPosRac,'.')
plot(dPosRacStm,'.');
subplot(1,2,2); hold on;
plot(dVelRac,'.')
plot(dVelRacStm,'.');

% xlim([0 100])
legend('Radial','Cross-track','Along-track')



%%
figure(2); clf; hold on;
plot3(satPosProp(:,1),satPosProp(:,2),satPosProp(:,3),'.')
plot3(posTrueEci(:,1),posTrueEci(:,2),posTrueEci(:,3),'.')


%% Just pull accelerations
if 0
    accModeled = nan(size(posTrueEci));
    
    t2 = t(1):(0.001/86400):t(end);
    
    accModeled = nan(length(t2),3);
    for tdx = 2:10000
        ti = t2(tdx)*86400;
        statei = [posTrueEci(tdx,:)'; velTrueEci(tdx,:)'];
        
        dstate = atlas.internal.orbDiffEq(ti,statei);
        
        accModeled(tdx,:) = dstate(4:6);
        %                 [t2, satPosPropi, satVelPropi]  = simpleOrbitProp(posTrueEci(tdx-1,:)',velTrueEci(tdx-1,:)',[t(tdx-1) t(tdx)]*86400);  % ECI data here!
        
    end
    
    figure(3); clf; hold on
    plot(diff(diff(diff(accModeled))),'.')
    
    
    
end








