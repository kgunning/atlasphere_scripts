% Developing ECI2ECEF (and ECEF2ECI) for real data


if ~exist('sats','var')
    dirnameEcef = 'C:\Users\kazgu\Documents\atlasphere data\ECEF\';
    satsEcef = atlas.tools.importGMATData(dirnameEcef);
    
    posTrueEcef = satsEcef.TruthData.Position.Data;
    velTrueEcef = satsEcef.TruthData.Velocity.Data;
    t = satsEcef.TruthData.Position.Time;
    
    
    dirnameEci = 'C:\Users\kazgu\Documents\atlasphere data\ECI\';
    satsEci = atlas.tools.importGMATData(dirnameEci);
    
    posTrueEci = satsEci.TruthData.Position.Data;
    velTrueEci = satsEci.TruthData.Velocity.Data;
    t = satsEci.TruthData.Position.Time;
end


Nepochs = length(t);


%%
if 0
    figure(1); clf; hold on;
    plot3(posTrueEcef(:,1),posTrueEcef(:,2),posTrueEcef(:,3))
    plot3(posTrueEci(:,1),posTrueEci(:,2),posTrueEci(:,3))
end

filenameSofa = 'C:\Users\kazgu\Documents\atlasphere_scripts\sofa matlab\224_EOP_C04_14.62-NOW.IAU2000A224.txt';

% ok, now try to do the rotation lol
jd = juliandate(datetime(t,'ConvertFrom','posixtime'));

[iy,im,id,ih,min,sec] = datevec(datetime(t,'ConvertFrom','posixtime'));

posRotEci = nan(size(posTrueEcef));
velRotEci = nan(size(posTrueEcef));
for idx = 1:60;%length(jd)
    R = ECI2ECEFSOFA(iy(idx),im(idx),id(idx),ih(idx),min(idx),sec(idx),filenameSofa);
    posRotEci(idx,:) = R'*posTrueEcef(idx,:)';
end
% [posRotEci, velRotEci] = atlas.internal.ecef2eciMICE(t,posTrueEcef,velTrueEcef);


%%


%Compare
dposEci = posRotEci-posTrueEci;
dvelEci = velRotEci-velTrueEci;

if 1
    figure(1); clf; hold on;
        plot3(posRotEci(:,1),posRotEci(:,2),posRotEci(:,3))
        plot3(posTrueEci(:,1),posTrueEci(:,2),posTrueEci(:,3))
%     plot(dposEci,'.')
    %     ylim([-10 10])
    
%     figure(2); clf; hold on;
%     plot(dvelEci,'.')
end