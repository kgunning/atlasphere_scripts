function R = ECI2ECEFSOFA(iy,im,id,ih,min,sec,filenameSofa)



% constants
AS2R = 4.848136811095359935899141D-6;
d2pi=6.283185307179586476925287d0 ;
% pi2dd2pi=6.283185307179586476925287d0 ;eg = 180/pi;

% polar motion- where do these come from?
% xp = 0.0349282D0 * AS2R;
% yp = 0.4833163D0 * AS2R;


% ut1-utc
% dut1 = -0.072073685D0;

% CIP OFFSETS WRT iau 2000A
% DX00 =  0.1725D0 * AS2R/1000D0;
% DY00 = -0.2650D0 * AS2R/1000D0;

% CIP offsets wrt IAU 2006/2000A
% DX06 =  0.1750D0 * AS2R/1000D0;
% DY06 = -0.2259D0 * AS2R/1000D0;

[djm0,date]=IAU_CAL2JD(iy,im,id);

time = ( 60D0*(60D0*ih + min) + sec ) / 86400D0;

utc = date+time;

%%
% load IERS EOP file
% filenamexpyp = 'C:\Users\kazgu\Documents\atlasphere_scripts\sofa matlab\224_EOP_C04_14.62-NOW.IAU2000A224.txt';
dataxpyp = importdata(filenameSofa,' ',14);
jdEop = dataxpyp.data(:,4);
xpEop = dataxpyp.data(:,5);
ypEop = dataxpyp.data(:,6);
dut1Eop = dataxpyp.data(:,7);
dxpEop  = dataxpyp.data(:,9);
dypEop  = dataxpyp.data(:,10);

xp = interp1(jdEop,xpEop,date);
yp = interp1(jdEop,ypEop,date);

% ut1-utc
dut1 = interp1(jdEop,dut1Eop,date);

% CIP offsets wrt IAU 2006/2000A
DX06 = interp1(jdEop,dxpEop,date)* AS2R;
DY06 = interp1(jdEop,dypEop,date)* AS2R;
%%

[dat] = IAU_DAT(iy,im,id,time);

dat = dat+2;

tai = utc+dat/86400;
tt = tai+32.184/86400;

tut = time+dut1/86400;
ut1 = date+tut;



%%
% CIP and CIO, IAU 2006/2000A
[x,y] = IAU_XY06(djm0,tt);
s = IAU_S06(djm0,tt,x,y);

% Add CIP corrections
x = x+DX06;
y = y+DY06;

% GCRS to CIRS matrix
RC2I = IAU_C2IXYS(x,y,s);

% Earth rotation angle
era = IAU_ERA00(djm0+date,tut);

% era*360/d2pi

% Form celestrial-terrestrial matrix (no polar motion yet)
RC2TI = IAU_CR(RC2I);
[~,RC2TI] = IAU_RZ(era,RC2TI);

% Polar motion matrix (TIRS -> ITRS, IERS 2003)
rpom = IAU_POM00(xp,yp,IAU_SP00(djm0,tt));

% Celestrial-terrestrial matrix (including polar motion)
R = rpom*RC2TI;












end