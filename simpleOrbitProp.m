function [epochs, pos, vel] = simpleOrbitProp(pos0,vel0,epochs)
% generateSimpleCircleOrbit     Helper function to create the orbit data
% for a simple circle orbit as used in Kaz's simple POD example.
%   altitude is in [km]
%
%   NOTE: This is a temporary function until such a time that it's final
%   resting place is decided -> I'm thinking there is going to be a
%   propagators part to this package, but still TBD


% Initial satellite state
% posTruth = [atlas.constants.EarthConstants.R+altitude 0 0]';
% velTruth = [0 0 sqrt(atlas.constants.EarthConstants.mu/norm(posTruth))]';

stateTruth = [pos0; vel0];

% Simulation time steps
% period = 2*pi*sqrt(norm(posTruth).^3./atlas.constants.EarthConstants.mu);
% Nepochs = floor(period);

% build the vector of epochs for which to calculate the position
dt = epochs(2)-epochs(1);


% NOTE: in Kaz's sample code, dt for the integrator was the same as the dt
% used in the epochs vector
dtInt = 1;

% contains for the data
pos = zeros(3, 1, length(epochs));
vel = zeros(3, 1, length(epochs));

pos(:,:,1) = pos0;
vel(:,:,1) = vel0;

% loop through all the time steps to propagate the orbit and compute the
% orbit data for all the time steps
if 1
    options = odeset('AbsTol',1e-6,'RelTol',1e-7);
    
    [t,posVel] = ode45(@atlas.internal.orbDiffEq,epochs,stateTruth,options);
    
    pos(:,1,:) = permute(posVel(:,1:3),[2 3 1]);
    vel(:,1,:) = permute(posVel(:,4:6),[2 3 1]);
else
    
    
    prevState = stateTruth;
    for i = 2:length(epochs)
        
        currentState = propagateOrbit(epochs(i),dt, dtInt, prevState);
        pos(:,:,i) = currentState(1:3);
        vel(:,:,i) = currentState(4:6);
        
        prevState = currentState;
    end
end




end




function state = propagateOrbit(t,dt, dtInt, state)

% dt is large time step
% dtInt is shorter integrator time step
% state is [pos; vel] in ECI
state0 = state;
lol = [];
% run the RK4 from 0 to dt in dtInt length time steps
nSteps = dt./dtInt;
for idx = 1:nSteps
    % rk4 for each time step
    k1 = dtInt * atlas.internal.orbDiffEq(t+(idx-1)*dt,state);
    k2 = dtInt * atlas.internal.orbDiffEq(t+(idx-1)*dt+dt/4,state + k1/2);
    k3 = dtInt * atlas.internal.orbDiffEq(t+(idx-1)*dt+2*dt/4,state + k2/2);
    k4 = dtInt * atlas.internal.orbDiffEq(t+(idx-1)*dt+3*dt/4,state + k3);
    state = state + 1/6 *(k1 + 2*k2 + 2*k3 + k4);
    
    lol = [lol k1 k2 k3 k4];
end

end








