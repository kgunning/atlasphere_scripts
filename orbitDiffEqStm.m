function dStmState = orbitDiffEqStm(t,stmState)

dStmState = zeros(size(stmState));

state = stmState(1:6);

dstate = atlas.internal.orbDiffEq(t,state);

dStmState(1:6) = dstate;

A = buildAmat(t,state);

Psi = reshape(stmState(7:end), 6,6);
PsiDot = A*Psi;

dStmState(7:end) = reshape(PsiDot, 36, 1);


end



function A = buildAmat(t,state)

A = zeros(6);

% take partials w.r.t. each state 
dx = 1e-1;

% dstatePert = zeros(6);

dstate = atlas.internal.orbDiffEq(t,state);

for idx = 1:6
    pert = zeros(6,1);
    pert(idx) = dx;
    A(:,idx) = (atlas.internal.orbDiffEq(t,state+pert)-dstate)./dx;
    
end

A = A;

% mu = atlas.constants.EarthConstants.mu;
% r = norm(state(1:3));
% 
% x = state(1);
% y = state(2);
% z = state(3);
% A2(1:3,4:6) = eye(3);
% A2(4,1) = mu*(2*x^2-y^2-z^2).*r^-5;
% A2(4,2) = 3*mu*x*y.*r^-5;
% A2(4,3) = 3*mu*x*z.*r^-5;
% 
% 
% A2(5,1) = 3*mu*x*y.*r^-5;
% A2(5,2) = -mu*(x^2-2*y^2+z^2).*r^-5;
% A2(5,3) = 3*mu*y*z.*r^-5;
% 
% A2(6,1) = 3*mu*x*z.*r^-5;
% A2(6,2) = 3*mu*y*z.*r^-5;
% A2(6,3) = -mu*(x^2+y^2-2*z^2).*r^-5;
% 
% A = A2;

end