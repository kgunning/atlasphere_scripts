% script for building the truth data
clear; clc;

%% Step 1: Build the World
%
%   The very first thing that needs to be done is to build out the world
%   that we are interested in simulating.  In this case, the world is not
%   just defined by a set of "entities" that exist in the world, but also
%   by the time in which these "entities" live.
%
%   For this example, the world will contain a single satellite (an
%   "entity") and a single ground station (another "entity") and will exist
%   for the duration of one entire orbit.

% NOTE: this is not what a final version of this would look like, this is
% very temporary to just get this example working -> this step is still in
% development to create more complex networks

%
% generate satellite data
%

% we want to tie those states over time to a specific satellite, so going
% to make sure we id each satellite with plane id and slot id


% parameters that control the generation of the constellation
nSatsPerPlane = 1;
nPlanes = 1;

planeSpacingDeg = 20;
planeSpacingRad = deg2rad(planeSpacingDeg);

alt = 1000 * 1000;  % orbit altitude in [km]

% compute the number of total satellites
Nsats = nSatsPerPlane * nPlanes;

% pre-allocate the output array
sats = atlas.Entity.empty(Nsats, 0);
planeSlot = zeros(nSatsPerPlane*nPlanes,2);

% create every satellite by looping through each plane and each satellite
% within the plane
sdx = 1;  % the index in the full satellite list
for pdx = 1:nPlanes
    for idx = 1:nSatsPerPlane
        
        % for backwards compatibility with Kaz's fso map
        planeSlot(idx+(pdx-1)*nSatsPerPlane,:) = [pdx idx];
        
        % create the satellite - specifying the plane id and the slot id
        sats(sdx) = atlas.Entity.createXonaSatellite(idx, pdx);
        
        % create the orbit for our satellite -> this is a very temporary function!
        if idx == 1 && pdx == 1
            
            % set the initial position and velocity of the satellite
            pos0 = [atlas.constants.EarthConstants.R+alt 0 0]';
            vel0 = [0 0 sqrt(atlas.constants.EarthConstants.mu/norm(pos0))]';
            
        elseif idx == 1 && pdx > 1
            
            % First satellite of new plane - rotate by some amount and offset
            dtOrbitOffset = floor(Nepochs/nSatsPerPlane/2);
            pos0 = squeeze(satPos(:,1,dtOrbitOffset));
            vel0 = squeeze(satVel(:,1,dtOrbitOffset));
            
            % Rotate the position and velocity
            Ri = [cos(planeSpacingRad) -sin(planeSpacingRad) 0;
                sin(planeSpacingRad) cos(planeSpacingRad) 0;
                0 0 1];
            pos0 = Ri * pos0;
            vel0 = Ri * vel0;
        else
            % Offset the previous orbit by some amount
            dtOrbitOffset = floor(Nepochs/nSatsPerPlane);
            pos0 = squeeze(satPos(:,1,dtOrbitOffset));
            vel0 = squeeze(satVel(:,1,dtOrbitOffset));
        end
        
        dt = 1e-4;
        period = 2*pi*sqrt(norm(pos0).^3./atlas.constants.EarthConstants.mu);
        
%         epochs = dt:dt:floor(period);
        epochs = dt:dt:10;
        Nepochs = floor(period);
        
        % propagate
        [t, satPos, satVel] = atlas.internal.generateSimpleCircleOrbit(pos0,vel0,epochs);  % ECI data here!
        
        % need to convert the ECI data to ECEF
        satPosECEF = zeros(length(t), 3);
        satVelECEF = zeros(length(t), 3);
        for i = 1:length(t)
            [satPosECEF(i,:), satVelECEF(i,:)] = atlas.tools.eci2ecef(t(i), satPos(:,1,i), satVel(:,1,i));
        end
        
        % Build super simple satellite attitude for each epoch- z pointed
        % down, x in along-track, y finishes right hand
        q_sat_e = zeros(length(t),4);
        for i = 1:length(t)
            % Build DCM at each epoch
            z = -satPosECEF(i,:)./norm(satPosECEF(i,:));
            x0 = satVelECEF(i,:)./norm(satVelECEF);
            y = cross(z,x0)./norm(cross(z,x0));
            x = cross(y,z);
            
            R_sat_ecef = [x' y' z'];
            q_sat_e(i,:) = atlas.tools.dcm2q(R_sat_ecef);
        end
        
        % create a time series object for the position and the velocity
        satPosTS = atlas.TimeSeries(t, satPosECEF);
        satVelTS = atlas.TimeSeries(t, satVelECEF);
        satAttTS = atlas.TimeSeries(t, q_sat_e);
        satRawState = atlas.statedata.Raw('Position', satPosTS, ...
            'Velocity', satVelTS, ...
            'Attitude',  satAttTS);
        
        % new method needs to add this state data to this specific satellite
        sats(sdx).TruthData = satRawState;
        
        % increment the satellite list index
        sdx = sdx + 1;
    end
end

%
% generate ground station data
%

% basically now do the same thing for the ground station
gsLlh = [39 -177 0]; % deg, deg, m % a

nGroundStation = size(gsLlh,1);
% gs = atlas.Entity.createGroundStation(nGroundStation);
gs = atlas.Entity.empty(nGroundStation, 0);

for idx = 1:nGroundStation
    % create the ground station
    
    gs(idx) = atlas.Entity.createGroundStation(idx);
    
    gsPosECEF = atlas.tools.llh2xyz(gsLlh(idx,:))';
    gsVelECEF = zeros(3,1);
    % Attitude DCM
    % gsLlh = atlas.tools.xyz2llh(gsPosECEF');
    R_gs_e = atlas.tools.findxyz2enu(gsLlh(idx,1)*pi/180,gsLlh(idx,2)*pi/180);
    q_gs_e = atlas.tools.dcm2q(R_gs_e);
    
    % create a time series object for the position and the velocity
    % gsPVASS = atlas.TimeSeries([], [gsPosECEF' gsVelECEF' nan(1, 4)]);
    % gsFullState = atlas.state.FastPVA(gs, gsPVASS);
    
    gsPosTS = atlas.TimeSeries([], gsPosECEF');
    gsVelTS = atlas.TimeSeries([], gsVelECEF');
    gsAttTS = atlas.TimeSeries([], q_gs_e');
    gsRawState = atlas.statedata.Raw('Position', gsPosTS, ...
        'Velocity', gsVelTS, ...
        'Attitude', gsAttTS);
    
    % new method needs to add this state data to this specific satellite
    % gs(idx).TruthData = gsRawState;
    
    gs(idx).TruthData = gsRawState;
end
%
% build out the world
%

% create the world object to be used by others
world = atlas.World([sats gs]);


%%
stdRange = 0.1;

% Create noisy and truth measurements
satPos = sats(1).TruthData.Position.Data;
gsPos  = gs(1).TruthData.Position.Data;

nEpochs = size(satPos,1);

rangeMeasTrue = sqrt(sum((satPos-gsPos).^2,2));
rangeMeasNoise = rangeMeasTrue+stdRange*randn(nEpochs,1);



%%
t = (1:length(rangeMeasTrue))';
% rangeMeasSmooth = nan(size(rangeMeasTrue));

nSamples = 9999;
% Simple moving mean
rangeMeasSmooth1 = movmean(rangeMeasNoise,9999);



rangeMeasSmooth2 = nan(size(rangeMeasTrue));
nPoly = nSamples;
for idx = (ceil(nPoly/2)+1):1:(nEpochs-(ceil(nPoly/2)+1))
    
    indsi = (idx-floor(nPoly/2)):(idx+floor(nPoly/2));
    [p,~,mu]= polyfit(t(indsi),rangeMeasNoise(indsi),2);
    
    rangeMeasSmooth2(idx) = polyval(p,idx,[],mu);
    
end

dRange2 = rangeMeasTrue-rangeMeasSmooth2;
dRange1  = rangeMeasTrue-rangeMeasSmooth1;
dRange0 = rangeMeasTrue-rangeMeasNoise;

%%
figure(1); clf; hold on;
% hist(dRange0,100)
% hist(dRange,1000)

plot(t*dt,100*dRange0,'.','MarkerSize',0.1)
plot(t*dt,100*dRange1,'.','MarkerSize',10)
plot(t*dt,100*dRange2,'.','MarkerSize',10)

legend(['Raw Meas Error, ' num2str(100*rms(dRange0(tSkip:(end-tSkip))),'%2.2f') ' cm RMS'],...
    ['Moving Average Error, ' num2str(100*rms(dRange1(tSkip:(end-tSkip))),'%2.2f') ' cm RMS'],...
    ['2nd Order Poly Error, ' num2str(100*rms(dRange2(tSkip:(end-tSkip))),'%2.2f') ' cm RMS'])
ylim([-25 25])

tSkip = nPoly;
rms(dRange0(tSkip:(end-tSkip)))
rms(dRange1(tSkip:(end-tSkip)))
rms(dRange2(tSkip:(end-tSkip)))
title([num2str(nSamples) ' sample smoothing'])
xlabel('Time [s]')
ylabel('Meas error [cm]')
%%
























