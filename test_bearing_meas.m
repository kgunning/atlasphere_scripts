% bearing measurement model testing

nMeas = 100000;

sigMeas = 1e-6;

% create azimuth and elevation measurements
azTrue = 2*pi*rand(nMeas,1);
elTrue = pi/2*rand(nMeas,1);

rad2deg = 180/pi;

% Plot true measurements
if 0
    figure(1); clf; hold on;
    plot(azTrue*rad2deg,elTrue*rad2deg,'.')
    xlabel('Azimuth')
    ylabel('Elevation')
end

% Add noise
azNoise = azTrue+sigMeas*randn(nMeas,1);
elNoise = elTrue+sigMeas*randn(nMeas,1);

azMeas = nan(size(azNoise));
elMeas = nan(size(elNoise));
for idx = 1:nMeas
    [elMeas(idx),azMeas(idx)] = atlas.tools.findElAz(atlas.tools.elAz2Los(elNoise(idx),azNoise(idx))'); 
end

%% Take the difference
dEl = elMeas-elTrue;
dAz = azMeas-azTrue;
dAz(dAz < -3) = dAz(dAz < -3) + 2*pi;


fig2 = figure(2); clf;
fig2.Position(3) = 1240;
fig2.Position(4) = 420;
subplot(1,2,1);
hist(dEl,100)
subplot(1,2,2);
hist(dAz,100)

figure(3); clf; 
qqplot(dAz)












