% script for running the estimator with precomputed measurements

%% Step 3: Add Estimator(s)
%
%   For this example there will be only a single estimator present whos job
%   is to estimate the state of the satellite at all times.
%
%   See the estimator class for a lot more detail on what is going on here

% using Kaz's format here for passing parameters to the filter

% for this example, the initial state and covariance as the initial
% position and velocity of the satellite and this covariance
%
% create the estimator given these terms
% TODO: pass in the sensor model to use...
% TODO: work out how best to pass in the sensor model to use
estimator = atlas.filter.PodES(t(1),world, PARAMS);


%% Step 3: Simulate!
%
%   In this example, the measurement data will not be pre-computed,
%   everything will be done in one shot

% before we can do the full simulation, we actually need to build out
% simulator...

% assemble the simulation desired
% NOTE: this wrapper is very much in development and is really aimed at
% being able to quickly run a parfor loop across many different simulations
% (see example code a few lines down)
%
%   ideally this will allow specifying additional models, etc to allow for
%   more flexibility and all that good stuff.

% limit the number of time steps
indsUse = 1:length(t);
indsUse = 1:100;
tShort = t(indsUse);
sim = atlas.Simulation(tShort, world, allSensors, ...
    'Estimator', estimator, ...
    'MeasurementData', measurementData(indsUse));


% run the sim -> and that's it!
%
% this steps through every time step and calls the Estimator's `simStep()`
% function at every time step, allowing for you to change up what should
% happen in the main simulation loop.  (Additionally the sim handles the
% getting of measurements at each time step, etc).
sim.run();

%% plot results- very simple for now
satEstUniqueID = unique(sim.Estimator.stateUniqueID);

satPlot = 10;

estPosEci = sim.StateHistory(sim.Estimator.stateUniqueID == satEstUniqueID(satPlot) ...
    & sim.Estimator.stateType == sim.Estimator.PositionStateTypeID,:);
estVelEci = sim.StateHistory(sim.Estimator.stateUniqueID == satEstUniqueID(satPlot) ...
    & sim.Estimator.stateType == sim.Estimator.VelocityStateTypeID,:);
estR_body_eci = sim.StateHistory(sim.Estimator.stateUniqueID == satEstUniqueID(satPlot) ...
    & sim.Estimator.stateType == sim.Estimator.AttitudeStateTypeID,:);

covPosEci = sim.CovarianceHistory(sim.Estimator.stateUniqueID == satEstUniqueID(satPlot) ...
    & sim.Estimator.stateType == sim.Estimator.PositionStateTypeID,sim.Estimator.stateUniqueID == satEstUniqueID(satPlot) ...
    & sim.Estimator.stateType == sim.Estimator.PositionStateTypeID,:);

covVelEci = sim.CovarianceHistory(sim.Estimator.stateUniqueID == satEstUniqueID(satPlot) ...
    & sim.Estimator.stateType == sim.Estimator.VelocityStateTypeID,sim.Estimator.stateUniqueID == satEstUniqueID(satPlot) ...
    & sim.Estimator.stateType == sim.Estimator.VelocityStateTypeID,:);

covAtt = sim.CovarianceHistory(sim.Estimator.stateUniqueID == satEstUniqueID(satPlot) ...
    & sim.Estimator.stateType == sim.Estimator.AttitudeStateTypeID,sim.Estimator.stateUniqueID == satEstUniqueID(satPlot) ...
    & sim.Estimator.stateType == sim.Estimator.AttitudeStateTypeID,:);

estPosEcef = nan(size(estPosEci));
estVelEcef = nan(size(estPosEci));
covPosEcef = nan(size(covPosEci));
covVelEcef = nan(size(covVelEci));
truePos = nan(size(estPosEcef));
trueVel = nan(size(estPosEcef));
attErr = nan(size(estPosEcef));
time = sim.Time;

% convert estimated satellite state to ecef
for idx = 1:length(tShort)
    [estPosEcef(:,idx), estVelEcef(:,idx),R_eci_ecef] = atlas.tools.eci2ecef(t(idx),estPosEci(:,idx),estVelEci(:,idx));
    
    covPosEcef(:,:,idx) = R_eci_ecef*squeeze(covPosEci(:,:,idx))*R_eci_ecef';
    covVelEcef(:,:,idx) = R_eci_ecef*squeeze(covVelEci(:,:,idx))*R_eci_ecef';
    
    trueStatei = sim.World.getState(t(idx), satEstUniqueID(satPlot) , {'Position', 'Velocity','Attitude'});
    
    truePos(:,idx) = trueStatei.Position;
    trueVel(:,idx) = trueStatei.Velocity;
    
    % Attitude
    % Estimated body to eci DCM
    R_est_eci = atlas.tools.euler2dcm123(estR_body_eci(1,idx),estR_body_eci(2,idx),estR_body_eci(3,idx));
    R_est_ecef = R_eci_ecef'*R_est_eci;
    
    R_true_ecef = atlas.tools.q2dcm(trueStatei.Attitude);
    
    R_true_est = R_true_ecef'*R_est_ecef;
    
    [attErr(1,idx),attErr(2,idx),attErr(3,idx)] = atlas.tools.dcm2euler123(R_true_est);
    
end

estPos =  estPosEcef;
estVel = estVelEcef;

dPos = estPos-truePos;
dVel = estVel-trueVel;

stdPosDiag = sqrt(squeeze([covPosEcef(1,1,:) covPosEcef(2,2,:) covPosEcef(3,3,:)]));

stdCovDiag = real(sqrt(squeeze([covAtt(1,1,:) covAtt(2,2,:) covAtt(3,3,:)])));
%
% figure(1); clf; hold on;
% plot3(truePos(1,:),truePos(2,:),truePos(3,:))
% plot3(estPos(1,:),estPos(2,:),estPos(3,:))
% axis equal

ylabels = {'x [m]','y [m]','z [m]'};
tPlot = (time-time(1))/60;
figure(1); clf; 
for idx = 1:3
    subplot(3,1,idx); hold on;
    plot(tPlot,dPos(idx,:)')
    plot(tPlot,3*stdPosDiag(idx,:)','k','linewidth',2)
    plot(tPlot,-3*stdPosDiag(idx,:)','k','linewidth',2)
    grid on;
    ylabel(ylabels{idx})
    xlabel('Time of sim [min]')
    xlim([0 max(tPlot)])
end
legend('Pos error','Covariance')

figure(2); clf; 
for idx = 1:3
    subplot(3,1,idx); hold on;
    plot(attErr(idx,:)');
    plot(-3*stdCovDiag(idx,:),'k','linewidth',2);
    plot(3*stdCovDiag(idx,:),'k','linewidth',2);
%     ylim(1e-5*[-1 1])

end





























