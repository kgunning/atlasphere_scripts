% quaternion test

%

nTest = 1000;
dAngles = nan(3,nTest);


for idx = 1:nTest
    
    % Make a random DCM
    
    x0 = randn(3,1);
    % normalize
    x = x0./norm(x0);
    
    y0 = randn(3,1);
    y0 = y0./norm(y0);
    
    z0 = cross(x0,y0);
    z = z0./norm(z0);
    
    y = cross(z,x);
    
    R_b_e0 = [x y z]';
    
    q_b_e = atlas.tools.dcm2q(R_b_e0);
    
    R_b_e1 = atlas.tools.q2dcm(q_b_e);
    
    dR = R_b_e0*R_b_e1';
    
    [dAngles(1,idx), dAngles(2,idx), dAngles(3,idx)] = atlas.tools.dcm2euler123(dR);
    
    
    if any(abs(dAngles(:,idx)) > 1e-10)
        'fdafa';
    end
        
end

figure(1); clf; hold on;
plot(dAngles','.')