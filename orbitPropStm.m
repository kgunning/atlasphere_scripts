function [epochs, pos, vel] = orbitPropStm(pos0,vel0,epochs)


stateTruth = [pos0; vel0];

% build the vector of epochs for which to calculate the position
dt = epochs(2)-epochs(1);

% contains for the data
pos = nan(3, 1, length(epochs));
vel = nan(3, 1, length(epochs));

pos(:,:,1) = pos0;
vel(:,:,1) = vel0;


% Integrate each step individually

for tdx = 2:25;%length(epochs)
    Psi = reshape(eye(6),36,1);
    
    pos0 = squeeze(pos(:,:,tdx-1));
    vel0 = squeeze(vel(:,:,tdx-1));
    
    stmState = [pos0; vel0; Psi];
    
    options = odeset('AbsTol',1e-10,'RelTol',1e-12);
    [t,stmStateOut] = ode45(@orbitDiffEqStm,epochs((tdx-1):tdx),stmState,options);
    
%     pos(:,1,:) = permute(posVel(:,1:3),[2 3 1]);
%     vel(:,1,:) = permute(posVel(:,4:6),[2 3 1]);
    Psi = reshape(stmStateOut(end,7:end), 6, 6);
    
    posVelProp = Psi*[pos0; vel0];
    
    pos(:,1,tdx) = posVelProp(1:3);
    vel(:,1,tdx) = posVelProp(4:6);
    
    pos(:,1,tdx) = stmStateOut(end,1:3);
    vel(:,1,tdx) = stmStateOut(end,4:6);
    
    % compare the two
    dpos = posVelProp(1:3)-stmStateOut(end,1:3)';
    dvel = posVelProp(4:6)-stmStateOut(end,4:6)';
end


end