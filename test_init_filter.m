%% Example: Running a Complete Simulation
%   This examples run through the most complex use of the atlasphere
%   toolbox: running a simulation to simulate the performance of an
%   estimator that is responsible for estimating the position of satellites
%   in orbit using optical sensor measurements between satellites and to
%   ground stations.
%

clear; clc; close all force;
%% Step 1: Build the World
%
%   The very first thing that needs to be done is to build out the world
%   that we are interested in simulating.  In this case, the world is not
%   just defined by a set of "entities" that exist in the world, but also
%   by the time in which these "entities" live.


%
% generate satellite data
%

disp('(1/6) Simulating satellite motion and initializing ground stations')

pos0 = [899168.711743225 -7323141.48563245 0]'; % same lon as DC
vel0 = [0 0 sqrt(xom.constants.earth.mu/norm(pos0))]';

% parameters that control the generation of the constellation
nSatsPerPlane = 3;
nPlanes = 1;
planeSpacingDeg = 20;

epochStart = xom.time.cal2epochs(2019, 08, 01, 0, 0, 0);
dt = 1;
epochs = epochStart+(dt:dt:(900+200));

sats = atlas.constbuilder.buildSimpleFromPV(nSatsPerPlane, nPlanes, epochs, 'PosEci0', pos0, ...
                                     'VelEci0', vel0, ...
                                     'PlaneSpacing', planeSpacingDeg);

%
% generate ground station data
%

% basically now do the same thing for the ground station
% gsLlh = [  39  -77   0;  % deg, deg, m - usno, dc
%            19 -155   0;  % deg, deg, m - keck, hawaii
%            40   -4   0;  % deg, deg, m - DSN, madrid
%            -35 149   0]; % deg, deg, m - canberra
    
gsLlh =  [45 -35 0];


gs = atlas.tools.buildTruthGS(gsLlh, epochs);

%
% build out the world
%

% create the world object to be used by others
erModel = xom.earthrot.IAU76FK5;
world = atlas.World([sats gs], 'EarthRotModel', erModel);

%% Step 2: Create Graph
%
% this creates the graph and adds the desired level of links

disp('(2/6) Building FSO graph')

% create the graph of the constellation given the list of satellites in the
% constellation
constGraph = atlas.ConstGraph(sats);

% add along plane links
constGraph.addAlongLinks();

% add cross plane links 
% `ConnectAll` signals that, for a 2 plane constellation, you want to
% connect all the satellites with cross links
%
% Not sure if eventually we want to analyze without this assumption (e.g.
% simulating the constellation growing), so have it as the awkward param
% for now
if nPlanes > 1
    constGraph.addCrossLinks('ConnectAll', true);
end
% for a sanity check before moving forward to make sure the links are where
% they are desired, you can visualize the links
figure;
constGraph.visualizeLinks();
title('Constellation with Cross-Links Visualized');

% NOTE: you can even animate the links for the truth data provided if you
% want using `constGraph.animateLinks()`

%% Step 3: Set filter and sensor parameters

disp('(3/6) Setting filter parameters')
% these parameters should at some point be split up for the world and the
% estimator, but for now they need to be included in the measurement
% creation

% Measurement sigmas
PARAMS.sigRange = 0.02; % std dev m
PARAMS.sigDopp  = 0.02; % std dev m/s0
PARAMS.sigBearingGS = 100e-6; % std dev radians
PARAMS.sigBearingSV = 600e-6; % std dev radians
PARAMS.sigST    = 1e-6; % std dev radians
PARAMS.rClkBias = 1e-10;
PARAMS.rClkDrft = 1e-10;

% Initial uncertainties
PARAMS.sigPos0 = 10; % m std dev
PARAMS.sigVel0 = 1; % m/s std dev
PARAMS.sigAtt0 = 0.1*pi/180; % rad std dev
PARAMS.sigClkBias0 = 1; % m std dev
PARAMS.sigClkDrft0 = 1; % m/s std dev

% Process noise sigmas
PARAMS.qPos = 1e-3;  % m std dev
PARAMS.qVel = 1e-5; % m/s std dev
PARAMS.qAtt = 2*pi/180;  % rad std dev
PARAMS.qClkBias = 10;  % m std dev
PARAMS.qClkDrft = 10;  % m std dev

% Whether or not to actually add noise to the measurements
PARAMS.addNoise = 1;

%% Step 4: Add Sensors
%
%   Now that we have entities in a world (and have created the associated
%   truth data), let's add some sensors to this world.
%

disp('(4/6) Adding sensors to satellite and ground station entities')

%
% add optical sensors on satellite to connect to ground stations and star
% trackers
%

opticalModel = atlas.sensor.model.optical.MotionFlexModel();

% specify the origin for this sensor -> here saying that the origin can
% be from any ground station
originUID = atlas.UniqueID(atlas.UIDTypeEnum.GroundStation);
sensorNormal = [0 0 1];  % adding sensor normals arbitrarily (just pointing down for now)
R_body_sensor = eye(3);  % body to sensor rotation matrix
beamwidth = 360;  % Beamwidth - because normals aren't set, these can see everywhere

% create optical error sources
% optical error source at satellite sensors
opticalWhiteNoiseSV = atlas.sensor.errorsource.OpticalWhiteNoise(PARAMS.sigRange*PARAMS.addNoise,...
    PARAMS.sigDopp*PARAMS.addNoise, PARAMS.sigBearingSV*PARAMS.addNoise);
opticalErrorSourceSV = opticalWhiteNoiseSV;

% optical error source at satellite sensors
opticalWhiteNoiseGS = atlas.sensor.errorsource.OpticalWhiteNoise(PARAMS.sigRange*PARAMS.addNoise,...
    PARAMS.sigDopp*PARAMS.addNoise, PARAMS.sigBearingGS*PARAMS.addNoise);
opticalErrorSourceGS = opticalWhiteNoiseGS;

% make the optical sensor with its target
opticalToGS = atlas.sensor.Optical('OriginUID', originUID, ...
                              'Normal', sensorNormal, ...
                              'Beamwidth', beamwidth, ...
                              'R_body_sensor', R_body_sensor, ...
                              'ErrorSource', opticalErrorSourceSV, ...
                              'Model', opticalModel);

stModel = atlas.sensor.model.startracker.SimpleSTModel();

sensorNormal = [0 0 1];  % adding sensor normals arbitrarily (just pointing down for now)
R_body_sensor = eye(3);  % body to sensor rotation matrix

% Create Error Source for star tracker measurements
starTrackerWhiteNoise = atlas.sensor.errorsource.StarTrackerWhiteNoise(PARAMS.sigST*PARAMS.addNoise);
starTrackerErrorSource = starTrackerWhiteNoise;

% make the optical sensor with its target
sensorST = atlas.sensor.StarTracker('Normal', sensorNormal, ...
                                  'R_body_sensor', R_body_sensor, ...
                                  'ErrorSource', starTrackerErrorSource, ...
                                  'Model', stModel);
                          
sats.addSensors([opticalToGS sensorST]);

%
% add optical sensors and timing sensors on ground staitons
%

% specify origin- this canbe any xona satellite
originUID = atlas.UniqueID(atlas.UIDTypeEnum.XonaSatellite);
sensorNormal = [0 0 1]';  %Normal is just straight up in body frame
R_body_sensor = eye(3);  % body to sensor rotation matrix
beamwidth = 160;  % Beamwidth

% build the optical sensor
% NOTE: can use different sensor noise or model here as desired
opticalToSat = atlas.sensor.Optical('OriginUID', originUID, ...
                              'Normal', sensorNormal, ...
                              'Beamwidth', beamwidth, ...
                              'R_body_sensor', R_body_sensor, ...
                              'ErrorSource', opticalErrorSourceGS, ...
                              'Model', opticalModel);
                          
% building the timing sensor
timingExternalWhiteNoise = atlas.sensor.errorsource.TimingExternalWhiteNoise(PARAMS.rClkBias*PARAMS.addNoise, PARAMS.rClkDrft*PARAMS.addNoise);
timingExternalErrorSource = timingExternalWhiteNoise;
gsTiming = atlas.sensor.TimingExternal('ErrorSource', timingExternalErrorSource);

gs.addSensors([opticalToSat gsTiming]);

% use the graph based representation to populate the sensors
[selfUIDs, originUIDs] = constGraph.getAllLinks();

% loop through all the sensors
for i = 1:length(selfUIDs)
    indSelf = find([sats.UniqueID] == selfUIDs(i));

    % the origin is specified to be a specific satellite
    originUID = originUIDs(i);

    beamwidth = 360;
    R_body_sensor = eye(3);

    % the optical sensor with the target
    sensor = atlas.sensor.Optical('OriginUID', originUID, ...
                                  'Normal', sensorNormal, ...
                                  'Beamwidth', beamwidth, ...
                                  'R_body_sensor', R_body_sensor, ...
                                  'ErrorSource', opticalErrorSourceSV, ...
                                  'Model', opticalModel);

    % add the sensor to the satellite
    sats(indSelf).addSensors(sensor);
end


%
% TODO: add a way to quickly get the list of sensors -> this is a hack for
% the fact that there is only a single sensor in the entire system
allSensors = [sats.Sensors gs.Sensors ];

% trigger the update of the sensor list for the world
world.updateSensorList();

%% Step 5: Make the measurements
%
%   We precompute the measurements at this point to allow for rerunning
%   just the sim without having to recompute measurements.

tSim = epochs(floor(100/dt):(end-floor(100/dt)));
% tSim = tTruth(6:(end-5));

disp('(5/6) Making true measurements')
% the container for measurement data
measurementData = cell(length(tSim), 1);

% parallelize across time
parfor i = 1:length(tSim)
    measData = [];
    for s = 1:length(allSensors)
        measDatai = allSensors(s).getMeasurement(tSim(i), world);
        measData = [measData; measDatai];
    end
    
    measurementData{i} = measData;
end

% clean the measurement data (removes impossible measurement data)
measurementData = atlas.sensor.meas.removeImpossibleMeasurements(measurementData);

%% Step 5.5: Take out some ground station measurements
%
%   Take out some measurements to simulate contact losses between entities

if false
    disp('(5.5/6) Raining on some of the ground stations')
    
    % Set inputs. See atlas.tools.messageDrop for more input details.
    timeOut = 50:70;
    measOutage(1).TypeID = atlas.UIDTypeEnum.GroundStation;
    measOutage(1).EntityID = 1;
    measOutage(1).timeOut = timeOut;
    measOutage(2).TypeID = atlas.UIDTypeEnum.GroundStation;
    measOutage(2).EntityID = 2;
    measOutage(2).timeOut = timeOut;
    measOutage(3).TypeID = atlas.UIDTypeEnum.GroundStation;
    measOutage(3).EntityID = 3;
    measOutage(3).timeOut = timeOut;
    measOutage(4).TypeID = atlas.UIDTypeEnum.GroundStation;
    measOutage(4).EntityID = 4;
    measOutage(4).timeOut = timeOut;
    
    measurementData4Sim = atlas.tools.messageDrop(measurementData, 'StructIn', measOutage);
else
    measurementData4Sim = measurementData;
end

%% Step 6: Add Estimator(s)

disp('(6/6) Adding Estimators')
 
% Order the satellites and initialize each filter with a subset of the
% Xona satellites
% For now, keep all of the satellites in the same filter. 
nSatsPerFilter = 10; %9999;
satIDs   = [sats(:).ID]';
planeIDs = bitshift(satIDs, -8);
slotIDs  = bitand(satIDs, 255);

% order the satelites by plane then slot
satIDPlaneSlot = sortrows([planeIDs slotIDs satIDs],[1 2]);

nFilters = ceil(size(satIDPlaneSlot,1)/nSatsPerFilter);

estimators = atlas.filter.PodDecSRIF.empty(nFilters,0);

% Setup the filter propagators
egmModel = xom.grav.EGM96(24);
erModel  = xom.earthrot.IAU76FK5;
thirdBodyModel = xom.hoeff.ThirdBody('WithMoon',true,'WithSun',true);
integrator = 'RK8';

estOrbitProp = xom.prop.StateVector('EgmModel',egmModel,'ErModel',erModel,...
    'Integrator',integrator);

for idx = 1:nFilters
    % pass in a list of satIDs to use in this filter
    satIDsi = satIDPlaneSlot(((idx-1)*nSatsPerFilter+1):min([idx*nSatsPerFilter size(satIDPlaneSlot,1)]),3);
    
    estimators(idx) = atlas.filter.PodDecSRIF(tSim(1), world, PARAMS,...
        'satIDsubset',satIDsi,'estimatorID',idx,'EarthRotModel',erModel,...
        'OpticalModel',opticalModel,'OrbitProp',estOrbitProp,'TFilterUpdate',dt);
end

% create the wrapper for the POD estimator that will be the thing that
% interfaces with the simulation
podWrappers = atlas.filter.PODWrapper(estimators);

%% Step 7: Register with Data Router
%
% Register the estimator(s) and the data publishers with the data router to
% be able to properly route the measurements in the world

disp('(7/6) Registering elements')

% create a ground station data manager -> this is the publisher of the
% state data for ground stations in the POD filter
gsDataManager = atlas.GroundStationDataManager(gs);

% create the corrections manager
correctionsManager = atlas.CorrectionDataManager();

% create the data router for the world -> will be responsible for making
% sure measurements and nav msgs get to each of the estimators that need it
%
% this piece ends up in the world object and is used by the simulation to
% manage the data internally
dataRouter = atlas.DataRouter();

% register all the estimators (since the router needs to know what
% estimators abound in this world)
%
% these are atlas.internal.Representer objects that ideally are
% atlas.filter.AbstractEstimators (TODO: determine if it actually has to
% be...)
dataRouter.registerEstimator(podWrappers);

% register all the publishers (since the router needs to be able to grab
% the relevant state nav msg to pass to an estimator)
%
% these must be atlas.internal.Representer(s) that have subclassed a
% publisher interface
dataRouter.registerPublisher(gsDataManager);
dataRouter.registerPublisher(podWrappers);  % NOTE: the wrappers end up being the publishers for the entities within a filter
dataRouter.registerPublisher(correctionsManager);

%% Step 8: Run the Simulation
%
% the actual running of the simulation
disp('(8/6) Running simulation')

%
%   Setup the simulation
tic
% limit the number of time steps
sim = atlas.Simulation(tSim, world, ...
    'Estimator', podWrappers, ...
    'MeasurementData', measurementData4Sim, ...
    'DataRouter', dataRouter, ...
    'LogAllEstimators', true);

gsDataManager.setTimeSource(sim);
correctionsManager.setTimeSource(sim);

% run the sim -> and that's it!
%
% this steps through every time step and calls the Estimator's `simStep()`
% function at every time step, allowing for you to change up what should
% happen in the main simulation loop.  (Additionally the sim handles the
% getting of measurements at each time step, etc).
sim.run();
toc

%% Step 9: Plot Results
%
% plot the results using the estimators plotting routines
% there is no formal interface for this aspect (yet, or probably ever) so
% this changes a lot between one estimator and another based on the type of
% information that you might want to plot
disp('(9/6) Plot results')

% plot results- lots of stuff commented out here, but feel free to check it
% out. 

% the satellite to plot
% satListIdx = 4;

% get the ID of the satellite to plot
% uids = unique(sim.Estimator(1).stateUniqueID);
% satID = uids(satListIdx);

% plot the selected satellite in the list
%
% NOTE: whatever estimator is used is the one who's plot function should be
% called here
% estimators.plotSatelliteHistory(sim, satID);
% drawnow

% Plot histograms
outStruc = estimators.plotNormErrHist(sim);

close all
estimators.plotNormErrProcessed(outStruc,'plot_all_rac_norm',true,'plot_hist_rac_norm',true);


% Compare to thresholds
racThresh = [3 3 4]';

if all(outStruc.rmsRac < racThresh)
    fprintf('\n(7/6) Satisfactory performance from the simulation- test passed! \n')    
else
    warning('(7/6) RMS errors exceed the pre-set thresholds- something may be wrong.')
end


% Plot residuals
% estimators.plotResids(sim);

% Plot a specific satellites residuals
% estimators.plotSpecificResids(sim,satID);

%% Step 10: Save data
%
% Save the data to be used for post-processing and inputs to the
% test_user_estimator.m script. Need to set true/false below for the types
% of saves you want to do.
disp('(10/6) Save results (if prompted)')


% Only need to save the EstimatorLogs from the sim object
estimatorLogs = sim.EstimatorLogs;

%
% Construct save string name
%

% Get time
totalSimTime = tSim(end) - tSim(1);
timeUnitStr = 's';
if totalSimTime > 60
    totalSimTime = totalSimTime/60;
    timeUnitStr = 'min';
    if totalSimTime > 60
        totalSimTime = totalSimTime/60;
        timeUnitStr = 'hr';
    end
end
if rem(totalSimTime,1) == 0
    timeStr = [num2str(totalSimTime), timeUnitStr];
else
    timeStr = [num2str(totalSimTime, '%.2f'), timeUnitStr];
end

% Get num sats/planes
nSatsPerPlaneStr = num2str(nSatsPerPlane);
nPlanesStr = num2str(nPlanes);

% Get the type of POD filter
if nSatsPerFilter == 1
    podFilterTypeStr = 'independent';
elseif nSatsPerFilter >= nSatsPerPlane*nPlanes
    podFilterTypeStr = 'centralized';
else
    podFilterTypeStr = 'decentralized';
end

% Miscellaneous string set by the person looking at this comment
miscStr = '';

saveString = [timeStr, '_', nPlanesStr, 'x', nSatsPerPlaneStr, '_',...
    podFilterTypeStr];
if ~isempty(miscStr)
    saveString = [saveString, '_', miscStr];
end

% Save info for POD post-processing
if false
    save([saveString, '_outStruc.mat'], 'outStruc', '-v7.3')
end

% Save info relevant for user estimation
if false
    save([saveString, '_userInput.mat'], 'world', 'PARAMS', 'tSim', 'estimatorLogs', 'measurementData4Sim', '-v7.3')
end

% Save all script information if desired
if false
    save([saveString, '_allInfo.mat'], '-v7.3');
end








