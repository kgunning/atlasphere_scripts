% Developing ECI2ECEF (and ECEF2ECI) for real data

if ~exist('datai','var')
    filename = 'C:\Users\Kaz\Documents\atlasphere data\gmat\rpt\ReportFile_PokeSat_00257_justgrav.rpt';
    
    datai = importdata(filename);
    
    posTrueEci = datai.data(:,11:13)*1000;
    velTrueEci = datai.data(:,14:16)*1000;
    
    posTrueEcef = datai.data(:,1:3)*1000;
    velTrueEcef = datai.data(:,4:6)*1000;
    
    t = datenum(datai.textdata(2:end));
end

Nepochs = length(t);


%%
% Convert from ECEF to ECI
posRotEci = nan(size(posTrueEcef));
velRotEci = nan(size(posTrueEcef));

posRotEcef = nan(size(posTrueEcef));
velRotEcef = nan(size(posTrueEcef));

posBothEci = nan(size(posTrueEcef));


for tdx = 1:Nepochs
    [posRotEci(tdx,:), velRotEci(tdx,:)] = ECEF_to_J2000(posTrueEcef(tdx,:)',velTrueEcef(tdx,:)',zeros(3,1),0,t(tdx)+0/86400);
    
%     [posRotEcef(tdx,:), velRotEcef(tdx,:)] = J2000_to_ECEF(posTrueEci(tdx,:)',velTrueEci(tdx,:)',zeros(3,1),0,t(tdx)+0*0.7/86400);
    
%     posBothEci(tdx,:) = ECEF_to_J2000(posRotEcef(tdx,:)',velRotEcef(tdx,:)',zeros(3,1),0,t(tdx)+0*0.7/86400);
end
% jd = juliandate(datetime(datestr(t)));
% 
% [posRotEci, velRotEci] = atlas.internal.ecef2eciMICE(t,posTrueEcef,velTrueEcef);

%%



%Compare
dposEci = posRotEci-posTrueEci;
dvelEci = velRotEci-velTrueEci;
dposEci(1,:)

% dposEcef = posRotEcef-posTrueEcef;

% dposEci2 = posBothEci-posTrueEci;

if 1
    figure(1); clf; hold on;
    %     plot3(posRotEci(:,1),posRotEci(:,2),posRotEci(:,3))
    %     plot3(posTrueEci(:,1),posTrueEci(:,2),posTrueEci(:,3))
    plot(dposEci,'.')
%         ylim([-5 5])
    
    figure(2); clf; hold on;
    plot(dvelEci,'.')
end