% UD decomposition



% Q = diag([1e-3 1e-4 1e-5]);
A = randn(3);

Q = A'*A;


M = triu(Q);

d = zeros(size(Q,1),1);
U = eye(size(Q,1));

n = size(Q,1);

for j = n:-1:1
    d(j) = M(j,j);
    
    if d(j) > 0
        alpha = 1/d(j);
    else
        alpha = 0;
    end
    
    for k = 1:j
       beta = M(k,j);
       U(k,j) = alpha*beta;
       M(1:(k),k) = M(1:(k),k) - beta*U(1:(k),j);
        
    end
end
d = diag(d);


dMat = Q-U*d*U'





