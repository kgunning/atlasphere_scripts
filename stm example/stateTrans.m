function A=stateTrans(t0, tf, x,G,m1,m2,m3)

%Compute the state transition matrix at time tf for the
%path x(t) with x(t0)=x0

tspan=[t0,tf];                        %time span over which to run the integration


%--------------------------------------------------------------------------
%The state transition matrix is determined by a 12X12 matrixODE
%This gives rise first to a system of 144 ODEs.  However the matrix
%ODE is nonautonomous and depends on a particular solution (trajectory)
%of the N-Body problem.  This trajectory is itself the solution of a system

%of 12 ODEs.  The two systems are solved simultaneously, giving an
%autonomous system of 156 ODEs.
%--------------------------------------------------------------------------
%set up the initial condition, y0 for the 156 component system
y0=0;
%since the initial condition for the 12X12 matrix system is the 12X12
%identity matrix the initial condition vector is very sparse.  The first
%144 components are 0s and 1s, and the last 12 are the initial conditions
%from the N-Body problem

%Initialize a 12X12 identity matrix
I=eye(12);
%put the entries, row by row, into y0.
for i=1:12
    for j=1:12;
        y(12*(i-1)+j)=I(i,j);
    end
end

%the inital conditions for the particular orbit of the N-body problem have
%been passed in as 'x'. This to the end of y.

y(145:156)=x';

%Convert to a column vector and pass the initial conditions to the
%integrator
y0=y';%inital condition
options=odeset('RelTol',1e-14,'AbsTol',1e-22);             %set tolerences
[t,Y]=ode113('sysSolve',tspan,y0,options,[],G,m1,m2,m3);   %integrate the system

%[t,Y] is a huge matrix.  It is made up of a row for each time and 156
%columns.  But the desired data is the state transition matrix at the final
%time.  Then the first 144 entries of the last row of Y must be put into a
%12X12 matrix which will be passed back to the caller
%find out the row number of tf
b=size(Y);        %returns the number of rows and columns as a row vector
m=b(1,1);         %so �m� is the number of rows
c=Y(m,1:144);     %so �c� is a vector containing the first 144 entries of the last row of Y

%now c has to be made into a 12X12 matrix
d=0;
for i=1:12
    for j=1:12
        d(i,j)=c(12*(i-1)+j);
    end
end


x2stm = d'*x;
x2ode = Y(end,(end-11):end)';

haha = [x2stm x2ode x2stm-x2ode];

%d is the state transition matrix at the final time and is passed back to%the caller
A=d;




















