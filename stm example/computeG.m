function GMatrix=computeG(x,G, m1, m2, m3)


%function returns the matrix 'G' for the three body problem in the
%plane with parameters as specified.

%function returns the matrix 'G' for the three body problem in the
%plane with parameters as specified.
%Collection the x data into the r_i vectors.
r1=x(1:2);
r2=x(3:4);
r3=x(5:6);

%The distances.  Rij stands for |r_j-r_i| where here r_i and
%r_j are vectors.  (two scripts is a scalar and one script is a vector.)
R12=sqrt((r2-r1)*(r2-r1)');
R21=R12;
R13=sqrt((r3-r1)*(r3-r1)');
R31=R13;
R23=sqrt((r3-r2)*(r3-r2)');
R32=R23;

%Only three terms, which are denoted by A12, A13, and A23 and are each 2X2
%matrices are necessary tocompute G.  All the elements of G are composed
%of these.
A12=((3/(R12)^5)*(r2-r1)'*(r2-r1)-(1/(R12)^3)*eye(2));
A13=((3/(R13)^5)*(r3-r1)'*(r3-r1)-(1/(R13)^3)*eye(2));
A23=((3/(R23)^5)*(r3-r2)'*(r3-r2)-(1/(R23)^3)*eye(2));

%These are combined to make G;
GMatrix=[G*(m2*A12+m3*A13), -G*m2*A12, -G*m3*A13;
    -G*m1*A12, G*(m1*A12+m3*A23), -G*m3*A23;
    -G*m1*A13, -G*m2*A23, G*(m1*A13+m2*A23)];










