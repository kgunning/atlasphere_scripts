%Program produces results for problem 4 of homework two
%This is the Choreography problem
%This is the code to cary out problem 3c and 3d in homework 2
G=1;         %gravatational constant
N=3;         %Number of bodies
%Masses
m1=1;
m2=1;
m3=1;
%Sum of masses
M=m1+m2+m3;
%mass vector
mass=[m1,m2,m3];

%time parameters
t0=0.0;     %initial time
%Initial positions
r11=-0.755;
r12=0.355;
r21=1.155;
r22=-0.0755;
r31=-0.4055;
r32=-0.3055;
%Initial velocities
v11=0.9955;
v12=0.07855;
v21=0.1055;
v22=0.4755;
v31=-1.1055;
v32=-0.5355;

%Initial state
xStar0=[r11; r12; 
    r21; r22;
    r31; r32;
    v11; v12;
    v21; v22;
    v31; v32];

tau=0.01;

X=stateTrans(t0,tau,xStar0,G,m1,m2,m3);