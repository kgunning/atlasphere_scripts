function ydot=sysSolve(t,y,options,flag,G,m1,m2,m3)

%This file contains the right hand side for the system which defines the
%state transition matrix for the three body problem in the plane.  So ydot
%is a vector with 156 components

%-------------------------------------------------------------------------
%The structure of the system is
%
%          A�=Df*A, x�=f(x)
%
%Where A,Df, are 12X12 matrices and A� is the derivative of A.
%Df is the derivative of the N-body vector field f and has the form
%
%                    |  0   I  |
%               Df = |         |
%                    | G    0  |
%
%where all submatrices are 6X6 and G has depends only on the position
%vectors of the bodies (all be it in a complicated way), and x�=f(x)
% is the 3-body problem in the plane.  All of this is combined onto one
%system of 156 1st order ODEs. The right hand side for the system is coded
%in the remainder of the file
%------------------------------------------------------------------------
%FIRST
%The matrix G is computed
%This matrix depends on the positions of the N-body problem.
%These positions are contained in the 6 entries y(145:150).
%put the positions aside
x(1:6)=y(145:150);

%Now compute the matrix G.  Since �G� already denotes the gravatational
%constant call the matrix G �GMatrix�.
%This is done by calling �computeG.m�
GMatrix=computeG(x,G, m1, m2, m3);

%SECOND%The right hand side for the state transition system is computed
%To do this construct a matrices �A� �I� and �O�, where �A� contains the
%variables and �O� is a 6X6 matrix of zeros and �I� is the 6X6 identity.
O=zeros(6);
I=eye(6);

%Now the complete Jacobian of f is assembled
Df=0;
Df=[O,I;GMatrix,O];

%Make A
for i=1:12
    for j=1:12
        A(i,j)=y(12*(i-1)+j);
    end
end

%Then compute the 12X12 matrix Df*A, is named DfA.
DfA=0;
DfA=Df*A;

%This has to be put into vector format. We temporaly place theresults in
%the 144-vector �a�.  Later this will be the first 144 components of ydot.
a=0;
for i=1:12
    for j=1:12
        a(12*(i-1)+j)=DfA(i,j);
    end
end

%THIRD
%The last 12 entries are the vector field for the 3-Body problem,
%restricted to the plane.  These are stored in �c�.
%-------------------------------------------------------------------------
N=3;                 %Number of bodies
M=m1+m2+m3;          %Total mass
Mass=[m1 m2 m3];     %Vector of Masses

%Initialize the variables
acc=0;
s1=0;
s2=0;
s3=0;

%Compute the elements of the n-body vector field
for i=1:N
    for j=1:N
        Rij=(y(144+2*i-1)-y(144+2*j-1))^2+(y(144+2*i)-y(144+2*j))^2;
        %compute the three components of acceleration i
        if j~=i
            s1=s1+(Mass(1,j)/(sqrt(Rij))^3)*(y(144+2*j-1)-y(144+2*i-1));
            s2=s2+(Mass(1,j)/(sqrt(Rij))^3)*(y(144+2*j)-y(144+2*i));
        else
            s1=s1+0;
            s2=s2+0;
        end
    end
    acc(2*i-1)=G*s1;
    acc(2*i)=G*s2;
    s1=0;
    s2=0;
end
%Store the accelerations
accelerations=0;
accelerations=acc;

%enter the velociety components
velocities=0;
for i=1:(2*N)
    velocities(i)=y(144+2*N+i);
end

%constructs a vector whose first 3*N entries are the velocities and whose
%last 3*N entries are the accelerations
c=0;
c(1:2*N,1)=velocities;
c(2*N+1:4*N,1)=accelerations;

%Put it all toghether and pass back to integrator
ydot=[a';c];
































