function [K, x, P] = MeasUpdate_(x, z, g, sigma, G, P, n)

Inv_W = sigma*sigma;    % Inverse weight (measurement covariance)

% Kalman gain
K = P*G'/(Inv_W+dot(G,P*G'));

% State update
x = x + K*(z-g);

% Covariance update  
P = (eye(n)-kron(K,G))*P;

