%--------------------------------------------------------------------------
%
% 
%    RADAU Integrator
%
% Last modified:   2019/03/24   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
close all
format long g

% constants
GM  = 1;                   % gravitational coefficient
e   = 0.1;                 % eccentricity
Kep = [1, e ,0 ,0 ,0 ,0]'; % (a,e,i,Omega,omega,M)

% initial state
Y0 = State(GM, Kep, 0);

% header
fprintf( '\nRADAU Integrator\n\n' );
Step = 60;
N_Step = 86400/60;
options = rdpset('RelTol',1e-13,'AbsTol',1e-16);
tic
Eph = Ephemeris(Y0, N_Step, Step)
% [tout,yout] = radau(@Deriv,tspan,y0,options);
Y = Eph(end,:)';
toc

Y_ref = State(GM, Kep, N_Step*Step); % Reference solution

fprintf(' Accuracy    Digits\n');
fprintf('%6.2e',norm(Y-Y_ref));
fprintf('%9.2f\n',-log10(norm(Y-Y_ref)));

toc

