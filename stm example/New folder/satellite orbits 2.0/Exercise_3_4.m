%--------------------------------------------------------------------------
%
%   Exercise 3-4: Orbit Perturbations
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

global const PC Cnm Snm AuxParam eopdata swdata n_eqn

SAT_Const
load DE430Coeff.mat
PC = DE430Coeff;

% read Earth gravity field coefficients
Cnm = zeros(181,181);
Snm = zeros(181,181);
fid = fopen('GGM03S.txt','r');
for n=0:180
    for m=0:n
        temp = fscanf(fid,'%d %d %f %f %f %f',[6 1]);
        Cnm(n+1,m+1) = temp(3);
        Snm(n+1,m+1) = temp(4);
    end
end
fclose(fid);

% read Earth orientation parameters
fid = fopen('eop19990101.txt','r');
%  ----------------------------------------------------------------------------------------------------
% |  Date    MJD      x         y       UT1-UTC      LOD       dPsi    dEpsilon     dX        dY    DAT
% |(0h UTC)           "         "          s          s          "        "          "         "     s 
%  ----------------------------------------------------------------------------------------------------
eopdata = fscanf(fid,'%i %d %d %i %f %f %f %f %f %f %f %f %i',[13 inf]);
fclose(fid);

% read space weather data
fid = fopen('sw19990101.txt','r');
%  ---------------------------------------------------------------------------------------------------------------------------------
% |                                                                                             Adj     Adj   Adj   Obs   Obs   Obs 
% | yy mm dd BSRN ND Kp Kp Kp Kp Kp Kp Kp Kp Sum Ap  Ap  Ap  Ap  Ap  Ap  Ap  Ap  Avg Cp C9 ISN F10.7 Q Ctr81 Lst81 F10.7 Ctr81 Lst81
%  ---------------------------------------------------------------------------------------------------------------------------------
swdata = fscanf(fid,'%4i %3d %3d %5i %3i %3i %3i %3i %3i %3i %3i %3i %3i %4i %4i %4i %4i %4i %4i %4i %4i %4i %4i %4f %2i %4i %6f %2i %6f %6f %6f %6f %6f',[33 inf]);
fclose(fid);

% epoch state (remote sensing satellite)
Mjd_UTC = Mjday(1999, 3, 1, 0, 0, 0);
Kep = [7178e3, 0.0010, 98.57*const.Rad, 0, 0, 0]';
Y0 = State(const.GM_Earth, Kep, 0);

% model parameters
Aux_ref = struct('Mjd_UTC',0,'area_solar',0,'area_drag',0,'mass',0,'Cr',0,...
                  'Cd',0,'n',0,'m',0,'sun',0,'moon',0,'sRad',0,'drag',0,...
                  'planets',0,'SolidEarthTides',0,'OceanTides',0,'Relativity',0);

Aux_ref.Mjd_UTC = Mjd_UTC;
Aux_ref.area_solar = 5; % [m^2] remote sensing satellite
Aux_ref.area_drag  = 5; % [m^2] remote sensing satellite
Aux_ref.mass    = 1000; % [kg]
Aux_ref.Cr      = 1.3;
Aux_ref.Cd      = 2.3;
Aux_ref.n       = 20;
Aux_ref.m       = 20;
Aux_ref.sun     = 1;
Aux_ref.moon    = 1;
Aux_ref.sRad    = 1;
Aux_ref.drag    = 1;
Aux_ref.planets = 1;
Aux_ref.SolidEarthTides = 1;
Aux_ref.OceanTides = 1;
Aux_ref.Relativity = 1;

% reference orbit
Step    = 120; % [s]
N_Step1 =  50; % 100 mins
N_Step2 = 720; % 1 day
n_eqn   = 6;

AuxParam = Aux_ref;
Eph_Ref = Ephemeris(Y0, N_Step2, Step);

% J2,0 perturbations
AuxParam.n = 2;
AuxParam.m = 0;
Eph_J20 = Ephemeris(Y0, N_Step2, Step);

% J2,2 perturbations
AuxParam.n = 2;
AuxParam.m = 2;
Eph_J22 = Ephemeris(Y0, N_Step2, Step);

% J4,4 perturbations
AuxParam.n = 4;
AuxParam.m = 4;
Eph_J44 = Ephemeris(Y0, N_Step2, Step);

% J10,10 perturbations
AuxParam.n = 10;
AuxParam.m = 10;
Eph_J1010 = Ephemeris(Y0, N_Step2, Step);
AuxParam.n = 20; AuxParam.m = 20;

% solar perturbations
AuxParam.sun = 0;
Eph_sun = Ephemeris(Y0, N_Step2, Step);
AuxParam.sun = 1;

% lunar perturbations
AuxParam.moon = 0;
Eph_moon = Ephemeris(Y0, N_Step2, Step);
AuxParam.moon = 1;

% solar radiation pressure perturbations
AuxParam.sRad = 0;
Eph_sRad = Ephemeris(Y0, N_Step2, Step);
AuxParam.sRad = 1;

% drag perturbations
AuxParam.drag = 0;
Eph_drag = Ephemeris(Y0, N_Step2, Step);
AuxParam.drag = 1;

% planets perturbations
AuxParam.planets = 0;
Eph_planets = Ephemeris(Y0, N_Step2, Step);
AuxParam.planets = 1;

% solid Earth tides perturbations
AuxParam.SolidEarthTides = 0;
Eph_SolidEarthTides = Ephemeris(Y0, N_Step2, Step);
AuxParam.SolidEarthTides = 1;

% ocean tides perturbations
AuxParam.OceanTides = 0;
Eph_OceanTides = Ephemeris(Y0, N_Step2, Step);
AuxParam.OceanTides = 1;

% relativity perturbations
AuxParam.Relativity = 0;
Eph_relativity = Ephemeris(Y0, N_Step2, Step);
AuxParam.Relativity = 1;

% find maximum over N_Step1 steps
max_J20 = 0;
max_J22 = 0;
max_J44 = 0;
max_J1010 = 0;
max_sun = 0;
max_moon = 0;
max_sRad = 0;
max_drag = 0;
max_planets = 0;
max_SolidEarthTides = 0;
max_OceanTides = 0;
max_relativity = 0;

for i=1:N_Step1
    max_J20   = max(norm (Eph_J20  (i,1:3)-Eph_Ref(i,1:3)), max_J20);
    max_J22   = max(norm (Eph_J22  (i,1:3)-Eph_Ref(i,1:3)), max_J22);
    max_J44   = max(norm (Eph_J44  (i,1:3)-Eph_Ref(i,1:3)), max_J44);
    max_J1010 = max(norm (Eph_J1010(i,1:3)-Eph_Ref(i,1:3)), max_J1010);
    max_sun   = max(norm (Eph_sun  (i,1:3)-Eph_Ref(i,1:3)), max_sun);
    max_moon  = max(norm (Eph_moon (i,1:3)-Eph_Ref(i,1:3)), max_moon);
    max_sRad  = max(norm (Eph_sRad (i,1:3)-Eph_Ref(i,1:3)), max_sRad);
    max_drag  = max(norm (Eph_drag (i,1:3)-Eph_Ref(i,1:3)), max_drag);
    max_planets = max(norm (Eph_planets (i,1:3)-Eph_Ref(i,1:3)), max_planets);
    max_SolidEarthTides = max(norm (Eph_SolidEarthTides (i,1:3)-Eph_Ref(i,1:3)), max_SolidEarthTides);
    max_OceanTides = max(norm (Eph_OceanTides (i,1:3)-Eph_Ref(i,1:3)), max_OceanTides);
    max_relativity = max(norm(Eph_relativity (i,1:3)-Eph_Ref(i,1:3)), max_relativity);
end

% output
fprintf('Exercise 3-4: Orbit Perturbations \n\n');

fprintf('Remote sensing satellite: \n\n');

fprintf('  maximum position errors within ');
fprintf('%5.1f', N_Step1*Step/60 );
fprintf(' min propagation interval \n\n');
fprintf('    J2,0    J2,2    J4,4  J10,10'); 
fprintf('     Sun    Moon  SolRad    Drag     planets  SolidTides  OceanTides  relativity\n');
fprintf('     [m]     [m]     [m]     [m]');
fprintf('     [m]     [m]     [m]     [m]         [m]         [m]         [m]         [m]\n');
fprintf('%8.1f%8.1f%8.1f%8.1f%8.1f%8.1f%8.1f%8.1f%12.1f%12.1f%12.1f%12.1f\n\n', ...
        max_J20, max_J22, max_J44, max_J1010, max_sun, max_moon, max_sRad, ...
        max_drag, max_planets, max_SolidEarthTides, max_OceanTides, max_relativity);

% find maximum over N_Step2 steps
for i=N_Step1+1:N_Step2
    max_J20   = max(norm (Eph_J20  (i,1:3)-Eph_Ref(i,1:3)), max_J20);
    max_J22   = max(norm (Eph_J22  (i,1:3)-Eph_Ref(i,1:3)), max_J22);
    max_J44   = max(norm (Eph_J44  (i,1:3)-Eph_Ref(i,1:3)), max_J44);
    max_J1010 = max(norm (Eph_J1010(i,1:3)-Eph_Ref(i,1:3)), max_J1010);
    max_sun   = max(norm (Eph_sun  (i,1:3)-Eph_Ref(i,1:3)), max_sun);
    max_moon  = max(norm (Eph_moon (i,1:3)-Eph_Ref(i,1:3)), max_moon);
    max_sRad  = max(norm (Eph_sRad (i,1:3)-Eph_Ref(i,1:3)), max_sRad);
    max_drag  = max(norm (Eph_drag (i,1:3)-Eph_Ref(i,1:3)), max_drag);
    max_planets = max(norm (Eph_planets (i,1:3)-Eph_Ref(i,1:3)), max_planets);
    max_SolidEarthTides = max(norm (Eph_SolidEarthTides (i,1:3)-Eph_Ref(i,1:3)), max_SolidEarthTides);
    max_OceanTides = max(norm (Eph_OceanTides (i,1:3)-Eph_Ref(i,1:3)), max_OceanTides);
    max_relativity = max(norm(Eph_relativity (i,1:3)-Eph_Ref(i,1:3)), max_relativity);
end

% output
fprintf('  maximum position errors within ');
fprintf('%5.1f', N_Step2*Step/60);
fprintf(' min propagation interval \n\n');
fprintf('    J2,0    J2,2    J4,4  J10,10');
fprintf('     Sun    Moon  SolRad    Drag     planets  SolidTides  OceanTides  relativity\n');
fprintf('     [m]     [m]     [m]     [m]');
fprintf('     [m]     [m]     [m]     [m]         [m]         [m]         [m]         [m]\n');
fprintf('%8.1f%8.1f%8.1f%8.1f%8.1f%8.1f%8.1f%8.1f%12.1f%12.1f%12.1f%12.1f\n\n', ...
        max_J20, max_J22, max_J44, max_J1010, max_sun, max_moon, max_sRad, ...
        max_drag, max_planets, max_SolidEarthTides, max_OceanTides, max_relativity);
  
% epoch state (geostationary satellite)
Kep = [42166e3, 0.0004, 0.02*const.Rad, 0, 0, 0]';
Y0 = State(const.GM_Earth, Kep, 0);

% model parameters
Aux_ref.area_solar = 10; % [m^2] geostationary satellite
Aux_ref.area_drag  = 10; % [m^2] geostationary satellite

% reference orbit
Step    = 1200; % [s]
N_Step1 =   72; % 1 day
N_Step2 =  144; % 2 days 

AuxParam = Aux_ref; 
Eph_Ref = Ephemeris(Y0, N_Step2, Step);

% J2,0 perturbations
AuxParam.n = 2;
AuxParam.m = 0;
Eph_J20 = Ephemeris(Y0, N_Step2, Step);

% J2,2 perturbations
AuxParam.n = 2;
AuxParam.m = 2;
Eph_J22 = Ephemeris(Y0, N_Step2, Step);

% J4,4 perturbations
AuxParam.n = 4;
AuxParam.m = 4;
Eph_J44 = Ephemeris(Y0, N_Step2, Step);

% J10,10 perturbations
AuxParam.n = 10;
AuxParam.m = 10;
Eph_J1010 = Ephemeris(Y0, N_Step2, Step);
AuxParam.n = 20;
AuxParam.m = 20;

% solar perturbations
AuxParam.sun = 0;
Eph_sun = Ephemeris(Y0, N_Step2, Step);
AuxParam.sun = 1;

% lunar perturbations
AuxParam.moon = 0;
Eph_moon = Ephemeris(Y0, N_Step2, Step);
AuxParam.moon = 1;

% solar radiation pressure perturbations
AuxParam.sRad = 0;
Eph_sRad = Ephemeris(Y0, N_Step2, Step);
AuxParam.sRad = 1;

% drag perturbations
AuxParam.drag = 0;
Eph_drag = Ephemeris(Y0, N_Step2, Step);
AuxParam.drag = 1;

% planets perturbations
AuxParam.planets = 0;
Eph_planets = Ephemeris(Y0, N_Step2, Step);
AuxParam.planets = 1;

% solid Earth tides perturbations
AuxParam.SolidEarthTides = 0;
Eph_SolidEarthTides = Ephemeris(Y0, N_Step2, Step);
AuxParam.SolidEarthTides = 1;

% ocean tides perturbations
AuxParam.OceanTides = 0;
Eph_OceanTides = Ephemeris(Y0, N_Step2, Step);
AuxParam.OceanTides = 1;

% relativity perturbations
AuxParam.Relativity = 0;
Eph_relativity = Ephemeris(Y0, N_Step2, Step);
AuxParam.Relativity = 1;

% find maximum over N_Step1 steps
max_J20 = 0;
max_J22 = 0;
max_J44 = 0;
max_J1010 = 0;
max_sun = 0;
max_moon = 0;
max_sRad = 0;
max_drag = 0;
max_planets = 0;
max_SolidEarthTides = 0;
max_OceanTides = 0;
max_relativity = 0;

for i=1:N_Step1
    max_J20   = max(norm (Eph_J20  (i,1:3)-Eph_Ref(i,1:3)), max_J20);
    max_J22   = max(norm (Eph_J22  (i,1:3)-Eph_Ref(i,1:3)), max_J22);
    max_J44   = max(norm (Eph_J44  (i,1:3)-Eph_Ref(i,1:3)), max_J44);
    max_J1010 = max(norm (Eph_J1010(i,1:3)-Eph_Ref(i,1:3)), max_J1010);
    max_sun   = max(norm (Eph_sun  (i,1:3)-Eph_Ref(i,1:3)), max_sun);
    max_moon  = max(norm (Eph_moon (i,1:3)-Eph_Ref(i,1:3)), max_moon);
    max_sRad  = max(norm (Eph_sRad (i,1:3)-Eph_Ref(i,1:3)), max_sRad);
    max_drag  = max(norm (Eph_drag (i,1:3)-Eph_Ref(i,1:3)), max_drag);
    max_planets = max(norm (Eph_planets (i,1:3)-Eph_Ref(i,1:3)), max_planets);
    max_SolidEarthTides = max(norm (Eph_SolidEarthTides (i,1:3)-Eph_Ref(i,1:3)), max_SolidEarthTides);
    max_OceanTides = max(norm (Eph_OceanTides (i,1:3)-Eph_Ref(i,1:3)), max_OceanTides);
    max_relativity = max(norm (Eph_relativity (i,1:3)-Eph_Ref(i,1:3)), max_relativity);
end

% output
fprintf('Geostationary satellite: \n\n');
fprintf('  maximum position errors within ');
fprintf('%5.1f', N_Step1*Step/60 );
fprintf(' min propagation interval \n\n');
fprintf('    J2,0    J2,2    J4,4  J10,10');
fprintf('     Sun    Moon  SolRad    Drag     planets  SolidTides  OceanTides  relativity\n');
fprintf('     [m]     [m]     [m]     [m]');
fprintf('     [m]     [m]     [m]     [m]         [m]         [m]         [m]         [m]\n');
fprintf('%8.1f%8.1f%8.1f%8.1f%8.1f%8.1f%8.1f%8.1f%12.1f%12.1f%12.1f%12.1f\n\n', ...
        max_J20, max_J22, max_J44, max_J1010, max_sun, max_moon, max_sRad, ...
        max_drag, max_planets, max_SolidEarthTides, max_OceanTides, max_relativity);

% find maximum over N_Step2 steps
for i=N_Step1+1:N_Step2
    max_J20   = max(norm (Eph_J20  (i,1:3)-Eph_Ref(i,1:3)), max_J20);
    max_J22   = max(norm (Eph_J22  (i,1:3)-Eph_Ref(i,1:3)), max_J22);
    max_J44   = max(norm (Eph_J44  (i,1:3)-Eph_Ref(i,1:3)), max_J44);
    max_J1010 = max(norm (Eph_J1010(i,1:3)-Eph_Ref(i,1:3)), max_J1010);
    max_sun   = max(norm (Eph_sun  (i,1:3)-Eph_Ref(i,1:3)), max_sun);
    max_moon  = max(norm (Eph_moon (i,1:3)-Eph_Ref(i,1:3)), max_moon);
    max_sRad  = max(norm (Eph_sRad (i,1:3)-Eph_Ref(i,1:3)), max_sRad);
    max_drag  = max(norm (Eph_drag (i,1:3)-Eph_Ref(i,1:3)), max_drag);
    max_planets = max(norm (Eph_planets (i,1:3)-Eph_Ref(i,1:3)), max_planets);
    max_SolidEarthTides = max(norm (Eph_SolidEarthTides (i,1:3)-Eph_Ref(i,1:3)), max_SolidEarthTides);
    max_OceanTides = max(norm (Eph_OceanTides (i,1:3)-Eph_Ref(i,1:3)), max_OceanTides);
    max_relativity = max(norm (Eph_relativity (i,1:3)-Eph_Ref(i,1:3)), max_relativity);
end

% output
fprintf('  maximum position errors within ');
fprintf('%5.1f', N_Step2*Step/60);
fprintf(' min propagation interval \n\n');
fprintf('    J2,0    J2,2    J4,4  J10,10');
fprintf('     Sun    Moon  SolRad    Drag     planets  SolidTides  OceanTides  relativity\n');
fprintf('     [m]     [m]     [m]     [m]');
fprintf('     [m]     [m]     [m]     [m]         [m]         [m]         [m]         [m]\n');
fprintf('%8.1f%8.1f%8.1f%8.1f%8.1f%8.1f%8.1f%8.1f%12.1f%12.1f%12.1f%12.1f\n\n', ...
        max_J20, max_J22, max_J44, max_J1010, max_sun, max_moon, max_sRad, ...
        max_drag, max_planets, max_SolidEarthTides, max_OceanTides, max_relativity);

