%--------------------------------------------------------------------------
%
% RTOD: Real Time Orbit Determination based on GPS navigation data 
%
% Last modified:   2018/01/27   M. Mahooti
% 
%--------------------------------------------------------------------------
clc
clear
format long g

global const PC Cnm Snm AuxParam eopdata

SAT_Const
load DE430Coeff.mat
PC = DE430Coeff;

% read Earth gravity field coefficients
Cnm = zeros(181,181);
Snm = zeros(181,181);
fid = fopen('GGM03S.txt','r');
for n=0:180
    for m=0:n
        temp = fscanf(fid,'%d %d %f %f %f %f',[6 1]);
        Cnm(n+1,m+1) = temp(3);
        Snm(n+1,m+1) = temp(4);
    end
end
fclose(fid);

% read Earth orientation parameters
fid = fopen('eop19620101.txt','r');
%  ----------------------------------------------------------------------------------------------------
% |  Date    MJD      x         y       UT1-UTC      LOD       dPsi    dEpsilon     dX        dY    DAT
% |(0h UTC)           "         "          s          s          "        "          "         "     s 
%  ----------------------------------------------------------------------------------------------------
eopdata = fscanf(fid,'%i %d %d %i %f %f %f %f %f %f %f %f %i',[13 inf]);
fclose(fid);

Obs = struct('Mjd_GPS',0,'r_obs',[0 0 0],'v_obs',[0 0 0],'r_ref',[0 0 0],'v_ref',[0 0 0]);

% model parameters
AuxParam = struct ('Mjd_UTC',0,'area_solar',0,'area_drag',0,...
                   'mass',0,'Cr',0,'Cd',0,'n',0,'m',0,'sun',0,'moon',0,...
                   'sRad',0,'drag',0,'planets',0,'SolidEarthTides',0,...
                   'OceanTides',0,'Relativity',0,'n_G',0,'m_G',0,'n_a',0,'m_a',0);

dxdY = [1,0,0,0,0,0]; % partials
dydY = [0,1,0,0,0,0];
dzdY = [0,0,1,0,0,0];

% control parameters from setup file
fid = fopen('RTOD_A.txt','r');
tline = fgetl(fid);
n_grav = str2num(tline(31:35));
tline = fgetl(fid);
Step = str2num(tline(31:35));
tline = fgetl(fid);
sig_xyz = str2num(tline(31:35));
tline = fgetl(fid);
sig_pos = str2num(tline(31:35));
tline = fgetl(fid);
sig_vel = str2num(tline(31:35));
tline = fgetl(fid);
w_pos = str2num(tline(31:37));
tline = fgetl(fid);
w_vel = str2num(tline(31:40));
tline = fgetl(fid);
EditLevel = str2num(tline(31:37));
fclose(fid);

% open GPS data input file, skip header and read first observation
fid = fopen('RTOD.dat','r');

tline = fgetl(fid);

for i=1:285
    tline = fgetl(fid);
    Y = str2num(tline(1:4));
    M = str2num(tline(6:7));
    D = str2num(tline(9:10));
    h = str2num(tline(13:14));
    m = str2num(tline(16:17));
    s = str2num(tline(19:24));
    r_obs(1) = str2num(tline(25:37));
    r_obs(2) = str2num(tline(38:48));
    r_obs(3) = str2num(tline(49:59));
    v_obs(1) = str2num(tline(60:72));
    v_obs(2) = str2num(tline(73:83));
    v_obs(3) = str2num(tline(84:94));
    r_ref(1) = str2num(tline(95:107));
    r_ref(2) = str2num(tline(108:118));
    r_ref(3) = str2num(tline(119:129));
    v_ref(1) = str2num(tline(130:142));
    v_ref(2) = str2num(tline(143:153));
    v_ref(3) = str2num(tline(154:164));
    Obs(i).Mjd_GPS = Mjday(Y,M,D,h,m,s);
    Obs(i).r_obs   = r_obs;
    Obs(i).v_obs   = v_obs;
    Obs(i).r_ref   = r_ref;
    Obs(i).v_ref   = v_ref;
end

fclose(fid);

% reference epoch (GPS time)
[x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,Obs(1).Mjd_GPS,'l');
[UT1_TAI, UTC_GPS, UT1_GPS, TT_UTC, GPS_UTC] = timediff(UT1_UTC, TAI_UTC);

Mjd0 = Obs(1).Mjd_GPS;

AuxParam.Mjd_UTC = Obs(1).Mjd_GPS + UTC_GPS/86400;
AuxParam.area_solar = 5; % [m^2] remote sensing satellite
AuxParam.area_drag  = 5; % [m^2] remote sensing satellite
AuxParam.mass    = 1000; % [kg]
AuxParam.Cr      = 1.3;
AuxParam.Cd      = 2.3;
AuxParam.n       = n_grav;
AuxParam.m       = n_grav;
AuxParam.n_a     = n_grav;
AuxParam.m_a     = n_grav;
AuxParam.n_G     = n_grav;
AuxParam.m_G     = n_grav;
AuxParam.sun     = 1;
AuxParam.moon    = 1;
AuxParam.planets = 0;
AuxParam.sRad    = 0;
AuxParam.drag    = 0;
AuxParam.SolidEarthTides = 1;
AuxParam.OceanTides = 0;
AuxParam.Relativity = 0;

% transformation from WGS to ICRS
Mjd_UT1 = Mjd0 + UT1_GPS/86400;
Mjd_TT  = Mjd_UT1 + (TT_UTC-UT1_UTC)/86400;

S = zeros(3);
S(1,2) = 1;
S(2,1) = -1;
P      = PrecMatrix(const.MJD_J2000,Mjd_TT); % IAU 1976 Precession
N      = NutMatrix(Mjd_TT);                  % IAU 1980 Nutation
Theta  = GHAMatrix(Mjd_UT1);                 % Earth rotation matrix
dTheta = const.omega_Earth*S*Theta;          % and derivative [1/s]
Pi     = PoleMatrix(x_pole,y_pole);          % Polar motion
U      = Pi*Theta*N*P;                       % ICRS to ITRS transformation
dU     = Pi*dTheta*N*P;                      % Derivative [1/s]

r = U'*Obs(1).r_obs';
v = U'*Obs(1).v_obs' + dU'*Obs(1).r_obs';
Y = [r;v];

% a priori covariance
SigY = [sig_pos,sig_pos,sig_pos,sig_vel,sig_vel,sig_vel];
P = diag(SigY);                    
P = P*P;

% header
fprintf('GPS/MET Real Time Orbit Determination\n\n');
fprintf(' Initial Epoch:           ');
[year,mon,day,hr,min,sec] = invjday(Obs(1).Mjd_GPS+2400000.5);
fprintf('%d/%2.2d/%2.2d ', year,mon,day);
fprintf('%2.2d:%2.2d:%.3f\n', hr,min,sec);
fprintf(' Inertial position:    ');
fprintf('%15.3f', r);
fprintf('\n Inertial velocity:    ');
fprintf('%15.6f', v);
fprintf('\n\n Gravity field order:  ');
fprintf('%.1f\n', AuxParam.n);
fprintf(' Step size:            ');
fprintf('%.1f', Step);
fprintf(' s\n');
fprintf(' Measurement sigma:    ');
fprintf('%.1f', sig_xyz);
fprintf(' m\n');
fprintf(' A priori pos. sigma:  ');
fprintf('%.1f', sig_pos);
fprintf(' m\n');
fprintf(' A priori vel. sigma:  ');
fprintf('%.4f', sig_vel);
fprintf(' m/s\n');
fprintf(' State noise position: ');
fprintf('%.1f', w_pos);
fprintf(' m\n');
fprintf(' State noise velocity: ');
fprintf('%.4f', w_vel);
fprintf(' m/s\n');
fprintf(' Edit level (sigma):   ');
fprintf('%.1f\n\n\n', EditLevel);
fprintf('   Time      x(WGS)     y(WGS)     z(WGS)');
fprintf('       Position [m]        Velocity [m/s]      Semi-major axis [m] \n');
fprintf('    [s]        [m]        [m]        [m] ');
fprintf('     Meas. Sigma  Estim. Meas.  Sigma  Estim.  Meas. Sigma  Estim.\n')

yPhi = zeros(42,1);
Phi  = zeros(6);
t = 0;
options = rdpset('RelTol',1e-13,'AbsTol',1e-16);

% measurement loop
for i=2:285    
    % previous step
    t_old = t;
    Y_old = Y;
    
    % propagation to measurement epoch
    t = (Obs(i).Mjd_GPS-Mjd0)*86400; % time since epoch [s]
    
    for ii=1:6
        yPhi(ii) = Y_old(ii);
        for j=1:6
            if (ii==j)
                yPhi(6*j+ii) = 1;
            else
                yPhi(6*j+ii) = 0;
            end
        end
    end
    
    yPhi = DEInteg(@VarEqn,0,t-t_old,1e-13,1e-6,42,yPhi);
    
    % extract state transition matrices
    for j=1:6
        Phi(:,j) = yPhi(6*j+1:6*j+6);
    end
    
    % process noise
    SigY = [w_pos,w_pos,w_pos,w_vel,w_vel,w_vel];
    Q = diag(SigY);
    Q = Q*Q;
    
    % integration to time of measurement  
    Y = Y_old;
    [~,yout] = radau(@Accel,[t_old t],Y,options);
    Y = yout(end,:)';
    
    % time update
    P = TimeUpdate(P, Phi, Q);
    
    % Earth rotation matrix and derivative
    [x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,Obs(i).Mjd_GPS,'l');
    [UT1_TAI, UTC_GPS, UT1_GPS, TT_UTC, GPS_UTC] = timediff(UT1_UTC, TAI_UTC);
    Mjd_UT1 = Obs(i).Mjd_GPS + UT1_GPS/86400.0;
    Mjd_TT  = Mjd_UT1 + (TT_UTC-UT1_UTC)/86400.0;
    
    Pr     = PrecMatrix(const.MJD_J2000,Mjd_TT); % IAU 1976 Precession
    N      = NutMatrix(Mjd_TT);                  % IAU 1980 Nutation
    Theta  = GHAMatrix(Mjd_UT1);                 % Earth rotation matrix
    dTheta = const.omega_Earth*S*Theta;          % and derivative [1/s]
    Pi     = PoleMatrix(x_pole,y_pole);          % Polar motion
    U      = Pi*Theta*N*Pr;                      % ICRS to ITRS transformation
    dU     = Pi*dTheta*N*Pr;                     % Derivative [1/s]
    
    % transformation of measurements to inertial system
    r = U'*Obs(i).r_obs';
    
    % data editing limit 
    Reject = (norm(r-Y(1:3)) > EditLevel*sig_xyz);
    
    % measurement updates
    if ( ~Reject )
        [K, Y, P] = MeasUpdate_(Y, r(1), Y(1), sig_xyz, dxdY, P, 6);
        [K, Y, P] = MeasUpdate_(Y, r(2), Y(2), sig_xyz, dydY, P, 6);
        [K, Y, P] = MeasUpdate_(Y, r(3), Y(3), sig_xyz, dzdY, P, 6);
    end
    
    % estimated state vector in rotating, Earth-fixed system
    r_est = U*Y(1:3);
    v_est = U*Y(4:6) + dU*Y(1:3);
    
    % semi-major axis
    r = U'*Obs(i).r_obs';
    v = U'*Obs(i).v_obs' + dU'*Obs(i).r_obs';
    a_obs = 1/(2/norm(r)-dot(v,v)/const.GM_Earth);
    
    r = U'*Obs(i).r_ref';
    v = U'*Obs(i).v_ref' + dU'*Obs(i).r_ref';
    a_ref = 1/(2/norm(r)-dot(v,v)/const.GM_Earth);
    
    r = Y(1:3);
    v = Y(4:6);
    a_est = 1/(2/norm(r)-dot(v,v)/const.GM_Earth);
    dadY = [(2*(a_est^2)/(norm(r)^3))*r; (2*(a_est^2)/const.GM_Earth)*v]';
    Sig_a = sqrt(dot(dadY*P,dadY));
    
    % output
    fprintf('%7.1f', t);                               % time
    fprintf('%12.1f', r_est);                          % estimated position
    fprintf('%7.1f', norm(Obs(i).r_obs-Obs(i).r_ref)); % measurement errors
    
    if (Reject)                                        % data editing flag
        fprintf('*');
    else
        fprintf(' ');
    end
    
    STD = EKFStdDev(P,6);
    fprintf('%6.1f', norm(STD(1:3)));                  % standard deviation
    fprintf('%6.1f', norm(r_est-Obs(i).r_ref'));       % oosition error
    fprintf('%8.3f', norm(Obs(i).v_obs-Obs(i).v_ref)); % measurement errors
    fprintf('%7.3f', norm(STD(4:6)));                  % standard deviation
    fprintf('%7.3f', norm(v_est-Obs(i).v_ref'));       % velocity error
    fprintf('%8.1f', abs(a_obs-a_ref));                % measurement errors
    fprintf('%7.1f', Sig_a);                           % standard deviation
    fprintf('%6.1f\n', abs(a_est-a_ref));              % semi-major axis error
end

