%--------------------------------------------------------------------------
%
% InvSymp: Inverts a symplectic matrix
%
% Input:
%   A         Sympletctic (even-dimensional square) matrix
% 
% output:     Inverse of A
%
%                      ( +(A_22)^T  -(A_12)^T )            ( A_11  A_12 )
%             A^(-1) = (                      )  with  A = (            )
%                      ( -(A_21)^T  +(A_11)^T )            ( A_21  A_22 )
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
function Inv = InvSymp (A)

[N, M] = size(A);
n = N/2;
Inv = zeros(N);

if (M~=N || 2*n~=N )
    fprintf('ERROR: Invalid shape in InvSymp(Matrix)');
    exit(1);
end

for i=1:n
    for j=1:n
        Inv(i  ,j  ) =  A(j+n,i+n);    
        Inv(i  ,j+n) = -A(j  ,i+n);
        Inv(i+n,j  ) = -A(j+n,i  );
        Inv(i+n,j+n) =  A(j  ,i  );
    end
end

