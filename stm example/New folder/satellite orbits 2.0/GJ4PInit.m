%--------------------------------------------------------------------------
%
% Initialization of backwards differences from initial conditions
%
%--------------------------------------------------------------------------
function [S1, S2, D] = GJ4PInit(t, r_0, v_0, h)

% Order of method
m = 4;

D = zeros(4,3);        % Backward differences of acceleration at t
d = zeros(4,3);        % Backward differences of acceleration at t+h

% Coefficients gamma/delta of 1st/2nd order Moulton/Cowell corrector method
gc = [+1.0, -1/2.0, -1/12.0, -1/24.0, -19/720.0];
dc = [+1.0,   -1.0, +1/12.0,     0.0,  -1/240.0, -1/240.0];

r0 = r_0;
v0 = v_0;

% Create table of accelerations at past times t-3h, t-2h, and t-h using
% RK4 steps
D(1,:) = f_Kep3D(t,r_0,v_0);     % D(i)=a(t-ih)
for i=2:m
    [r,v] = RK4_(t,r_0,v_0,-h);
    r_0 = r;
    v_0 = v;
    t = t-h;
    D(i,:) = f_Kep3D(t,r_0,v_0);
end

% Compute backwards differences
for i=2:m
    for j=m:-1:i
        D(j,:) = D(j-1,:)-D(j,:);
    end
end

% Initialize backwards sums using 4th order GJ corrector
S1 = v0/h;
for i=2:m+1
    S1 = S1-gc(i)*D(i-1,:);
end

S2 = r0/(h*h)-dc(2)*S1;
for i=3:m+2
    S2 = S2-dc(i)*D(i-2,:);
end

