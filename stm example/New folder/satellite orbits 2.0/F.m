%--------------------------------------------------------------------------
%
%    F : local function for use by FindEta()
%    F = 1 - eta +(m/eta**2)*W(m/eta**2-l)
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
function ou = F(eta, m, l)

epsilon = 100*eps;

w = m/(eta*eta)-l;

if (abs(w)<0.1)  % Series expansion
    W = 4/3;
    a = 4/3;
    n = 0.0;    
    while (1)
        n = n + 1;
        a = a*(w*(n+2)/(n+1.5));
        W = W + a;
        if (abs(a) < epsilon)
            break
        end
    end    
elseif (w > 0)
    g = 2*asin(sqrt(w));
    W = (2*g - sin(2*g))/(sin(g)^3);
else
    g = 2*log(sqrt(-w)+sqrt(1-w));  % =2.0*arsinh(sqrt(-w))
    W = (sinh(2*g) - 2*g)/(sinh(g)^3);
end

ou = (1 - eta + (w+l)*W);

