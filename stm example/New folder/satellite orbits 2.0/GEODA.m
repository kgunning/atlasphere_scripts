%--------------------------------------------------------------------------
%
% GEODA: Geostationary satellite Orbit Determination error Analysis
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

global R d

SAT_Const

% Input
fid = fopen('GEODA_A1.txt','r');
tline = fgetl(fid);
lon = str2num(tline(51:58));
tline = fgetl(fid);
lam = str2num(tline(51:58));
phi = str2num(tline(59:65));
h_Sta1 = str2num(tline(66:72));
lon_Sta1 = const.Rad*(lam-lon);
lat_Sta1 = const.Rad*phi;
tline = fgetl(fid);
Sta1_ang_sig_noise = str2num(tline(51:59));
Sta1_ang_sig_bias = str2num(tline(60:65));
Sta1_ang_step = str2num(tline(66:69));
Sta1_ang_est_bias = str2num(tline(70:72));
tline = fgetl(fid);
Sta1_rng_sig_noise = str2num(tline(51:58));
Sta1_rng_sig_bias = str2num(tline(59:64));
Sta1_rng_step = str2num(tline(65:69));
Sta1_rng_est_bias = str2num(tline(70:72));
tline = fgetl(fid);
lam = str2num(tline(51:58));
phi = str2num(tline(59:65));
h_Sta2 = str2num(tline(66:72));
lon_Sta2 = const.Rad*(lam-lon);
lat_Sta2 = const.Rad*phi;
tline = fgetl(fid);
Sta2_ang_sig_noise = str2num(tline(51:59));
Sta2_ang_sig_bias = str2num(tline(60:65));
Sta2_ang_step = str2num(tline(66:69));
Sta2_ang_est_bias = str2num(tline(70:72));
tline = fgetl(fid);
Sta2_rng_sig_noise = str2num(tline(51:58));
Sta2_rng_sig_bias = str2num(tline(59:64));
Sta2_rng_step = str2num(tline(65:69));
Sta2_rng_est_bias = str2num(tline(70:72));
tline = fgetl(fid);
T_track = str2num(tline(51:58));
tline = fgetl(fid);
T_pred = str2num(tline(51:58));
fclose(fid);

% Change units
lon = lon*const.Rad; 
Sta1_ang_sig_noise = Sta1_ang_sig_noise*const.Rad;
Sta1_ang_sig_bias = Sta1_ang_sig_bias*const.Rad;
Sta2_ang_sig_noise = Sta2_ang_sig_noise*const.Rad;
Sta2_ang_sig_bias = Sta2_ang_sig_bias*const.Rad;

% Define bias parameter indizes 
% (estimation parameters first, consider parameters last)

n_orb  = 6;              % Number of dynamical parameters
n_bias = 6;              % Number of bias parameters
n_dim  = n_orb + n_bias; % Dimension of least squares system

a = 42164000;            % Geostationary radius [m]
n = const.pi2*1.0027/24; % Mean angular velocity [rad/h]

dady = [4,0,0,0,2/n,0];  % Partials of semi-major axis
                         % w.r.t. state vector

Var_b = zeros(n_bias,1); % Bias parameter variance

dhdb = zeros(n_bias,1);  % Partials wrt. bias parameters

n_est =  6;   % Estimation parameter count (default: state vector only)
n_con =  0;   % Consider parameter count
k     =  0;   % Bias parameter index

if ( Sta1_ang_est_bias)
    Sta1_ang_index=k;
    k = k+2;
    n_est = n_est+2;
end
if (Sta1_rng_est_bias)
    Sta1_rng_index=k;
    k = k+1;
    n_est = n_est+1;
end
if (Sta2_ang_est_bias)
    Sta2_ang_index=k;
    k = k+2;
    n_est= n_est+2;
end
if (Sta2_rng_est_bias)
    Sta2_rng_index=k;
    k = k+1;
    n_est= n_est+1;
end

if (~Sta1_ang_est_bias)
    Sta1_ang_index=k;
    k= k+2;
    n_con = n_con+2;
end
if (~Sta1_rng_est_bias)
    Sta1_rng_index=k;
    k = k+1;
    n_con = n_con+1;
end
if (~Sta2_ang_est_bias)
    Sta2_ang_index=k;
    k = k+2;
    n_con = n_con+2;
end
if (~Sta2_rng_est_bias)
    Sta2_rng_index=k;
    k = k+1;
    n_con = n_con+1;
end

% Bias parameter variance
Var_b(Sta1_ang_index+1) = (Sta1_ang_sig_bias^2);  % Azimuth bias station 1
Var_b(Sta1_ang_index+2) = (Sta1_ang_sig_bias^2);  % Elevation bias station 1
Var_b(Sta1_rng_index+1) = (Sta1_rng_sig_bias^2);  % Range bias station 1
Var_b(Sta2_ang_index+1) = (Sta2_ang_sig_bias^2);  % Azimuth bias station 2
Var_b(Sta2_ang_index+2) = (Sta2_ang_sig_bias^2);  % Elevation bias station 2
Var_b(Sta2_rng_index+1) = (Sta2_rng_sig_bias^2);  % Range bias station 2

% Consider parameter covariance
C = diag(Var_b(n_est-n_orb+1:n_bias));

% Partial derivatives of inertial w.r.t. rotating state vector
U = eye(6); U(5,1) = +n; U(4,2) = -n;

% Header
fprintf('GEO Orbit Determination Error Analysis\n\n');
fprintf('  Setup parameters:\n\n');
fprintf('  Subsatellite longitude [deg]                   :');
fprintf('%9.3f\n', const.Deg*lon);
fprintf('  Station 1 (lon [deg], lat [deg], alt [m])      :');
fprintf('%9.3f%9.3f%9.3f\n', const.Deg*(lon_Sta1+lon),const.Deg*lat_Sta1,h_Sta1);
fprintf('  Angles (noise & bias [deg], step [h], est_bias):');
fprintf('%9.3f%9.3f', const.Deg*Sta1_ang_sig_noise,const.Deg*Sta1_ang_sig_bias);
fprintf('%9.3f%9.3f\n', Sta1_ang_step,Sta1_ang_est_bias);
fprintf('  Range (noise & bias [deg], step [h], est_bias) :');
fprintf('%9.3f%9.3f', Sta1_rng_sig_noise,Sta1_rng_sig_bias);
fprintf('%9.3f%9.3f\n', Sta1_rng_step,Sta1_rng_est_bias);
fprintf('  Station 2 (lon [deg], lat [deg], alt [m])      :');
fprintf('%9.3f%9.3f%9.3f\n', const.Deg*(lon_Sta2+lon),const.Deg*lat_Sta2,h_Sta2);
fprintf('  Angles (noise & bias [deg], step [h], est_bias):');
fprintf('%9.3f%9.3f', const.Deg*Sta2_ang_sig_noise,const.Deg*Sta2_ang_sig_bias);
fprintf('%9.3f%9.3f\n', Sta2_ang_step,Sta2_ang_est_bias);
fprintf('  Range (noise & bias [deg], step [h], est_bias) :');
fprintf('%9.3f%9.3f', Sta2_rng_sig_noise,Sta2_rng_sig_bias);
fprintf('%9.3f%9.3f\n', Sta2_rng_step,Sta2_rng_est_bias);
fprintf('  Tracking interval [h]                          :');
fprintf('%9.3f\n', T_track);
fprintf('  Prediction interval [h]                        :');
fprintf('%9.3f\n\n', T_pred);

% Initialization of least squares system
R = zeros(n_dim);
d = zeros(n_dim,1);

% Process partials of measurements from both stations

% Azimuth, elevation, range and partials
E = LTCMatrix(lon_Sta1, lat_Sta1);
r = [a,0,0] - Position(lon_Sta1, lat_Sta1, h_Sta1);
[Az, El, dAds, dEds] = AzElPa(E*r');
D=norm(r); 
dAdr=dAds*E;
dEdr=dEds*E;
dDdr=r/D;

% Angle measurements
t = 0;
if (Sta1_ang_step>0)
    while(1)
        % Partials of azimuth measurement
        Ph = Phi(n,t);
        dhdy = dAdr*Ph(1:3,1:6);         % Partials w.r.t. epoch state
        dhdb = zeros(1,n_bias);
        dhdb(Sta1_ang_index+1) = 1;      % Partials w.r.t. biases
        % Add partials to least squares system
        LSQAccumulate (n_dim, [dhdy,dhdb], 0, Sta1_ang_sig_noise);
        % Partials of elevation measurement
        Ph = Phi(n,t);
        dhdy = dEdr*Ph(1:3,1:6);         % Partials w.r.t. epoch state
        dhdb = zeros(1,n_bias);
        dhdb(Sta1_ang_index+2) = 1;      % Partials w.r.t. biases
        % Add partials to least squares system
        LSQAccumulate (n_dim, [dhdy,dhdb], 0, Sta1_ang_sig_noise);
        % Increment time of measurement and exit if past end of data arc
        t = t + Sta1_ang_step;
        if (t>T_track)
            break
        end
    end
end

% Range measurements
t = 0;
if (Sta1_rng_step>0)
    while(1)
        % Partials of range measurement
        Ph = Phi(n,t);
        dhdy = dDdr*Ph(1:3,1:6);         % Partials w.r.t. epoch state
        dhdb = zeros(1,n_bias);
        dhdb(Sta1_rng_index+1) = 1;      % Partials w.r.t. biases
        LSQAccumulate (n_dim, [dhdy,dhdb], 0, Sta1_rng_sig_noise);
        % Increment time of measurements and exit if past end of data arc
        t = t + Sta1_rng_step;
        if (t>T_track)
            break
        end
    end
end

% Azimuth, elevation, range and partials
E = LTCMatrix(lon_Sta2, lat_Sta2);
r = [a,0,0] - Position(lon_Sta2, lat_Sta2, h_Sta2);
[Az, El, dAds, dEds] = AzElPa(E*r');
D=norm(r); 
dAdr=dAds*E;
dEdr=dEds*E;
dDdr=r/D;

% Angle measurements
t = 0;
if (Sta2_ang_step>0)
    while(1)
        % Partials of azimuth measurement
        Ph = Phi(n,t);
        dhdy = dAdr*Ph(1:3,1:6);         % Partials w.r.t. epoch state
        dhdb = zeros(1,n_bias);
        dhdb(Sta2_ang_index+1) = 1;      % Partials w.r.t. biases
        % Add partials to least squares system
        LSQAccumulate (n_dim, [dhdy,dhdb], 0, Sta2_ang_sig_noise);
        % Partials of elevation measurement
        Ph = Phi(n,t);
        dhdy = dEdr*Ph(1:3,1:6);         % Partials w.r.t. epoch state
        dhdb = zeros(1,n_bias);
        dhdb(Sta2_ang_index+2) = 1;      % Partials w.r.t. biases
        % Add partials to least squares system
        LSQAccumulate (n_dim, [dhdy,dhdb], 0, Sta2_ang_sig_noise);
        % Increment time of measurement and exit if past end of data arc
        t = t + Sta2_ang_step;
        if (t>T_track)
            break
        end
    end
end

% Range measurements
t = 0;
if (Sta2_rng_step>0)
    while(1)
        % Partials of range measurement
        Ph = Phi(n,t);
        dhdy = dDdr*Ph(1:3,1:6);         % Partials w.r.t. epoch state
        dhdb = zeros(1,n_bias);
        dhdb(Sta2_rng_index+1) = 1;      % Partials w.r.t. biases
        LSQAccumulate (n_dim, [dhdy,dhdb], 0, Sta2_rng_sig_noise);
        % Increment time of measurements and exit if past end of data arc
        t = t + Sta2_rng_step;
        if (t>T_track)
            break
        end
    end
end

% Decomposition of square root information matrix
R_xx = R(1:n_est,1:n_est);                    % Square root inf. matrix
                                              % of estimated parameters
R_xc = R(1:n_est,n_est+1:n_dim);              % Transformed partials  
                                              % wrt. consider parameters
% Computed covariance
R_xx_inv = InvUpper(R_xx);                    % Inverse of SRIM
P_x  = R_xx_inv*R_xx_inv';                    % Estim. parameter covariance
P_y0 = P_x(1:6,1:6);                          % Epoch state covariance
P_a  = dot(dady*P_y0,dady);                   % Semi-major axis covariance
P_y  = U*P_y0*U';                             % Inertial state covariance

% Output
fprintf('Epoch state standard deviation (unmodelled effects ignored)\n\n');
temp = sqrt(diag(P_y));
fprintf('Position (xyz)');
fprintf('%10.2f', temp(1:3));
fprintf(' [m]\n');
fprintf('Velocity (xyz)');
fprintf('%10.5f', temp(4:6)/3600);
fprintf(' [m/s]\n\n');
fprintf('Semi-major axis');
fprintf('%8.2f', sqrt(P_a));
fprintf(' [m]\n\n');

% Consider covariance
Pc_x = P_x + R_xx_inv*R_xc*C*R_xc'*R_xx_inv';
P_y0 = Pc_x(1:6,1:6);                         % Epoch state covariance
P_a  = dot(dady*P_y0,dady);                   % Semi-major axis covariance
P_y  = U*P_y0*U';                             % Inertial state covariance

% Output
fprintf('Epoch state standard deviation (including unmodelled effects)\n\n');
temp = sqrt(diag(P_y));
fprintf('Position (xyz)');
fprintf('%10.2f', temp(1:3));
fprintf(' [m]\n');
fprintf('Velocity (xyz)');
fprintf('%10.5f', temp(4:6)/3600);
fprintf(' [m/s]\n\n');
fprintf('Semi-major axis');
fprintf('%8.2f', sqrt(P_a));
fprintf(' [m]\n\n');

% Debug print 
fprintf('Effect of unestimated parameters\n\n');
for i=1:n_con
    temp1 = R_xx_inv*R_xc;
    temp2 = sqrt(Var_b(n_est-n_orb+i));
    temp3 = temp1(:,i)*temp2;
    fprintf('%10.3f', -temp3(1:3));
    fprintf('\n');
end
fprintf('\n');

% Position/velocity standard deviation 
fprintf('Position/velocity standard deviation (including unmodelled effects)\n\n'); 
fprintf('                   Position                      Velocity         \n'); 
fprintf('  Time    radial     tang     normal    radial     tang     normal\n');
fprintf('   [h]      [m]       [m]       [m]      [m/s]     [m/s]     [m/s]\n' );

t = 0;

while(1)
    % Transition matrix
    T = Phi(n,t);
    % Covariance 
    P_y = U*T*P_y0*T'*U';
    % Output
    fprintf('%6.1f', t);
    temp = sqrt(diag(P_y));
    fprintf('%10.1f', temp(1:3));
    fprintf('%10.4f', temp(4:6)/3600);
    fprintf('\n');
    % Increment time and exit if past end of prediction arc
    t = t+3;
    if (t>T_pred)
        break
    end
end

