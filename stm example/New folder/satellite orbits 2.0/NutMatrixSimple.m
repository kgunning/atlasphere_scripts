%--------------------------------------------------------------------------
%
% NutMatrixSimple: Transformation from mean to true equator and equinox
%                  (low precision)
%
% Input:
%   Mjd_TT      Modified Julian Date (Terrestrial Time)
%
% Output:
%   NutMatSimp  Nutation matrix
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
function NutMatSimp = NutMatrixSimple(Mjd_TT)

global const

% Constants
T  = (Mjd_TT-const.MJD_J2000)/36525;

% Mean arguments of luni-solar motion
ls = const.pi2*Frac(0.993133+  99.997306*T); % mean anomaly Sun          
D  = const.pi2*Frac(0.827362+1236.853087*T); % diff. longitude Moon-Sun  
F  = const.pi2*Frac(0.259089+1342.227826*T); % mean argument of latitude 
N  = const.pi2*Frac(0.347346-   5.372447*T); % longit. ascending node    

% Nutation angles
dpsi = ( -17.200*sin(N)   - 1.319*sin(2*(F-D+N)) - 0.227*sin(2*(F+N)) ...
         + 0.206*sin(2*N) + 0.143*sin(ls) ) / Arcs;
deps = ( + 9.203*cos(N)   + 0.574*cos(2*(F-D+N)) + 0.098*cos(2*(F+N)) ...
         - 0.090*cos(2*N)                 ) / Arcs;

% Mean obliquity of the ecliptic
eps  = 0.4090928-2.2696e-4*T;    

NutMatSimp = R_x(-eps-deps)*R_z(-dpsi)*R_x(+eps);

