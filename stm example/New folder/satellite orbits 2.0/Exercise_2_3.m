%--------------------------------------------------------------------------
%
%   Exercise 2-3: Osculating elements
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

SAT_Const

% Position and velocity 
r = [+10000e3,+40000e3, -5000e3];  % [m]
v = [  -1.500e3,  +1.000e3,  -0.100e3];  % [m/s]

% Orbital elements
y = [r,v];

Kep = Element(const.GM_Earth, y);

% Output
fprintf('Exercise 2-3: Osculating elements \n\n');

fprintf('State vector:\n\n');
fprintf('  Position       ');
for i=0:2
    fprintf('%12.3f', r(i+1)/1000);
end
fprintf('  [km]\n');
fprintf('  Velocity       ');
for i=0:2
    fprintf('%12.6f', v(i+1)/1000);
end
fprintf('  [km/s]\n');

fprintf('\nOrbital elements:\n\n');
fprintf('  Semimajor axis   %10.3f', Kep(1)/1000);
fprintf(' km\n' );
fprintf('  Eccentricity     %10.7f\n', Kep(2));
fprintf('  Inclination      %10.3f deg \n', Kep(3)*const.Deg);
fprintf('  RA ascend. node  %10.3f deg \n', Kep(4)*const.Deg);
fprintf('  Arg. of perigee  %10.3f deg \n', Kep(5)*const.Deg);
fprintf('  Mean anomaly     %10.3f deg \n', Kep(6)*const.Deg);

