%--------------------------------------------------------------------------
%
%   Exercise 6-4: Tropospheric Refraction
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

SAT_Const

% read Earth orientation parameters
fid = fopen('eop19620101.txt','r');
%  ----------------------------------------------------------------------------------------------------
% |  Date    MJD      x         y       UT1-UTC      LOD       dPsi    dEpsilon     dX        dY    DAT
% |(0h UTC)           "         "          s          s          "        "          "         "     s 
%  ----------------------------------------------------------------------------------------------------
eopdata = fscanf(fid,'%i %d %d %i %f %f %f %f %f %f %f %f %i',[13 inf]);
fclose(fid);

% Ground station
lon_Sta = 11*const.Rad; % [rad]
lat_Sta = 48*const.Rad; % [rad]
h_Sta   =  0;           % [m]

% Fixed media data at ground site
T0 =  273.2;            % Temperature at 0 deg C [K]
pa = 1024;              % Partial pressure of dry air [mb]
fh =    0.7;            % Relative humidity 

% Spacecraft orbit
MJD_Epoch = Mjday(1997,01,01); % Epoch

a     = 42164e3;               % Semimajor axis [m]
e     =   0.000296;            % Eccentricity
i     =  0.05*const.Rad;       % Inclination [rad]
Omega = 150.7*const.Rad;       % RA ascend. node [rad]
omega =   0;                   % Argument of latitude [rad]
M0    =   0;                   % Mean anomaly at epoch [rad]

Kep = [a,e,i,Omega,omega,M0];  % Keplerian elements

Tv = [303, 283];               % Temperature [K]

% Station
R_Sta = Position(lon_Sta, lat_Sta, h_Sta); % Geocentric position vector
E     = LTCMatrix(lon_Sta, lat_Sta);       % Transformation to
                                           % local tangent coordinates
% Header
fprintf('Exercise 6-4: Tropospheric Refraction\n\n');

% Orbit 
for Hour=0:8
    MJD_UTC = MJD_Epoch + 3*Hour/24;       % Modified Julian Date [UTC]
    [x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,MJD_UTC,'l');
    MJD_UT1 = MJD_UTC + UT1_UTC/86400;    
    dt = (MJD_UTC-MJD_Epoch)*86400;        % Time since epoch [s]
    Y = State(const.GM_Earth,Kep,dt);       
    r = Y(1:3);                            % Inertial position vector
    U = R_z(gmst(MJD_UT1));                % Earth rotation 
    s = E * ( U*r - R_Sta' );              % Topocentric position vector
    Dist = norm(s);                        % Distance
    [Azim, Elev] = AzEl(s);                % Azimuth, Elevation
    
    if (Hour==0)
        Elev0 = Elev;                      % Store initial elevation
        fprintf('E0 [deg]     %.3f\n\n', Elev0*const.Deg);
        fprintf('   Date         UTC          E-E0      dE_t1     dE_t2 \n');
        fprintf('yyyy/mm/dd  hh:mm:ss.sss     [deg]     [deg]     [deg]\n');
    end
    
    dElev = zeros(1,2);
    
    for Ti=1:2                                 % Evaluate at 2 temperatures
        T  = Tv(Ti);                           % Map to scalar 
        TC = T-T0;                             % Temperature [C]
        eh = 6.10*fh*exp(17.15*TC/(234.7+TC)); % Partial water pressure
        Ns = 77.64*pa/T + 3.734e5*eh/(T*T);    % Refractivity
        dElev(Ti) = Ns*1e-6/tan(Elev);         % Tropospheric refraction
    end
    
    [year,mon,day,hr,min,sec] = invjday(MJD_UTC+2400000.5);
    fprintf('%4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
    fprintf('%10.3f', (Elev-Elev0)*const.Deg);
    fprintf('%10.3f', dElev(1)*const.Deg);
    fprintf('%10.3f\n', dElev(2)*const.Deg);    
end

