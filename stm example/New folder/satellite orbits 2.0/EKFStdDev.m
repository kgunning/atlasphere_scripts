function Sigma = EKFStdDev(P,n)

% Standard deviation
Sigma = zeros(n);

for i=1:n
    Sigma(i) = sqrt(P(i,i));
end

end
