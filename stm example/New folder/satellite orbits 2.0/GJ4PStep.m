%--------------------------------------------------------------------------
%
% Step from t to t+h
%
%--------------------------------------------------------------------------
function [S1, S2, D, r, v] = GJ4PStep(S1, S2, D, t, h) 

% Order of method
m = 4;

% Coefficients gamma/delta of 1st/2nd order Bashforth/Stoermer predictor
gp = [+1.0, +1/2.0, +5/12.0,  +3/8.0, +251/720.0];
dp = [+1.0,    0.0, +1/12.0, +1/12.0,  +19/240.0,  +3/40.0];

% 4th order predictor
r_p = dp(1)*S2;
for i=3:m+2
    r_p = r_p+dp(i)*D(i-2,:);
end

r_p = (h*h)*r_p;

v_p = gp(1)*S1;
for i=2:m+1
    v_p = v_p+gp(i)*D(i-1,:);
end

v_p = h*v_p;

% Update backwards difference table
d(1,:) = f_Kep3D ( t+h, r_p, v_p );       % Acceleration at t+h

for i=2:m                                 % New differences at t+h
    d(i,:)=d(i-1,:)-D(i-1,:);
end

for i=1:m
    D(i,:)=d(i,:);               % Update differences
end

S1 = S1+d(1,:);                  % Update sums
S2 = S2+S1;

% Update independent variable and solution
r = r_p;
v = v_p;

