%--------------------------------------------------------------------------
%
%   Exercise 3-3: Accelerations
%   
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

global const eopdata swdata

SAT_Const

% read Earth orientation parameters
fid = fopen('eop19620101.txt','r');
%  ----------------------------------------------------------------------------------------------------
% |  Date    MJD      x         y       UT1-UTC      LOD       dPsi    dEpsilon     dX        dY    DAT
% |(0h UTC)           "         "          s          s          "        "          "         "     s 
%  ----------------------------------------------------------------------------------------------------
eopdata = fscanf(fid,'%i %d %d %i %f %f %f %f %f %f %f %f %i',[13 inf]);
fclose(fid);

% read space weather data
fid = fopen('sw19571001.txt','r');
%  ---------------------------------------------------------------------------------------------------------------------------------
% |                                                                                             Adj     Adj   Adj   Obs   Obs   Obs 
% | yy mm dd BSRN ND Kp Kp Kp Kp Kp Kp Kp Kp Sum Ap  Ap  Ap  Ap  Ap  Ap  Ap  Ap  Avg Cp C9 ISN F10.7 Q Ctr81 Lst81 F10.7 Ctr81 Lst81
%  ---------------------------------------------------------------------------------------------------------------------------------
swdata = fscanf(fid,'%4i %3d %3d %5i %3i %3i %3i %3i %3i %3i %3i %3i %3i %4i %4i %4i %4i %4i %4i %4i %4i %4i %4i %4f %2i %4i %6f %2i %6f %6f %6f %6f %6f',[33 inf]);
fclose(fid);

R_ref = 0.6378136300E+07; % Earth's radius [m]; GGM03S
r_Moon = 384400e3;        % Geocentric Moon distance [m]
J20_norm = 4.841e-4;      % Normalized geopotential coefficients
J22_norm = 2.812e-6;

h1  =  150e3;  % Start of interval [m]
h2  = 2000e3;  % Stop of interval [m]

fprintf('Exercise 3-3: Accelerations \n\n');

% Balance of accelerations due to drag and solar radiation pressure
h = Pegasus(@A_Diff, h1, h2);

fprintf(' a_DRG > a_SRP  for ');
fprintf(' %6.0f km\n', h/1000);

% Balance of accelerations due to J22 and Moon
r5 = 3/2 * const.GM_Earth/const.GM_Moon * (R_ref^2) * (r_Moon^3) * J22_norm;
r = r5^0.2;

fprintf(' a_J22 > a_Moon for ');
fprintf(' %6.0f km\n', (r - R_ref)/1000);

% Balance of accelerations due to J22 and Sun
r5 = 3/2 * const.GM_Earth/const.GM_Sun * (R_ref^2) * (const.AU^3) * J22_norm;
r = r5^0.2;

fprintf(' a_J22 > a_Sun  for ');
fprintf(' %6.0f km\n', (r - R_ref)/1000);

% Balance of accelerations due to J20 and Moon
r5 = 3/2 * const.GM_Earth/const.GM_Moon * (R_ref^2) * (r_Moon^3) * J20_norm;
r = r5^0.2;

fprintf(' a_J20 > a_Moon for ');
fprintf(' %6.0f km\n', (r - R_ref)/1000);

% Balance of accelerations due to J20 and Sun
r5 = 3/2 * const.GM_Earth/const.GM_Sun * (R_ref^2) * (const.AU^3) * J20_norm;
r = r5^0.2;

fprintf(' a_J20 > a_Sun  for ');
fprintf(' %6.0f km\n', (r - R_ref)/1000);

