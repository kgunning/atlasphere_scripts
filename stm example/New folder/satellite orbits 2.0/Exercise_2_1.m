%--------------------------------------------------------------------------
%
%   Exercise 2-1: Orbit raising using Hohmann transfer
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

SAT_Const

% Constants
h_1 = 750.0e3;     % Initial altitude [m]
h_2 = 775.0e3;     % Final   altitude [m]

r_1 = const.R_Earth+h_1; % Initial radius [m]
r_2 = const.R_Earth+h_2; % Final   radius [m]

% Circular velocities
v_1 = sqrt(const.GM_Earth/r_1);  % [m/s]
v_2 = sqrt(const.GM_Earth/r_2);  % [m/s]

% Transfer orbit 
a_t = 0.5*(r_1+r_2);                % [m]
v_p = sqrt(const.GM_Earth*r_2/(a_t*r_1)); % [m/s]
v_a = sqrt(const.GM_Earth*r_1/(a_t*r_2)); % [m/s]

% Header
fprintf('Exercise 2-1: Orbit raising using Hohmann transfer \n\n');

% Results
fprintf(' Initial altitude     h_1    %7.2f km  \n', h_1/1000 );
fprintf(' Final   altitude     h_2    %7.2f km  \n', h_2/1000 );
fprintf(' Circular velocity    v_1    %6.2f m/s \n', v_1      );
fprintf(' Circular velocity    v_2    %6.2f m/s \n', v_2      );
fprintf(' Difference                  %7.2f m/s \n\n', v_1-v_2);

fprintf(' Transfer orbit sma   a_t    %6.2f km \n', a_t/1000);
fprintf(' Pericenter velocity  v_p    %6.2f m/s \n', v_p    );
fprintf(' Apocenter  velocity  v_a    %6.2f m/s \n', v_a    );
fprintf(' Difference           v_p-v_1%7.2f m/s \n', v_p-v_1);
fprintf(' Difference           v_2-v_a%7.2f m/s \n', v_2-v_a);
fprintf(' Total velocity diff.        %7.2f m/s \n', v_2-v_a+v_p-v_1);

