%--------------------------------------------------------------------------
%
% f_Kep3D: Computes the second time derivative of the position vector for
%          the normalized (GM=1) Kepler's problem in three dimensions
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
function a = f_Kep3D(t, r_0, v_0)

global nCalls

% 2nd order derivative d^2(r)/dt^2 of the position vector
a = -r_0/(norm(r_0)^3);

% Increment function call count
nCalls = nCalls+1;

