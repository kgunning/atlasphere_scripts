%--------------------------------------------------------------------------
%
%   Exercise 4-3: Step size control of DE multistep method
%
% Last modified:   2018/01/27   M. Mahooti
% 
%--------------------------------------------------------------------------
clc
clear
format long g

global nCalls t

% Constants
GM    = 1;                        % Gravitational coefficient
e     = 0.9;                      % Eccentricity
t_end = 20;                       % End time
Kep   = [1, e, 0, 0, 0, 0]';      % (a,e,i,Omega,omega,M)
y_ref = State(GM,Kep,t_end);      % Reference solution

% Variables
nCalls = 0;
n_eqn = 6;
relerr = 0;
abserr = 1e-8;

% Header
fprintf( 'Exercise 4-3: Step size control of DE multistep method\n\n' );
fprintf( '  Step       t           h          r \n' );

% Initial values
t = 0;

y = [1-e, 0, 0, 0, sqrt((1+e)/(1-e)), 0]';

% Integration from t=t to t=t_end
y = DEInteg(@f_Kep6D,t,t_end,relerr,abserr,n_eqn,y);

