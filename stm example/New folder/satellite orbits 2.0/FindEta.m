%--------------------------------------------------------------------------
%
% FindEta: Computes the sector-triangle ratio from two position vectors and 
%          the intermediate time 
%
% Inputs:
%   r_a        Position at time t_a
%   r_a        Position at time t_b
%   tau        Normalized time (sqrt(GM)*(t_a-t_b))
%
% Output:
%   eta2	   Sector-triangle ratio
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
function [eta2] = FindEta(r_a, r_b, tau)

maxit = 30;
delta = 1e-16;  

s_a = norm(r_a);  
s_b = norm(r_b);  

kappa = sqrt(2*(s_a*s_b+dot(r_a,r_b)));

m = (tau^2)/kappa^3;   
l = (s_a+s_b)/(2*kappa)-0.5;

eta_min = sqrt(m/(l+1));

%  Start with Hansen's approximation
eta2 = (12+10*sqrt(1+(44/9)*m /(l+5/6)))/22;
eta1 = eta2 + 0.1;

%  Secant method
F1 = F(eta1, m, l);
F2 = F(eta2, m, l);

i = 0;

while(abs(F2-F1) > delta)
    d_eta = -F2*(eta2-eta1)/(F2-F1);  
    eta1 = eta2; F1 = F2; 
    while ((eta2+d_eta)<=eta_min)  
        d_eta = d_eta/2;
    end
    
    eta2 = eta2+ d_eta;
    F2 = F(eta2,m,l); 
    i = i+1;
    
    if  i == maxit
        disp('Convergence problems in FindEta')
        break
    end
end

