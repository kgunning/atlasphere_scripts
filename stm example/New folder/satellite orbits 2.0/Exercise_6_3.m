%--------------------------------------------------------------------------
%
%   Exercise 6-3: User Clock Error from GPS Pseudorange.
% 
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

SAT_Const

% Constants
n_eph = 13;                    % Number of ephemeris points
n_obs = 6;                     % Number of observations
n_it  = 3;                     % Light time iterations steps

L1    = 154;                   % Frequency multiplier L1
L2    = 120;                   % Frequency multiplier L2
f_rel = 1/(1-(L2*L2)/(L1*L1)); % Ionosphere multiplier
mean  = 0;

% Goldstone station coordinates WGS-84 [m]
r_Sta = [-2353614.1280, -4641385.4470, 3676976.5010]'; 

% Variables
res = zeros(n_obs,1);              % Pseudorange residuals [m]

% SP3 PRN1 position coordinates (x,y,z) [m] and clock error [s]
% 1998/02/19 08:00:00.0 - 11:00:00.0, at 15 m intervals [GPS time]
t = [8.00, 8.25, 8.50, 8.75, 9.00, 9.25, 9.50, 9.75, 10.00, 10.25, ...
     10.50, 10.75, 11.00];

x = [-15504291.797,-15284290.679,-14871711.829,-14242843.546, ...
     -13380818.523,-12276418.004,-10928585.710, -9344633.744, ...
      -7540134.384, -5538503.062, -3370289.205, -1072201.838, ...
       1314093.678];

y = [-21530763.883,-21684703.684,-21600510.259,-21306712.708, ...
     -20837175.663,-20229688.085,-19524421.024,-18762314.034, ...
     -17983451.817,-17225491.970,-16522202.377,-15902162.018, ...
     -15387672.739];

z = [-1271498.273,  1573435.406,  4391350.089,  7133948.741, ...
      9754366.309, 12207953.668, 14453015.617, 16451492.281, ...
     18169574.686, 19578246.580, 20653745.961, 21377940.941, ...
     21738615.794];

clk = [40.018233e-6, 40.097295e-6, 40.028697e-6, 40.154941e-6, ...
       40.193626e-6, 40.039288e-6, 40.012677e-6, 39.883106e-6, ...
       40.181357e-6, 40.328261e-6, 40.039533e-6, 40.052642e-6, ...
       40.025493e-6];

% Goldstone pseudo range observations P2, C1 [m] from PRN1
% 1998/02/19 08:30:00.0 - 11:00:00.0, at 30 m intervals [GPS time]
t_obs = [8.5, 9.0, 9.5, 10.0, 10.5, 11.0];
  
range_P2 = [21096577.475, 20519964.850, 20282706.954, 20375838.496, ...
            20751678.769, 21340055.129];

range_C1 = [21096579.501, 20519966.875, 20282709.233, 20375840.613, ...
            20751680.997, 21340057.362];

% Process pseudorange measurements
for i=1:n_obs
    dt_Sat = neville(t,clk,t_obs(i));           % Satellite clock offset [s]
    range  = range_P2(i)...                     % Pseudrorange corrected for
           - (range_P2(i)-range_C1(i))*f_rel... % - ionosphere
           + const.c_light*dt_Sat;              % - satellite clock offset
           
    % Light time iteration for downleg GPS -> station
    dt_User = 0;
    tau     = 0;
    for i_it=1:n_it
        t_rcv = t_obs(i) - dt_User/3600;       % Receive time (GPS)
        t_snd = t_rcv - tau/3600;              % Transmit time (GPS) [h]
        r_GPS = [neville(t,x,t_snd),...        % Position of GPS satellite
                 neville(t,y,t_snd),...        % at t_snd in Earth-fixed
                 neville(t,z,t_snd)]';         % system at t_snd and in               
        r_GPS = R_z(const.omega_Earth*tau)*r_GPS; % Earth-fixed frame at t_obs
        rho   = norm(r_GPS-r_Sta);             % Range
        tau   = rho/const.c_light;             % Light time
        res(i)  = range - rho;                 % Residual
        dt_User = res(i)/const.c_light;        % Approx. user clock error 
    end
    mean = mean + res(i);
end

mean = mean/n_obs;                                    % Scaling of mean value

% Output
fprintf('Exercise 6-3: User Clock Error from GPS Pseudorange\n\n');
fprintf('   Date         UTC           Error    Err-Mean\n');
fprintf('yyyy/mm/dd  hh:mm:ss.sss       [m]       [m]  \n');

for i=1:n_obs
    [year,mon,day,hr,min,sec] = invjday(Mjday(1998,2,19)+2400000.5+t_obs(i)/24);
    fprintf('%4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
    fprintf('%12.2f', res(i));
    fprintf('%9.2f\n', res(i)-mean);
end
fprintf('\n Mean value = %9.2f\n',  mean);

