%--------------------------------------------------------------------------
%
%  inputs          description                    range / units
%    ecc         - eccentricity                   0.0  to
%    m           - mean anomaly                   -2pi to 2pi rad
%
%  outputs       :
%    e0          - eccentric anomaly              0.0  to 2pi rad
%    nu          - true anomaly                   0.0  to 2pi rad
%
%--------------------------------------------------------------------------
function [e0,nu] = newtonm (ecc,m)

numiter =    50;
small   =     0.00000001;
halfpi  = pi * 0.5;

if ( (ecc-1.0 ) > small )
    if ( ecc < 1.6  )
        if ( ((m<0.0 ) && (m>-pi)) || (m>pi) )
            e0= m - ecc;
          else
            e0= m + ecc;
        end
      else
        if ( (ecc < 3.6 ) && (abs(m) > pi) )
            e0= m - sign(m)*ecc;
          else
            e0= m/(ecc-1.0 );
        end
    end
    ktr= 1;
    e1 = e0 + ( (m-ecc*sinh(e0)+e0) / (ecc*cosh(e0) - 1.0 ) );
    while ((abs(e1-e0)>small ) && ( ktr<=numiter ))
        e0= e1;
        e1= e0 + ( ( m - ecc*sinh(e0) + e0 ) / ( ecc*cosh(e0) - 1.0  ) );
        ktr = ktr + 1;
    end

    sinv= -( sqrt( ecc*ecc-1.0  ) * sinh(e1) ) / ( 1.0  - ecc*cosh(e1) );
    cosv= ( cosh(e1) - ecc ) / ( 1.0  - ecc*cosh(e1) );
    nu  = atan2( sinv,cosv );
else
    if ( abs( ecc-1.0  ) < small )
         s = 0.5  * (halfpi - atan( 1.5 *m ) );
         w = atan( tan( s )^(1.0 /3.0 ) );
         e0= 2.0 *cot(2.0 *w);
         ktr= 1;
         nu = 2.0  * atan(e0);
    else
        if ( ecc > small )
            if ( ((m < 0.0 ) && (m > -pi)) || (m > pi) )
                e0= m - ecc;
              else
                e0= m + ecc;
            end
            ktr= 1;
            e1 = e0 + ( m - e0 + ecc*sin(e0) ) / ( 1.0  - ecc*cos(e0) );
            while (( abs(e1-e0) > small ) && ( ktr <= numiter ))
                ktr = ktr + 1;
                e0= e1;
                e1= e0 + ( m - e0 + ecc*sin(e0) ) / ( 1.0  - ecc*cos(e0) );
            end

            sinv= ( sqrt( 1.0 -ecc*ecc ) * sin(e1) ) / ( 1.0 -ecc*cos(e1) );
            cosv= ( cos(e1)-ecc ) / ( 1.0  - ecc*cos(e1) );
            nu  = atan2( sinv,cosv );
        else
            ktr= 0;
            nu= m;
            e0= m;
        end
    end
end

