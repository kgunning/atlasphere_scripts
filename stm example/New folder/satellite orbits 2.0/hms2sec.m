%--------------------------------------------------------------------------
%
%  hms2sec: Converts hours, minutes and seconds into seconds from the
%           beginning of the day.
%
%  Inputs:         description                    range / units
%    hr          - hours                          0 .. 24
%    min         - minutes                        0 .. 59
%    sec         - seconds                        0.0 .. 59.99
%
%   Outputs:
%    utsec       - seconds                        0.0 .. 86400.0
%
% -------------------------------------------------------------------------
function utsec = hms2sec(hr,min,sec)

utsec  = hr * 3600 + min * 60 + sec;

