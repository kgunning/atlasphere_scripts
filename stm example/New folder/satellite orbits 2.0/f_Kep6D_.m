%--------------------------------------------------------------------------
%
% f_Kep6D
%
% Purpose:
% 
%   Computes the derivative of the state vector for the normalized (GM=1)
%   Kepler's problem in three dimensions
%
%--------------------------------------------------------------------------
function yp = f_Kep6D_(x, y)

global nCalls t

% State vector derivative
r = y(1:3);
v = y(4:6);
yp = [v;-r/((norm(r))^3)];

% store time for next step
nCalls = nCalls+1;
t = x;

