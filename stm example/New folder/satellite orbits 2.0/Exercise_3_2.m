%--------------------------------------------------------------------------
%
%   Exercise 3-2: Lunar ephemerides
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

global const PC

SAT_Const
load DE430Coeff.mat
PC = DE430Coeff;

% Constants
N_Step = 8;
Step   = 0.5; % [day]

% Epoch
Mjd0 = Mjday(2006,03,14,00,00,0.0);

% Output
fprintf('Exercise 3-2: Lunar Ephemerides \n\n');

fprintf('Moon position from high precision analytical theory\n');
fprintf(' Date [TDB]                  Position [km] \n');

for i=0:N_Step
    Mjd_TDB = Mjd0 + i*Step;
    r = MoonPos(Mjd_TDB)/1000;
    fprintf( ' ' );
    [year,mon,day,hr,min,sec] = invjday(Mjd_TDB+2400000.5);
    fprintf('%4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
    fprintf('%14.3f', r);
    fprintf('\n');
end

fprintf('\nMoon position from DE430\n');
fprintf(' Date [TDB]                  Position [km] \n');

for i=0:N_Step
    Mjd_TDB = Mjd0 + i*Step;
    [r_Mercury,r_Venus,r_Earth,r_Mars,r_Jupiter,r_Saturn,r_Uranus, ...
     r_Neptune,r_Pluto,r_Moon,r_Sun,r_SunSSB] = JPL_Eph_DE430(Mjd_TDB);
    fprintf( ' ' );
    [year,mon,day,hr,min,sec] = invjday(Mjd_TDB+2400000.5);
    fprintf('%4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
    fprintf('%14.3f', 1e-3*r_Moon);
    fprintf('\n');
end

