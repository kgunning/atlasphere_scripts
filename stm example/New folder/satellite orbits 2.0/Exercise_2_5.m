%--------------------------------------------------------------------------
%
%   Exercise 2-5: Sunsynchronous repeat orbits
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

SAT_Const

K   =  3;                          % Number of cycles
N   = 43;                          % Number of orbits
T_N = K / N;                       % Draconic period [d]
J_2 = 1.08263e-3;                  % Oblateness coefficient

Omega_dot = 0.985647240*const.Rad; % Nodal rate [rad/d]

omega_dot = 0;                     % Arg. of latitude rate
Delta_n   = 0;                     % Perturb. of mean motion
n_0       = 0;
a         = 0; 
h         = 0;
i         = 0;

% Header 
fprintf( 'Exercise 2-5: Sunsynchronous repeat orbits\n\n' );
fprintf( '  Draconic period          T_N = ' );
fprintf( '%12.7f d \n', T_N );
fprintf( '  Rate                 2pi/T_N = ' );
fprintf( '%12.5f deg/d \n', 360/T_N );
fprintf( '  Node offset  Delta lam_Omega = ' );
fprintf( '%12.5f deg/Orbit \n\n', -K*360/N );

% Iteration
for iterat=0:2
    
    % Secular rates of argument of perigee and mean anomaly [rad/d]
    if (iterat==0)
        omega_dot = 0;
        Delta_n   = 0;
    else
        omega_dot = -0.75*n_0*J_2*(const.R_Earth/a)^2*(1-5*cos(i)^2);
        Delta_n   = -0.75*n_0*J_2*(const.R_Earth/a)^2*(1-3*cos(i)^2);
    end
    
    % Mean motion, semimajor axis and altitude
    n_0 = const.pi2/T_N - Delta_n - omega_dot;    % [rad/d]
    a   = (const.GM_Earth/((n_0/86400)^2))^(1/3); % [m]
    h   = a - const.R_Earth;                      % [m]
    
    % Inclination [rad}
    i   = acos(-2*Omega_dot/(3*n_0*J_2)*(a/const.R_Earth)^2);
    
    fprintf('  Iteration %d \n\n', iterat);
    fprintf('  Arg. perigee rate  omega_dot = ');
    fprintf('  %12.5f  deg/d \n', const.Deg*omega_dot);
    fprintf('  Perturb. mean motion   n-n_0 = ');
    fprintf('  %12.5f  deg/d \n', const.Deg*Delta_n);
    fprintf('  Mean motion              n_0 = ');
    fprintf('  %12.5f  deg/d \n', const.Deg*n_0);
    fprintf('  Semimajor axis             a = ');
    fprintf('  %12.3f  km \n', a/1000);
    fprintf('  Altitude                   h = ');
    fprintf('  %12.3f  km \n', h/1000);
    fprintf('  Inclination                i = ');
    fprintf('  %12.3f  deg \n\n', const.Deg*i);
    
end

% Ascending nodes
fprintf('  Greenwich longitude of ascending node\n\n');
fprintf('      Day 1          Day 2          Day 3    \n');
fprintf('  Orbit   [deg]  Orbit   [deg]  Orbit   [deg] \n');

for I=0:14
    for J=0:2
        fprintf('%5.2d', 15*J+I);
        fprintf('%10.2f', mod(-(15*J+I)*K*360/N+180,360)-180);
    end 
    fprintf('\n');
end

