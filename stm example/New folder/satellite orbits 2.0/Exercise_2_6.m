%--------------------------------------------------------------------------
%
%   Exercise 2-6: Initial orbit determination
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

SAT_Const

% read Earth orientation parameters
fid = fopen('eop19620101.txt','r');
%  ----------------------------------------------------------------------------------------------------
% |  Date    MJD      x         y       UT1-UTC      LOD       dPsi    dEpsilon     dX        dY    DAT
% |(0h UTC)           "         "          s          s          "        "          "         "     s 
%  ----------------------------------------------------------------------------------------------------
eopdata = fscanf(fid,'%i %d %d %i %f %f %f %f %f %f %f %f %i',[13 inf]);
fclose(fid);

% Ground station
R_Sta= [1344.143e3,6068.601e3,1429.311e3]; % Position vector
[lon_Sta,lat_Sta,h_Sta] = Geodetic(R_Sta); % Geodetic coordinates

% Observations
Obs = [Mjday(1999,04,02, 00,30,0.0), 132.67*const.Rad, 32.44*const.Rad, 16945.450e3;
       Mjday(1999,04,02, 03,00,0.0), 123.08*const.Rad, 50.06*const.Rad, 37350.340e3];

% Transformation to local tangent coordinates
E = LTCMatrix(lon_Sta,lat_Sta);

% Convert observations
r = zeros(3,2);
for i=1:2
  % Earth rotation
  [x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,Obs(i,1),'l');
  Mjd_UT1 = Obs(i,1) + UT1_UTC/86400;
  U = R_z(gmst(Mjd_UT1));
  % Topocentric position vector
  Az = Obs(i,2); El = Obs(i,3); d = Obs(i,4);
  s = Polar(pi/2-Az,El,d);
  % Inertial position vector
  r(:,i) = U'*(E'*s+R_Sta');
end

% Orbital elements
[a,e,inc,Omega,omega,M] = Elements(const.GM_Earth,Obs(1,1),Obs(2,1),r(:,1),r(:,2));

% Output
fprintf('Exercise 2-6: Initial orbit determination\n\n');
fprintf('Inertial positions:\n\n');
fprintf('                                [km]        [km]        [km]\n');

for i=1:2
    fprintf('  ');
    [year,mon,day,hr,min,sec] = invjday(Obs(i,1)+2400000.5);
    fprintf('%4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year, mon, day, hr, min, sec);
    for j=1:3
        fprintf('%12.3f', r(j,i)/1000);
    end
    fprintf('\n');
end  
fprintf('\n');

fprintf('Orbital elements:\n\n');
fprintf('  Epoch (1st obs.)  ');
[year,mon,day,hr,min,sec] = invjday(Obs(1,1) + 2400000.5);
fprintf('%4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f\n', year, mon, day, hr, min, sec);
fprintf('  Semimajor axis   ');
fprintf('%10.3f  km\n', a/1000);
fprintf('  Eccentricity     ');
fprintf('%10.7f  \n', e);
fprintf('  Inclination      ');
fprintf('%10.3f  deg\n', inc*const.Deg);
fprintf('  RA ascend. node  ');
fprintf('%10.3f  deg\n', Omega*const.Deg);
fprintf('  Arg. of perigee  ');
fprintf('%10.3f  deg\n', omega*const.Deg);
fprintf('  Mean anomaly     ');
fprintf('%10.3f  deg\n\n', M*const.Deg);

