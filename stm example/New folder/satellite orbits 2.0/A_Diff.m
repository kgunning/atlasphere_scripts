%--------------------------------------------------------------------------
%
% A_Diff: Computes the difference of acceleration due to SRP and DRG
%
% Input:
%   h         Height [m]
%
% Output:
%   adiff     Difference of acceleration
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
function [adiff] = A_Diff(h)

global const eopdata

CD     =   2.3; % Spacecraft parameters
CR     =   1.3;
Mjd_TT = 51269; % State epoch

r = [1, 0, 0] * (const.R_Earth + h);

[x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,Mjd_TT,'l');
[UT1_TAI, UTC_GPS, UT1_GPS, TT_UTC, GPS_UTC] = timediff(UT1_UTC,TAI_UTC);
dens = nrlmsise00(Mjd_TT, r, UT1_UTC, TT_UTC);

adiff = (0.5*CD*dens*const.GM_Earth/(const.R_Earth + h))-(const.P_Sol*CR);

