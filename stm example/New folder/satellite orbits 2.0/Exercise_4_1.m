%--------------------------------------------------------------------------
%
%   Exercise 4-1: Runge-Kutta 4th-order Integration
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

% Constants
GM    = 1;                      % Gravitational coefficient
e     = 0.1;                    % Eccentricity
t_end = 20;                     % End time
Kep   = [1, e ,0 ,0 ,0 ,0]';    % (a,e,i,Omega,omega,M)
y_ref = State(GM, Kep, t_end);  % Reference solution

Steps = [50, 100, 250, 500, 750, 1000, 1500, 2000];

% Variables
global nCalls

% Header
fprintf('Exercise 4-1: Runge-Kutta 4th-order integration\n\n');
fprintf('  Problem D1 (e=0.1)\n\n');
fprintf('  N_fnc   Accuracy    Digits \n');

% Loop over test cases
for iCase=1:8
    % Step size
    h = t_end/Steps(iCase);
    
    % Initial values
    t_0 = 0;
    y_0 = [1-e, 0, 0, 0, sqrt((1+e)/(1-e)), 0]';
    nCalls = 0;
    
    % Integration from t=t to t=t_end
    for i=1:Steps(iCase)
        [y] = RK4(t_0, y_0, h);
        y_0   = y;
        t_0 = t_0+h;
    end
    
    % Output
    fprintf('%6i %13.3e %6.2f\n', nCalls, norm(y-y_ref), -log10(norm(y-y_ref)));
end

