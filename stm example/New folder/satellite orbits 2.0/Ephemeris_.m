%--------------------------------------------------------------------------
%
% Ephemeris computation
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
function Y = Ephemeris_(Y0, N_Step, Step)

options = rdpset('RelTol',1e-13,'AbsTol',1e-16);
[~,yout] = radau(@VarEqn,[0 N_Step*Step],Y0,options);
Y = yout(end,:)';

% global n_eqn
% 
% t_0 = 0;
% 
% for i=1:N_Step
%     t_end = i*Step;
%     Y = DEInteg(@VarEqn,t_0,t_end,1e-13,1e-6,n_eqn,Y0);
%     t_0 = t_end;
%     Y0 = Y;    
% end

