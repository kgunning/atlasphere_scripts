%--------------------------------------------------------------------------
%
%   Exercise 2-2: Solution of Kepler's equation
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

SAT_Const

% Variables
MeanAnom = [4, 50];   % [deg]

for iCase=0:1
    
    % Test Case
    M     = MeanAnom(iCase+1)*const.Rad;
    e     = 0.72; 
    E_ref = EccAnom(M,e);
    
    fprintf('Exercise 2-2: Solution of Kepler''s equation \n\n');
    
    fprintf('  M = %14.11f   \n',  M);
    fprintf('  e = %14.11f   \n',  e);
    fprintf('  E = %14.11f \n\n',  E_ref);
    
    % Newton's iteration
    fprintf('  a) Newton''s iteration \n\n');
    fprintf('  i          E          Accuracy   sin/cos \n');
    
    E = M;
    i = 0;
    
    while ( abs(E-E_ref)> 1e-10 )
        i = i+1;
        E = E - (E-e*sin(E)-M)/(1-e*cos(E)); 
        fprintf('%3d ', i);
        fprintf('%16.11f ', E);
        fprintf('%11.2e ', abs(E-E_ref));
        fprintf('%6d \n', 2*i);          
    end
    
    fprintf('\n');
    
    % Fixed point iteration
    fprintf('  b) Fixed point iteration \n\n');
    fprintf('  i          E          Accuracy   sin/cos \n');
    
    E = M;
    i = 0;
    
    while ( abs(E-E_ref)> 1e-10 )
        i = i+1;
        E = M + e*sin(E); 
        fprintf('%3d ', i);
        fprintf('%16.11f ', E);
        fprintf('%11.2e ', abs(E-E_ref));
        fprintf('%6d \n', i);
    end
    
    fprintf('\n');
    
end

