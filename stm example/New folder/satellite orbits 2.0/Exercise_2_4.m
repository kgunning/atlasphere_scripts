%--------------------------------------------------------------------------
%
%   Exercise 2-4: Topocentric satellite motion
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

global const

SAT_Const

% read Earth orientation parameters
fid = fopen('eop19620101.txt','r');
%  ----------------------------------------------------------------------------------------------------
% |  Date    MJD      x         y       UT1-UTC      LOD       dPsi    dEpsilon     dX        dY    DAT
% |(0h UTC)           "         "          s          s          "        "          "         "     s 
%  ----------------------------------------------------------------------------------------------------
eopdata = fscanf(fid,'%i %d %d %i %f %f %f %f %f %f %f %f %i',[13 inf]);
fclose(fid);

% Ground station
lon_Sta = 11*const.Rad; % [rad]
lat_Sta = 48*const.Rad; % [rad]
alt_h   =  0;           % [m]

% Spacecraft orbit
MJD_Epoch = Mjday(1997,01,01); % Epoch

a     = 960e3 + const.R_Earth; % Semimajor axis [m]
e     =   0;                   % Eccentricity
i     =  97*const.Rad;         % Inclination [rad]
Omega = 130.7*const.Rad;       % RA ascend. node [rad]
omega =   0*const.Rad;         % Argument of latitude [rad]
M0    =   0*const.Rad;         % Mean anomaly at epoch [rad]

Kep = [a,e,i,Omega,omega,M0];  % Keplerian elements

% Station
R_Sta = Position(lon_Sta, lat_Sta, alt_h); % Geocentric position vector
E     = LTCMatrix(lon_Sta, lat_Sta);       % Transformation to local tangent coordinates

% Header
fprintf('Exercise 2-4: Topocentric satellite motion\n\n');
fprintf('   Date         UTC           Az         El      Dist\n');
fprintf('yyyy/mm/dd  hh:mm:ss.sss     [deg]     [deg]     [km]\n');

% Orbit
for Minute = 6:24
    
    MJD_UTC = MJD_Epoch + Minute/1440;  % Time
    [x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,MJD_UTC,'l');
    MJD_UT1 = MJD_UTC + UT1_UTC/86400;
    dt = (MJD_UTC-MJD_Epoch)*86400;     % Time since epoch [s] 
    
    Y = State(const.GM_Earth, Kep, dt); % Inertial vector
    r = Y(1:3);                         % Inertial position vector
    U = R_z(gmst(MJD_UT1));             % Earth rotation 
    s = E*(U*r - R_Sta');               % Topocentric position vector
    
    Dist = norm(s);                     % Distance
    [Azim, Elev] = AzEl(s);             % Azimuth, Elevation
    
    [year,mon,day,hr,min,sec] = invjday(MJD_UTC+2400000.5);
    
    fprintf('%4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year, mon, day, hr, min, sec);
    fprintf('%10.1f', Azim*const.Deg);
    fprintf('%10.1f', Elev*const.Deg);
    fprintf('%10.1f', Dist/1000);
    fprintf('\n');
    
end

