%--------------------------------------------------------------------------
%
%   Exercise 8-1: Least-squares fit using Givens rotations
%
% Last modified:   2018/01/27   M. Mahooti
% 
%--------------------------------------------------------------------------
clc
clear
format long g

global R d

t = [0.04, 0.32, 0.51, 0.73, 1.03, 1.42, 1.60];
z = [2.63, 1.18, 1.16, 1.54, 2.65, 5.41, 7.67];

R = zeros(3);
d = zeros(3,1);

% Header 
fprintf('Exercise 8-1: Least-squares fit using Givens rotation\n\n');

% Accumulation of data equations
N = 3;
sigma = 1;

for i=1:7   
    % Data equation
    a(1) = 1;
    a(2) = t(i);
    a(3) = t(i)*t(i);
    b    = z(i);
    
    fprintf('Observation ');
    fprintf('%d\n\n', i-1);
    fprintf('  a =');
    fprintf('%7.4f', a(1));
    fprintf('%7.4f', a(2));
    fprintf('%7.4f', a(3));
    fprintf('    ');
    fprintf('  b =%7.4f\n\n', b);
    
    % Process data equation
    LSQAccumulate (N, a, b, sigma);
    
    % Square-root information matrix and transformed data 
    fprintf('%12.4f', R(1,1));
    fprintf('%7.4f', R(1,2));
    fprintf('%7.4f', R(1,3));
    fprintf('          %.4f\n', d(1));
    fprintf('  R =');
    fprintf('%7.4f', R(2,:));
    fprintf('    ');
    fprintf('  d = %.4f\n', d(2));
    fprintf('%12.4f', R(3,1));
    fprintf('%7.4f', R(3,2));
    fprintf('%7.4f', R(3,3));
    fprintf('          %.4f\n\n', d(3));    
end

% Solution of least squares system
c = LSQSolve(N, R, d);

fprintf('\n');
fprintf('Adjusted polynomial coefficients\n\n');

for i=1:3
    fprintf('  c(');
    fprintf('%d', i);
    fprintf(') = ');
    fprintf('%f\n', c(i));
end

