%--------------------------------------------------------------------------
%
% 4th order Runge-Kutta step for 2nd order differential equation
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
function [r, v] = RK4_(t, r_0, v_0, h)

v_1 = v_0;            a_1 = f_Kep3D(t    , r_0          , v_1); 
v_2 = v_0+(h/2)*a_1;  a_2 = f_Kep3D(t+h/2, r_0+(h/2)*v_1, v_2);
v_3 = v_0+(h/2)*a_2;  a_3 = f_Kep3D(t+h/2, r_0+(h/2)*v_2, v_3); 
v_4 = v_0+h*a_3;      a_4 = f_Kep3D(t+h  , r_0+h*v_3    , v_4);

r = r_0+(h/6)*(v_1 + 2*v_2 + 2*v_3 + v_4);
v = v_0+(h/6)*(a_1 + 2*a_2 + 2*a_3 + a_4);