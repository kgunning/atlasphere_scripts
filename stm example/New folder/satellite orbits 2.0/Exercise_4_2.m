%--------------------------------------------------------------------------
%
%   Exercise 4-2: Gauss-Jackson 4th-order predictor 
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

% Constants
GM    = 1;                     % Gravitational coefficient
e     = 0.1;                   % Eccentricity
t_end = 20;                    % End time
Kep   = [1,e,0,0,0,0];         % (a,e,i,Omega,omega,M)
y_ref = State(GM,Kep,t_end);   % Reference solution

Steps = [100, 300, 600, 1000, 1500, 2000, 3000, 4000];

% Variables
global nCalls

% Header
fprintf('Exercise 4-2: Gauss-Jackson 4th-order predictor\n\n' );
fprintf('  Problem D1 (e=0.1)\n\n' );
fprintf('  N_fnc   Accuracy   Digits \n' );

% Loop over test cases
for iCase=1:8
    % Step size
    h = t_end/Steps(iCase);
    
    % Initial values
    t_0 = 0;
    r_0 = [1-e, 0, 0];
    v_0 = [0, sqrt((1+e)/(1-e)), 0];
    nCalls = 0;
    
    [S1, S2, D] = GJ4PInit(t_0, r_0, v_0, h);
    
    % Integration from t=t to t=t_end
    for i=1:Steps(iCase)
        [S1, S2, D, r, v] = GJ4PStep(S1, S2, D, t_0, h);
        t_0 = t_0+h;
    end
    
    y = [r,v]';
    
    % Output
    fprintf('%6i %13.3e %6.2f\n', nCalls, norm(y-y_ref), -log10(norm(y-y_ref)));
end

