%--------------------------------------------------------------------------
%
%   Exercise 3-1: Gravity field
%
% Hint:
%   The number of evaluations (N_Step) should be suitably adjusted for 
%   different platforms. The numerical results may differ, depending 
%   on the specific run-time environment.
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

global Cnm Snm

% read Earth gravity field coefficients
Cnm = zeros(181,181);
Snm = zeros(181,181);
fid = fopen('GGM03S.txt','r');
for n=0:180
    for m=0:n
        temp = fscanf(fid,'%d %d %f %f %f %f',[6 1]);
        Cnm(n+1,m+1) = temp(3);
        Snm(n+1,m+1) = temp(4);
    end
end

% Constants
N_Step = 10000; % Recommended for 0.01 sec timer (Linux)
n_max  =    20;

r = [6525.919e3, 1710.416e3, 2508.886e3]'; % Position [m]

% Header
fprintf('Exercise 3-1: Gravity Field Computation \n\n');

fprintf(' Order   CPU Time [s]\n\n');

% Outer loop [2,4,...,n_max]
for n=2:2:n_max    
    % Start timing
    tic;
    
    % Evaluate gravitational acceleration N_Step times
    for i=0:N_Step
        a = AccelHarmonic(r,eye(3),n,n);
    end
    
    % Stop CPU time measurement
    duration = toc;
    
    fprintf('%4d %12.2f\n', n, duration); 
end

