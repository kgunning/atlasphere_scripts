%--------------------------------------------------------------------------
%
% StatePartials: Computes the partial derivatives of the satellite state
%                vector with respect to the orbital elements for elliptic,
%                Keplerian orbits
%
% Inputs:
%   gm        Gravitational coefficient
%             (gravitational constant * mass of central body)
%   Kep       Keplerian elements (a,e,i,Omega,omega,M) at epoch with
%               a      Semimajor axis 
%               e      Eccentricity 
%               i      Inclination [rad]
%               Omega  Longitude of the ascending node [rad]
%               omega  Argument of pericenter  [rad]
%               M      Mean anomaly at epoch [rad]
%   dt        Time since epoch
% 
% Output:  
%   dYdA      Partials derivatives of the state vector (x,y,z,vx,vy,vz) at
%             time dt with respect to the epoch orbital elements
%
% Notes:
%   The semimajor axis a=Kep(0), dt and gm must be given in consistent units, 
%   e.g. [m], [s] and [m^3/s^2]. The resulting units of length and velocity  
%   are implied by the units of gm, e.g. [m] and [m/s].
%
%   The function cannot be used with circular or non-inclined orbit.
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
function dYdA = StatePartials( gm, Kep, dt )

% Keplerian elements at epoch
a = Kep(1);  Omega = Kep(4);
e = Kep(2);  omega = Kep(5); 
i = Kep(3);  M0    = Kep(6);

% Mean and eccentric anomaly
n = sqrt (gm/(a*a*a));
M = M0 +n*dt;
E = EccAnom(M,e);   

% Perifocal coordinates
cosE = cos(E); 
sinE = sin(E);
fac  = sqrt((1-e)*(1+e));  

r = a*(1.0-e*cosE);  % Distance
v = sqrt(gm*a)/r;    % Velocity

x  = +a*(cosE-e); y  = +a*fac*sinE;
vx = -v*sinE;     vy = +v*fac*cosE; 

% Transformation to reference system (Gaussian vectors) and partials
PQW = R_z(-Omega) * R_x(-i) * R_z(-omega);

P = PQW(:,1);  Q = PQW(:,2);  W = PQW(:,3);

e_z = [0,0,1];  N = cross(e_z,W); N = N/norm(N);

dPdi = cross(N,P);  dPdO = cross(e_z,P); dPdo =  Q;
dQdi = cross(N,Q);  dQdO = cross(e_z,Q); dQdo = -P;

% Partials w.r.t. semimajor axis, eccentricity and mean anomaly at time dt
dYda = [ (x/a)*P + (y/a)*Q, (-vx/(2*a))*P + (-vy/(2*a))*Q ];

dYde = [ (-a-(y/fac)^2/r)*P + (x*y/(r*fac*fac))*Q , ...
               (vx*(2*a*x+e*(y/fac)^2)/(r*r))*P ...
               + ((n/fac)*(a/r)^2*(x*x/r-(y/fac)^2/a))*Q ];

dYdM = [ (vx*P+vy*Q)/n, (-n*(a/r)^3)*(x*P+y*Q) ];

% Partials w.r.t. inlcination, node and argument of pericenter
dYdi = [ x*dPdi+y*dQdi, vx*dPdi+vy*dQdi ]; 
dYdO = [ x*dPdO+y*dQdO, vx*dPdO+vy*dQdO ]; 
dYdo = [ x*dPdo+y*dQdo, vx*dPdo+vy*dQdo ]; 

% Derivative of mean anomaly at time dt w.r.t. the semimajor axis at epoch
dMda = -1.5*(n/a)*dt;  

% Combined partial derivative matrix of state with respect to epoch elements
dYdA = zeros(6,6);

for k=1:6
    dYdA(k,1) = dYda(k) + dYdM(k)*dMda;  
    dYdA(k,2) = dYde(k); 
    dYdA(k,3) = dYdi(k); 
    dYdA(k,4) = dYdO(k);
    dYdA(k,5) = dYdo(k);
    dYdA(k,6) = dYdM(k);
end

