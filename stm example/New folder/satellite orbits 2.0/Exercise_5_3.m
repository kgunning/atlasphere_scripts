%--------------------------------------------------------------------------
%
%   Exercise 5-3: Geodetic coordinates
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

SAT_Const

% WGS84 geoid parameters
R_WGS84 = 6378.137e3;      % Radius Earth [m]
f_WGS84 = 1/298.257223563; % Flattening

% Header
fprintf('Exercise 5-3: Geodetic coordinates\n\n');

% Coordinates of NIMA GPS station at Diego Garcia (WGS84(G873); epoch 1997.0)
R_Sta = [1917032.190, 6029782.349, -801376.113]; % [m]

% Geodetic coordinates
[lon, lat, h] = Geodetic(R_Sta);

% Output
fprintf('Cartesian station coordinates (WGS84) [m]\n\n');
fprintf('%13.3f', R_Sta);
fprintf('\n\n');
fprintf('Geodetic station coordinates (WGS84)\n\n');
fprintf(' longitude %12.8f', const.Deg*lon);
fprintf(' deg\n');
fprintf(' latitude %12.8f', const.Deg*lat);
fprintf(' deg\n');
fprintf(' height %12.3f', h);
fprintf(' m\n');

