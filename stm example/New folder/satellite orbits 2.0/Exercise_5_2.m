%--------------------------------------------------------------------------
%
%   Exercise 5-2: Velocity in the Earth-fixed frame
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

SAT_Const

% read Earth orientation parameters
fid = fopen('eop19620101.txt','r');
%  ----------------------------------------------------------------------------------------------------
% |  Date    MJD      x         y       UT1-UTC      LOD       dPsi    dEpsilon     dX        dY    DAT
% |(0h UTC)           "         "          s          s          "        "          "         "     s 
%  ----------------------------------------------------------------------------------------------------
eopdata = fscanf(fid,'%i %d %d %i %f %f %f %f %f %f %f %f %i',[13 inf]);
fclose(fid);

% Header 
fprintf('Exercise 5-2: Velocity in the Earth-fixed frame\n\n');

% Date
MJD_GPS = Mjday(1999,03,04,0,0,0.0);

% Earth Orientation Parameters (UT1-UTC[s],TAI-UTC[s], x["], y["])
% (from IERS Bulletin B #135 and C #16; valid for 1999/03/04 0:00 UTC)
[x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,MJD_GPS,'l');
[UT1_TAI,UTC_GPS,UT1_GPS,TT_UTC,GPS_UTC] = timediff(UT1_UTC,TAI_UTC);
MJD_UTC = MJD_GPS - GPS_UTC/86400;
MJD_UT1 = MJD_UTC + UT1_UTC/86400;
MJD_TT  = MJD_UTC + TT_UTC/86400; 

% Earth-fixed state vector of GPS satellite #PRN15
% (from NIMA ephemeris nim09994.eph; WGS84(G873) system)
r_WGS = [19440.953805e+3,16881.609273e+3, -6777.115092e+3]'; % [m]
v_WGS = [-8111.827456e-1,-2573.799137e-1,-30689.508125e-1]'; % [m/s]

% ICRS to ITRS transformation matrix and derivative
P     = PrecMatrix(const.MJD_J2000,MJD_TT); % IAU 1976 Precession
N     = NutMatrix(MJD_TT);                  % IAU 1980 Nutation
Theta = GHAMatrix(MJD_UT1);                 % Earth rotation
Pi    = PoleMatrix(x_pole,y_pole);          % Polar motion

S = zeros(3);
S(1,2) = 1.0; S(2,1) = -1.0;                % Derivative of Earth rotation 
dTheta = const.omega_Earth*S*Theta;         % matrix [1/s]

U      = Pi*Theta*N*P;                      % ICRS to ITRS transformation
dU     = Pi*dTheta*N*P;                     % Derivative [1/s]

% Transformation from WGS to ICRS
r = U'*r_WGS;
v = U'*v_WGS + dU'*r_WGS;

% Orbital elements
y   = [r;v];
Kep = Element(const.GM_Earth, y);

% Output
fprintf('Date\n\n');
[year,mon,day,hr,min,sec] = invjday(MJD_GPS+2400000.5);
fprintf(' %4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
fprintf(' GPS\n');
[year,mon,day,hr,min,sec] = invjday(MJD_UTC+2400000.5);
fprintf(' %4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
fprintf(' UTC\n');
[year,mon,day,hr,min,sec] = invjday(MJD_UT1+2400000.5);
fprintf(' %4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
fprintf(' UT1\n');
[year,mon,day,hr,min,sec] = invjday(MJD_TT+2400000.5);
fprintf(' %4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
fprintf(' TT\n\n');
     
fprintf('WGS84 (G873) State vector:\n\n');
fprintf('  Position       ');
fprintf('%12.3f', r_WGS/1000);
fprintf('  [km]\n');
fprintf('  Velocity       ');
fprintf('%12.6f', v_WGS/1000);
fprintf('  [km/s]\n\n');

fprintf('ICRS-ITRS transformation\n\n');
for i=1:3
    for j=1:3
        fprintf('%12.8f', U(i,j));
    end
    fprintf('\n');
end
fprintf('\n');

fprintf('Derivative of ICRS-ITRS transformation [10^(-4)/s]\n\n');
for i=1:3
    for j=1:3
        fprintf('%12.8f', dU(i,j)*1e4);
    end
    fprintf('\n');
end
fprintf('\n');

fprintf('ICRS State vector:\n\n');
fprintf('  Position       ');
fprintf('%12.3f', r/1000);
fprintf('  [km]\n');
fprintf('  Velocity       ');
fprintf('%12.6f', v/1000);
fprintf('  [km/s]\n\n');

fprintf('Orbital elements:\n\n');
fprintf('  Semimajor axis   ');
fprintf('%10.3f', Kep(1)/1000);
fprintf(' km\n');
fprintf('  Eccentricity     ');
fprintf('%10.7f\n', Kep(2));
fprintf('  Inclination      ');
fprintf('%10.3f', Kep(3)*const.Deg);
fprintf(' deg\n');
fprintf('  RA ascend. node  ');
fprintf('%10.3f', Kep(4)*const.Deg);
fprintf(' deg\n');
fprintf('  Arg. of perigee  ');
fprintf('%10.3f', Kep(5)*const.Deg);
fprintf(' deg\n');
fprintf('  Mean anomaly     ');
fprintf('%10.3f', Kep(6)*const.Deg);
fprintf(' deg\n');

