%--------------------------------------------------------------------------
%
%   Exercise 7-1: State transition matrix
%
% Last modified:   2018/01/27   M. Mahooti
% 
%--------------------------------------------------------------------------
clc
clear
format long g

global Cnm Snm AuxParam eopdata n_eqn const

SAT_Const

% read Earth gravity field coefficients
Cnm = zeros(181,181);
Snm = zeros(181,181);
fid = fopen('GGM03S.txt','r');
for n=0:180
    for m=0:n
        temp = fscanf(fid,'%d %d %f %f %f %f',[6 1]);
        Cnm(n+1,m+1) = temp(3);
        Snm(n+1,m+1) = temp(4);
    end
end
fclose(fid);

% read Earth orientation parameters
fid = fopen('eop19620101.txt','r');
%  ----------------------------------------------------------------------------------------------------
% |  Date    MJD      x         y       UT1-UTC      LOD       dPsi    dEpsilon     dX        dY    DAT
% |(0h UTC)           "         "          s          s          "        "          "         "     s 
%  ----------------------------------------------------------------------------------------------------
eopdata = fscanf(fid,'%i %d %d %i %f %f %f %f %f %f %f %f %i',[13 inf]);
fclose(fid);

a   = const.R_Earth+650e3; % Semi-major axis [m]
e   = 0.001;               % Eccentricity
inc = 51*const.Rad;        % Inclination [rad]

Kep = [a, e, inc, 0.0, 0.0, 0.0]';

n = sqrt(const.GM_Earth/(a*a*a));  % Mean motion

Scale = diag([1,1,1,1/n,1/n,1/n]); % Velocity
scale = diag([1,1,1,  n,  n,  n]); % normalization

Scale = diag([1,1,1,1/1,1/1,1/1]); % Velocity
scale = diag([1,1,1,  1,  1,  1]); % normalization

t_end = 86400; 
step  = 300;

% Epoch
Mjd0 = const.MJD_J2000;

y0 = State(const.GM_Earth, Kep, 0);

yPhi1 = zeros(42,1);
Phi1  = zeros(6);
Phi2  = zeros(6);
Phi3  = zeros(6);

for i=1:6
    yPhi1(i) = y0(i);
    for j=1:6  
        if (i==j) 
            yPhi1(6*j+i) = 1; 
        else
            yPhi1(6*j+i) = 0;
        end
    end
end

% Model parameters
Aux1 = struct ('Mjd_UTC',0,'n_a',0,'m_a',0,'n_G',0,'m_G',0);
Aux2 = Aux1;
Aux3 = Aux1;

% Model parameters for reference solution (full 10x10 gravity model) and
% simplified variational equations
Aux1.Mjd_UTC = Mjd0;
Aux1.n_a     = 10;
Aux1.m_a     = 10;
Aux1.n_G     = 10;
Aux1.m_G     = 10;

Aux2.Mjd_UTC = Mjd0;
Aux2.n_a     = 2;
Aux2.m_a     = 0;
Aux2.n_G     = 2;
Aux2.m_G     = 0;

Aux3.Mjd_UTC = Mjd0;
Aux3.n_a     = 0;
Aux3.m_a     = 0;
Aux3.n_G     = 0;
Aux3.m_G     = 0;

% Header
fprintf('Exercise 7-1: State transition matrix\n\n');
fprintf('  Time [s]  Error (J2)  Error (Kep)\n');

% Integration
n_eqn = 42;
yPhi2 = yPhi1;
yPhi3 = yPhi1;

% Steps
for t = step:step:t_end
    AuxParam = Aux1;    
    yPhi1 = Ephemeris_(yPhi1, 1, step);
    
    % Extract and normalize state transition matrices
    for j=1:6
        Phi1(:,j) = yPhi1(6*j+1:6*j+6);
    end
    Phi1 = Scale*Phi1*scale;
    
    AuxParam = Aux2;    
    yPhi2 = Ephemeris_(yPhi2, 1, step);
        
    % Extract and normalize state transition matrices
    for j=1:6
        Phi2(:,j) = yPhi2(6*j+1:6*j+6);
    end
    Phi2 = Scale*Phi2*scale;
    
    AuxParam = Aux3;    
    yPhi3 = Ephemeris_(yPhi3, 1, step);
        
    % Extract and normalize state transition matrices
    for j=1:6
        Phi3(:,j) = yPhi3(6*j+1:6*j+6);
    end
    Phi3 = Scale*Phi3*scale;
    
    % Matrix error norms
    max_    = max( max ( eye(6)-Phi1*InvSymp(Phi2) ) );
    max_Kep = max( max ( eye(6)-Phi1*InvSymp(Phi3) ) );
        
    % Output 
    fprintf('%10.2f%10.2f%12.2f\n', t, max_, max_Kep);
	
    Aux1.Mjd_UTC = Aux1.Mjd_UTC+step/86400;
    Aux2.Mjd_UTC = Aux2.Mjd_UTC+step/86400;
	Aux3.Mjd_UTC = Aux3.Mjd_UTC+step/86400;
end

% Output
fprintf('\nTime since epoch [s]\n\n');
fprintf('%12.2f\n\n', t);
fprintf('State transition matrix (reference)\n\n');
for i=1:6
    fprintf('%12.5f', Phi1(i,:));
    fprintf('\n');
end
fprintf('\n');
fprintf('J2 State transition matrix\n\n');
for i=1:6
    fprintf('%12.5f', Phi2(i,:));
    fprintf('\n');
end
fprintf('\n');
fprintf('Keplerian State transition matrix\n\n');
for i=1:6
    fprintf('%12.5f', Phi3(i,:));
    fprintf('\n');
end

