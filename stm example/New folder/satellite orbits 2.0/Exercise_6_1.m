%--------------------------------------------------------------------------
%
%   Exercise 6-1: Light Time Iteration
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

SAT_Const

% read Earth orientation parameters
fid = fopen('eop19620101.txt','r');
%  ----------------------------------------------------------------------------------------------------
% |  Date    MJD      x         y       UT1-UTC      LOD       dPsi    dEpsilon     dX        dY    DAT
% |(0h UTC)           "         "          s          s          "        "          "         "     s 
%  ----------------------------------------------------------------------------------------------------
eopdata = fscanf(fid,'%i %d %d %i %f %f %f %f %f %f %f %f %i',[13 inf]);
fclose(fid);

% Ground station
lon_Sta = 11*const.Rad;        % [rad]
lat_Sta = 48*const.Rad;        % [rad]
h_Sta   =  0;                  % [m]

% Spacecraft orbit
MJD_Epoch = Mjday(1997,01,01); % Epoch

a     = 960e3 + const.R_Earth; % Semimajor axis [m]
e     =   0;                   % Eccentricity
i     =  97*const.Rad;         % Inclination [rad]
Omega = 130.7*const.Rad;       % RA ascend. node [rad]
omega =   0;                   % Argument of latitude [rad]
M0    =   0;                   % Mean anomaly at epoch [rad]

Kep = [a,e,i,Omega,omega,M0];  % Keplerian elements

% Light time iteration 
I_max = 3;                     % Maxim. number of iterations

% Station
R_Sta = Position(lon_Sta, lat_Sta, h_Sta); % Geocentric position vector

% Header
fprintf('Exercise 6-1: Light time iteration\n\n');
fprintf('   Date         UTC        Distance      ');
fprintf('Down It 1      It 2      Up It 1    Range\n');
fprintf('yyyy/mm/dd  hh:mm:ss.sss       [m]        ');
fprintf('   [m]        [mm]         [m]       [m] \n');

% Orbit
for Step = 0:6
    % Ground-received time
    t = 360 + 180*Step;                          % Time since epoch [s]
    MJD_UTC = MJD_Epoch + t/86400;               % Modified Julian Date [UTC]
    [x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,MJD_UTC,'l');
    MJD_UT1 = MJD_UTC + UT1_UTC/86400;
    U = R_z(gmst(MJD_UT1));                      % Earth rotation matrix
    r_Sta = U'*R_Sta';                           % Inertial station position
    
    % Light time iteration for downleg satellite -> station
    tau_down = 0;
    for Iteration = 1:I_max
        Y = State(const.GM_Earth,Kep,t-tau_down);
        r = Y(1:3);                              % Spacecraft position
        rho = norm(r-r_Sta);                     % Downleg range
        tau_down = rho/const.c_light;            % Downleg light time
        rho_down(Iteration) = rho;
    end
    
    % Light time iteration for upleg station -> satellite
    tau_up = 0;
    for Iteration = 1:I_max
        U = R_z(gmst(MJD_UT1-(tau_down+tau_up)/86400));
        r_Sta = U'*R_Sta';                       % Inertial station pos.
        rho = norm(r-r_Sta);                     % at ground transmit time
        tau_up = rho/const.c_light;              % Upleg light time
        rho_up(Iteration) = rho;
    end
    
    % Two-way range
    range = 0.5 * ( rho_down(I_max) + rho_up(I_max) );
    
    [year,mon,day,hr,min,sec] = invjday(MJD_UTC+2400000.5);
    fprintf('%4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
    fprintf('%13.1f', rho_down(1))                   % Geometric range
    fprintf('%11.1f', rho_down(2)-rho_down(1));
    fprintf('%12.1f', (rho_down(3)-rho_down(2))*1000);
    fprintf('%12.1f', rho_up(2)-rho_up(1));
    fprintf('%13.1f\n', range);    
end

