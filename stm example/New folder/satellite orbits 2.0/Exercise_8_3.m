%--------------------------------------------------------------------------
%
%   Exercise 8-3: Orbit Determination using Extended Kalman Filter
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

global AuxParam Cnm Snm eopdata PC const

SAT_Const
load DE430Coeff.mat
PC = DE430Coeff;

% read Earth gravity field coefficients
Cnm = zeros(181,181);
Snm = zeros(181,181);
fid = fopen('GGM03S.txt','r');
for n=0:180
    for m=0:n
        temp = fscanf(fid,'%d %d %f %f %f %f',[6 1]);
        Cnm(n+1,m+1) = temp(3);
        Snm(n+1,m+1) = temp(4);
    end
end
fclose(fid);

% read Earth orientation parameters
fid = fopen('eop19620101.txt','r');
%  ----------------------------------------------------------------------------------------------------
% |  Date    MJD      x         y       UT1-UTC      LOD       dPsi    dEpsilon     dX        dY    DAT
% |(0h UTC)           "         "          s          s          "        "          "         "     s 
%  ----------------------------------------------------------------------------------------------------
eopdata = fscanf(fid,'%i %d %d %i %f %f %f %f %f %f %f %f %i',[13 inf]);
fclose(fid);

% model parameters
AuxParam = struct ('Mjd_UTC',0,'area_solar',0,'area_drag',0,'mass',0,'Cr',0,...
                   'Cd',0,'n',0,'m',0,'sun',0,'moon',0,'sRad',0,'drag',0,...
                   'planets',0,'SolidEarthTides',0,'OceanTides',0,...
                   'Relativity',0,'n_a',0,'m_a',0,'n_G',0,'m_G',0);

n_obs = 6;
step  = 0.1;
Null3D = [0,0,0];
sigma_range = 10;             % [m]
sigma_angle = 0.01*const.Rad; % [rad] (0.01 deg = 36")

% Ground station
R = [1344e3,6069e3,1429e3]';  % [m] Bangalore

[lon,lat,h] = Geodetic(R);
E = LTCMatrix(lon,lat);                                   

% generation of artificial observations from given epoch state 
MJD0 = Mjday(1995,03,30,00,00,0.0); % epoch (UTC)

AuxParam.Mjd_UTC    = MJD0;
AuxParam.area_solar = 1.32*0.81; % [m^2]
AuxParam.area_drag  = 1.32*0.81; % [m^2]
AuxParam.mass       = 346; % [kg]
AuxParam.Cr         = 1.0;
AuxParam.Cd         = 2.0;
AuxParam.n          = 10;
AuxParam.m          = 10;
AuxParam.n_a        = 10;
AuxParam.m_a        = 10;
AuxParam.n_G        = 10;
AuxParam.m_G        = 10;
AuxParam.sun        = 1;
AuxParam.moon       = 1;
AuxParam.sRad       = 0;
AuxParam.drag       = 0;
AuxParam.planets    = 0;
AuxParam.SolidEarthTides = 1;
AuxParam.OceanTides = 0;
AuxParam.Relativity = 0;

Y0_true = [-6345.000e3, -3723.000e3,  -580.000e3, ... % [m]
           +2.169000e3, -9.266000e3, -1.079000e3 ]';  % [m/s]

fprintf('Exercise 8-3: Sequential orbit determination\n\n');
fprintf('Measurements\n\n');
fprintf('     Date          UTC        Az[deg]    El[deg]   Range[km]\n');

options = rdpset('RelTol',1e-13,'AbsTol',1e-16);

for i=1:n_obs
    % Time increment and propagation
    t       = i*step;                               % Time since epoch [s]
    MJD_UTC = MJD0 + t/86400;                       % Modified Julian Date
    [x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,MJD_UTC,'l');
    MJD_UT1 = MJD_UTC + UT1_UTC/86400;
    AuxParam.Mjd_UTC = MJD_UTC;
        [~,yout] = radau(@Accel,[0 t],Y0_true,options); % State vector
%     [~,yout] = radau(@Accel,[0 t],Y0_true,options); % State vector
    
    Y = yout(end,:)';
    
    % Topocentric coordinates
    theta = gmst(MJD_UT1);           % Earth rotation
    U = R_z(theta);
    r = Y(1:3);
    s = E*(U*r-R);                   % Topocentric position [m]
    [Azim,Elev] = AzEl(s);           % Azimuth, Elevation
    Dist = norm(s);                  % Range
    
    % Observation record
    Obs(i,1) = MJD_UTC;
    Obs(i,2) = Azim;
    Obs(i,3) = Elev;
    Obs(i,4) = Dist;
    [year,mon,day,hr,min,sec] = invjday(MJD_UTC+2400000.5);
    fprintf('  %4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year, mon, day, hr, min, sec);
    fprintf(' %10.3f%10.3f%13.3f\n', Obs(i,2)*const.Deg,Obs(i,3)*const.Deg,Obs(i,4)/1000);
end

% Orbit determination
fprintf('\nState errors\n\n');
fprintf('                                         Pos[m]                 Vel[m/s]    \n'); 
fprintf('     Date          UTC    Upd.     Error       Sigma       Error       Sigma\n');

% Initialization
MJD0 = Mjday(1995,03,30,00,00,0.0);       % Epoch (UTC)
t = 0;
Y = Y0_true + [10e3,-5e3,1e3,-1,3,-0.5]';
P = zeros(6);
for i=1:3
    P(i,i) = 1e8;
end
for i=4:6
    P(i,i) = 1e2;
end

yPhi = zeros(42,1);
Phi  = zeros(6);

% Measurement loop
for i=1:n_obs
    % Previous step
    t_old = t;
    Y_old = Y;
    
    % Propagation to measurement epoch
    MJD_UTC = Obs(i,1);                              % Modified Julian Date
    t      = (MJD_UTC-MJD0)*86400;                   % Time since epoch [s]
    [x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,MJD_UTC,'l');
    MJD_UT1 = MJD_UTC + UT1_UTC/86400;
    AuxParam.Mjd_UTC = MJD_UTC;    
    [~,yout] = radau(@Accel,[0 t-t_old],Y_old,options); % State vector
%     [~,yout] = ode15s(@Accel,[0 t-t_old],Y_old,options); % State vector
    Y = yout(end,:)';
    
    for ii=1:6
        yPhi(ii) = Y_old(ii);
        for j=1:6  
            if (ii==j) 
                yPhi(6*j+ii) = 1; 
            else
                yPhi(6*j+ii) = 0;
            end
        end
    end
    
    yPhi = DEInteg(@VarEqn,0,t-t_old,1e-13,1e-6,42,yPhi);
    
    % Extract state transition matrices
    for j=1:6
        Phi(:,j) = yPhi(6*j+1:6*j+6);
    end
    
    theta = gmst(MJD_UT1); % Earth rotation
    U = R_z(theta);
    
    % Time update
    P = TimeUpdate(P, Phi);
    
    % Truth orbit
    [~,yout] = radau(@Accel,[0 t],Y0_true,options); % State vector
    Y_true = yout(end,:)';
    
    for ii=1:6
        yPhi(ii) = Y0_true(ii);
        for j=1:6
            if (ii==j)
                yPhi(6*j+ii) = 1;
            else
                yPhi(6*j+ii) = 0;
            end
        end
    end
    
    yPhi = DEInteg(@VarEqn,0,t,1e-13,1e-6,42,yPhi);
    
    % Extract state transition matrices
    for j=1:6
        Phi(:,j) = yPhi(6*j+1:6*j+6);
    end
    
    % State error and standard deviation
    ErrY = Y-Y_true;
    SigY = EKFStdDev(P,n_obs);
    
    [year,mon,day,hr,min,sec] = invjday(MJD_UTC+2400000.5);
    fprintf('%4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
    fprintf('  t    ');
    fprintf('%10.1f', norm(ErrY(1:3)));
    fprintf('%12.1f', norm(SigY(1:3)));
    fprintf('%12.4f', norm(ErrY(4:6)));
    fprintf('%12.4f\n', norm(SigY(4:6)));
    
    % Azimuth and partials
    r = Y(1:3);
    s = E*(U*r-R);                        % Topocentric position [m]
    [Azim, Elev, dAds, dEds] = AzElPa(s); % Azimuth, Elevation
    dAdY = [dAds*E*U,Null3D];
    
    % Measurement update
    [K,Y,P] = MeasUpdate(Y,Obs(i,2),Azim,sigma_angle/cos(Elev),dAdY,P,n_obs);
    ErrY = Y-Y_true;
    SigY = EKFStdDev(P,n_obs);
    
    fprintf('                          Az   ');
    fprintf('%10.1f', norm(ErrY(1:3)));
    fprintf('%12.1f', norm(SigY(1:3)));
    fprintf('%12.4f', norm(ErrY(4:6)));
    fprintf('%12.4f\n', norm(SigY(4:6)));
    
    % Elevation and partials
    r = Y(1:3);
    s = E*(U*r-R);                          % Topocentric position [m]
    [Azim, Elev, dAds, dEds] = AzElPa(s);   % Azimuth, Elevation
    dEdY = [dEds*E*U,Null3D];
    
    % Measurement update
    [K,Y,P] = MeasUpdate(Y,Obs(i,3),Elev,sigma_angle,dEdY,P,n_obs);
    ErrY = Y-Y_true;
    SigY = EKFStdDev(P,n_obs);
    
    fprintf('                          El   ');
    fprintf('%10.1f', norm(ErrY(1:3)));
    fprintf('%12.1f', norm(SigY(1:3)));
    fprintf('%12.4f', norm(ErrY(4:6)));
    fprintf('%12.4f\n', norm(SigY(4:6)));
    
    % Range and partials
    r = Y(1:3);
    s = E*(U*r-R);                          % Topocentric position [m]
    Dist = norm(s); dDds = (s/Dist)';       % Range
    dDdY = [dDds*E*U,Null3D];
    
    % Measurement update
    [K,Y,P] = MeasUpdate(Y,Obs(i,4),Dist,sigma_range,dDdY,P,n_obs);
    ErrY = Y-Y_true;
    SigY = EKFStdDev(P,n_obs);
    
    fprintf('                          rho   ');
    fprintf('%9.1f', norm(ErrY(1:3)));
    fprintf('%12.1f', norm(SigY(1:3)));
    fprintf('%12.4f', norm(ErrY(4:6)));
    fprintf('%12.4f\n', norm(SigY(4:6)));
end

