%--------------------------------------------------------------------------
%
% f_Kep6D: Computes the derivative of the state vector for the normalized
%          (GM=1) Kepler's problem in three dimensions
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
function yp = f_Kep6D(x, y)

global nCalls t

% State vector derivative
r = y(1:3);
v = y(4:6);
yp = [v;-r/((norm(r))^3)];

% Write current time, step size and radius; store time for next step
if ((x-t)>1e-10)
    nCalls = nCalls+1;
    fprintf('%5i', nCalls);
    fprintf('%12.6f', x);
    fprintf('%12.6f', x-t);
    fprintf('%10.3f', norm(r));
    fprintf('\n');
    t = x;
end

