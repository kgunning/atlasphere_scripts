%--------------------------------------------------------------------------
%
%   Exercise 8-2: Least-squares orbit determination
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

global const PC R d AuxParam Cnm Snm eopdata

SAT_Const
load DE430Coeff.mat
PC = DE430Coeff;

% read Earth gravity field coefficients
Cnm = zeros(181,181);
Snm = zeros(181,181);
fid = fopen('GGM03S.txt','r');
for n=0:180
    for m=0:n
        temp = fscanf(fid,'%d %d %f %f %f %f',[6 1]);
        Cnm(n+1,m+1) = temp(3);
        Snm(n+1,m+1) = temp(4);
    end
end
fclose(fid);

% model parameters
AuxParam = struct ('Mjd_UTC',0,'area_solar',0,'area_drag',0,'mass',0,'Cr',0,...
                   'Cd',0,'n',0,'m',0,'sun',0,'moon',0,'sRad',0,'drag',0,...
                   'planets',0,'SolidEarthTides',0,'OceanTides',0,...
                   'Relativity',0,'n_a',0,'m_a',0,'n_G',0,'m_G',0);

% read Earth orientation parameters
fid = fopen('eop19620101.txt','r');
%  ----------------------------------------------------------------------------------------------------
% |  Date    MJD      x         y       UT1-UTC      LOD       dPsi    dEpsilon     dX        dY    DAT
% |(0h UTC)           "         "          s          s          "        "          "         "     s 
%  ----------------------------------------------------------------------------------------------------
eopdata = fscanf(fid,'%i %d %d %i %f %f %f %f %f %f %f %f %i',[13 inf]);
fclose(fid);

n_obs = 6;
Obs = zeros(n_obs,4);
step  = 1200;

null3d = [0,0,0];

sigma_range = 10;             % [m]
sigma_angle = 0.01*const.Rad; % [rad] (=36")

Label =  ['x','y','z'];

% Ground station
Rs = [1344e3,6069e3,1429e3]'; % [m]

[lon,lat,h] = Geodetic(Rs);

E = LTCMatrix(lon,lat);

% generation of artificial observations from given epoch state
MJD0 = Mjday(1995, 3, 30, 0, 0, 0); % epoch (UTC)

AuxParam.Mjd_UTC    = MJD0;
AuxParam.area_solar = 1.32*0.81; % [m^2]
AuxParam.area_drag  = 1.32*0.81; % [m^2]
AuxParam.mass       = 346;  % [kg]
AuxParam.Cr         = 1.0;
AuxParam.Cd         = 2.0;
AuxParam.n          = 10;
AuxParam.m          = 10;
AuxParam.n_a        = 10;
AuxParam.m_a        = 10;
AuxParam.n_G        = 10;
AuxParam.m_G        = 10;
AuxParam.sun        = 1;
AuxParam.moon       = 1;
AuxParam.sRad       = 0;
AuxParam.drag       = 0;
AuxParam.planets    = 0;
AuxParam.SolidEarthTides = 1;
AuxParam.OceanTides = 0;
AuxParam.Relativity = 0;

Y0_ref = [-6345.000e3, -3723.000e3,  -580.000e3, ... % [m]
          +2.169000e3, -9.266000e3, -1.079000e3 ]';  % [m/s]

fprintf('Exercise 8-2: Least-squares orbit determination\n\n');
fprintf('Measurements \n\n');
fprintf('     Date          UTC       Az(deg)    El(deg)   Range(km)\n');

options = rdpset('RelTol',1e-13,'AbsTol',1e-16);

for i=1:n_obs
    % Time increment and propagation
    t       = i*step;                              % Time since epoch [s]
    MJD_UTC = MJD0 + t/86400;                      % Modified Julian Date
    [x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,MJD_UTC,'l');
    MJD_UT1 = MJD_UTC + UT1_UTC/86400;
    AuxParam.Mjd_UTC = MJD_UTC;        
    [~,yout] = radau(@Accel,[0 t],Y0_ref,options); % State vector
    Y = yout(end,:)';
    
    % Topocentric coordinates
    theta = gmst(MJD_UT1);    % Earth rotation
    U = R_z(theta);
    r = Y(1:3);
    s = E*(U*r-Rs);            % Topocentric position [m]
    [Azim,Elev] = AzEl(s);     % Azimuth, Elevation
    Dist = norm(s);            % Range
    
    % Observation record
    Obs(i,1) = MJD_UTC;
    Obs(i,2) = Azim;
    Obs(i,3) = Elev;
    Obs(i,4) = Dist;
    
    [year,mon,day,hr,min,sec] = invjday(MJD_UTC+2400000.5);
    fprintf('  %4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
    fprintf('%10.3f%10.3f%13.3f \n', Obs(i,2)*const.Deg,Obs(i,3)*const.Deg,Obs(i,4)/1000);
end

% Orbit determination
A = zeros(n_obs*3,6);
b = zeros(n_obs*3,1);
w = zeros(n_obs*3,n_obs*3);

MJD0   = Mjday(1995, 3, 30, 0, 0,0); % epoch (UTC)

Y0_apr = Y0_ref + [10e3,-5e3,1e3,-1,3,-0.5]';
Y0     = Y0_apr;

yPhi  = zeros(42,1);
dYdY0 = zeros(6);

% Iteration
for iterat = 1:3
    fprintf('\nIteration Nr. %d \n', iterat);
    fprintf('\nResiduals: \n\n');
    fprintf('     Date          UTC          Az(deg)    El(deg)   Range(m)\n');
    
    R = zeros(6);
    d = zeros(6,1);
    
    for i=1:n_obs
        % Time increment and propagation
        MJD_UTC = Obs(i,1);                        % Modified Julian Date
        t      = (MJD_UTC-MJD0)*86400;             % Time since epoch [s]
        [x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,MJD_UTC,'l');
        MJD_UT1 = MJD_UTC + UT1_UTC/86400;
        AuxParam.Mjd_UTC = MJD_UTC;
        [~,yout] = radau(@Accel,[0 t],Y0,options); % State vector
        Y = yout(end,:)';
        
        for ii=1:6
            yPhi(ii) = Y0(ii);
            for j=1:6  
                if (ii==j) 
                    yPhi(6*j+ii) = 1; 
                else
                    yPhi(6*j+ii) = 0;
                end
            end
        end
        
        yPhi = DEInteg(@VarEqn,0,t,1e-13,1e-6,42,yPhi);
        
        % Extract state transition matrices
        for j=1:6
            dYdY0(:,j) = yPhi(6*j+1:6*j+6);
        end
        
        % Topocentric coordinates
        theta  = gmst(MJD_UT1); % Earth rotation
        U = R_z(theta);
        r = Y(1:3);
        s = E*(U*r-Rs);         % Topocentric position [m]
        
        % Observations and partials
        [Azim, Elev, dAds, dEds] = AzElPa(s); % Azimuth, Elevation
        Dist = norm(s);                       % Range
        dDds = (s/Dist)';
        
        dAdY0 = [dAds*E*U,zeros(1,3)]*dYdY0;
        dEdY0 = [dEds*E*U,zeros(1,3)]*dYdY0;
        dDdY0 = [dDds*E*U,zeros(1,3)]*dYdY0;
        
        % Accumulate least-squares system
        LSQAccumulate(6,dAdY0,(Obs(i,2)-Azim),sigma_angle/cos(Elev));
        LSQAccumulate(6,dEdY0,(Obs(i,3)-Elev),sigma_angle);
        LSQAccumulate(6,dDdY0,(Obs(i,4)-Dist),sigma_range);
        
        [year,mon,day,hr,min,sec] = invjday(MJD_UTC+2400000.5);
        fprintf('  %4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
        fprintf(' %10.3f', (Obs(i,2)-Azim)*const.Deg);
        fprintf(' %10.3f', (Obs(i,3)-Elev)*const.Deg);
        fprintf(' %11.1f\n', Obs(i,4)-Dist);
    end
    
    % Solve least-squares system
    dY0 = LSQSolve(6, R, d);
    SigY0 = LSQStdDev(R, 6);
    
    fprintf('\n Correction: \n\n');
    fprintf(' Pos ');
    fprintf('%8.1f%8.1f%8.1f',dY0(1),dY0(2),dY0(3));
    fprintf(' [m]');
    fprintf('\n Vel ');
    fprintf('%8.4f%8.4f%8.4f',dY0(4),dY0(5),dY0(6));
    fprintf(' [m/s]\n');
    
    % Correct epoch state
    Y0 = Y0 + dY0';
end

fprintf('\nSummary: \n');
fprintf('        a priori   correction      final        sigma\n');

for i=1:3
    fprintf('%s',Label(i));
    fprintf('[m]   ');
    fprintf('%11.1f%11.1f%14.1f%11.1f', Y0_apr(i),Y0(i)-Y0_apr(i),Y0(i),SigY0(i));
    fprintf('\n');
end
for i=4:6
    fprintf('v%s',Label(i-3));
    fprintf('[m/s]');
    fprintf('%11.4f%11.4f%14.4f%11.4f', Y0_apr(i),Y0(i)-Y0_apr(i),Y0(i),SigY0(i));
    fprintf('\n');
end

