%--------------------------------------------------------------------------
%
%   Exercise 5-1: Transformation from celestial to terrestrial reference
%                 system
%
% Last modified:   2018/01/27   M. Mahooti
%
%--------------------------------------------------------------------------
clc
clear
format long g

global const

SAT_Const

% read Earth orientation parameters
fid = fopen('eop19620101.txt','r');
%  ----------------------------------------------------------------------------------------------------
% |  Date    MJD      x         y       UT1-UTC      LOD       dPsi    dEpsilon     dX        dY    DAT
% |(0h UTC)           "         "          s          s          "        "          "         "     s 
%  ----------------------------------------------------------------------------------------------------
eopdata = fscanf(fid,'%i %d %d %i %f %f %f %f %f %f %f %f %i',[13 inf]);
fclose(fid);

% Header 
fprintf('Exercise 5-1: Transformation from celestial ');
fprintf('to terrestrial reference system\n\n');

% Date
MJD_UTC = Mjday(1999,03,04,0,0,0.0);

% Earth Orientation Parameters (UT1-UTC[s],UTC-TAI[s], x["], y["])
% (from IERS Bulletin B #135 and C #16; valid for 1999/03/04 0:00 UTC)
[x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,MJD_UTC,'l');
[UT1_TAI,UTC_GPS,UT1_GPS,TT_UTC,GPS_UTC] = timediff(UT1_UTC,TAI_UTC);
MJD_UT1 = MJD_UTC + UT1_UTC/86400;
MJD_TT  = MJD_UTC + TT_UTC/86400;

% IAU 1976 Precession
% (ICRF to mean equator and equinox of date)
P = PrecMatrix(const.MJD_J2000,MJD_TT);

% IAU 1980 Nutation
% (Transformation to the true equator and equinox)
N = NutMatrix(MJD_TT);

% Apparent Sidereal Time
% Rotation about the Celestial Ephemeris Pole
Theta = GHAMatrix(MJD_UT1);   % Note: here we evaluate the equation of the
                              % equinoxes with the MJD_UT1 time argument 
                              % (instead of MJD_TT)

% Polar motion
% (Transformation from the CEP to the IRP of the ITRS)
Pi = PoleMatrix(x_pole, y_pole);   % Note: the time argument of polar motion series
                                   % is not rigorously defined, but any differences
                                   % are negligible

% Output
fprintf('Date\n');
[year,mon,day,hr,min,sec] = invjday(MJD_UTC+2400000.5);
fprintf(' %4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
fprintf(' UTC\n');
[year,mon,day,hr,min,sec] = invjday(MJD_UT1+2400000.5);
fprintf(' %4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
fprintf(' UT1\n');
[year,mon,day,hr,min,sec] = invjday(MJD_TT+2400000.5);
fprintf(' %4d/%2.2d/%2.2d  %2.2d:%2.2d:%6.3f', year,mon,day,hr,min,sec);
fprintf(' TT\n\n\n');

fprintf('IAU 1976 Precession matrix (ICRS to tod)\n');
for i=1:3
    for j=1:3
        fprintf('%14.8f', P(i,j));
    end
    fprintf('\n');
end
fprintf('\n');

fprintf('IAU 1980 Nutation matrix (tod to mod)\n');
for i=1:3
    for j=1:3
        fprintf('%14.8f', N(i,j));
    end
    fprintf('\n');
end
fprintf('\n');

fprintf('Earth Rotation matrix\n');
for i=1:3
    for j=1:3
        fprintf('%14.8f', Theta(i,j));
    end
    fprintf('\n');
end
fprintf('\n');

fprintf('Polar motion matrix\n');
for i=1:3
    for j=1:3
        fprintf('%14.8f', Pi(i,j));
    end
    fprintf('\n');
end
fprintf('\n');

E = Pi*Theta*N*P;

fprintf('ICRS-ITRS transformation\n');
for i=1:3
    for j=1:3
        fprintf('%14.8f', E(i,j));
    end
    fprintf('\n');
end

