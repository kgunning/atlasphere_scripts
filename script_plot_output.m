


% Plotting outputs from POD sim

[filename,filepath] = uigetfile('', 'Select output file');

datai = load([filepath  filename]);

datai = datai.outStruc;

%% 
satIndPlot = 5;

%%
time = datai.time;
ylabels = {'x [m]','y [m]','z [m]'};
tPlot = (time-time(1))/60;

dPos = squeeze(datai.posErrEcef(satIndPlot,:,:))';
stdPosDiag = squeeze(datai.posStdEcef(satIndPlot,:,:))';
gsConnectFlag = datai.gsConnectFlag;

fig1 = figure; clf; 
for idx = 1:3
    subplot(3,1,idx); hold on;
    plot(tPlot,dPos(idx,:)')
    plot(tPlot(),stdPosDiag(idx,:)','k','linewidth',2)
    plot(tPlot(),-stdPosDiag(idx,:)','k','linewidth',2)
    
    stdPosNoConnect = stdPosDiag(idx,:);
    stdPosNoConnect(gsConnectFlag) = nan;
    
    plot(tPlot(),stdPosNoConnect','r.','linewidth',2)
    plot(tPlot(),-stdPosNoConnect','r.','linewidth',2)
    
    
    plot(tPlot(~gsConnectFlag),stdPosDiag(idx,~gsConnectFlag)','r.','linewidth',2)
    plot(tPlot(~gsConnectFlag),-stdPosDiag(idx,~gsConnectFlag)','r.','linewidth',2)
    
    grid on;
    ylabel(ylabels{idx})
    xlabel('Time of sim [min]')
    xlim([0 max(tPlot)])
end
legend('Pos error','Covariance')

%%
xlabels = {'Radial [m]','Along-track [m]','Cross-track [m]'};

fig1 = figure; 
fig1.Position(3:4) = [1140 420];
for idx = 1:3
    subplot(1,3,idx);
    histData = datai.posErrRac(:,:,idx);
    histData = histData(:);
    histData(isnan(histData)) = [];
    
    rmsi = sqrt(mean(histData.^2));
    
    hist(histData,100)
    if idx == 2
        title('Aggregate Raw Orbit Position Errors')
    end
    if idx == 1
        ylabel('Count')
    end

    ylims = ylim;
    xlims = xlim;
    
    text(xlims(1)+0.05*diff(xlims),ylims(2)-0.1*diff(ylims),['RMS error: ' num2str(rmsi,'% 2.2f') ' m'])
    
    xlabel(xlabels{idx})
    grid on
end

%% 
xlabels = {'x [rad]','y [rad]','z [rad]'};

fig1 = figure; 
fig1.Position(3:4) = [1140 420];
for idx = 1:3
    subplot(1,3,idx);
    histData = datai.attErr(:,:,idx);
    histData = histData(:);
    histData(isnan(histData)) = [];
    
    rmsi = sqrt(mean(histData.^2));
    
    hist(histData,100)
    if idx == 2
        title('Aggregate Raw Orbit Attitude Errors')
    end
    if idx == 1
        ylabel('Count')
    end

    ylims = ylim;
    xlims = xlim;
    
    text(xlims(1)+0.05*diff(xlims),ylims(2)-0.1*diff(ylims),['RMS error: ' num2str(rmsi,'% 2.2e') ' rad'])
    
    xlabel(xlabels{idx})
    grid on
end


