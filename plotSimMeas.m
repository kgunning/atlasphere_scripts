% plotting measurements

function plotSimMeas(t,world,measurementData)

indsPlot = 1:10:length(t);
tPlot = t(indsPlot);

nEntities = length(world.Entities);
nTimes = length(tPlot);

posEntities = nan(nEntities,nTimes,3);
entityUniqueIDs = [world.Entities.UniqueID];

for tdx = 1:length(tPlot)
    for idx = 1:length(world.Entities) 
         originData = world.getState(tPlot(tdx), world.Entities(idx).UniqueID, {'Position'});
         posEntities(idx,tdx,:) = originData.Position;
    end   
end

%%
[xEarth,yEarth,zEarth] = sphere;

rEarth = 6356*1000;
xEarth = xEarth*rEarth;
yEarth = yEarth*rEarth;
zEarth = zEarth*rEarth;

colori = permute([1 1 1],[1 3 2]);
cEarth = repmat(colori,size(xEarth,1),size(xEarth,2));

for tdx = 1:length(tPlot)
    
    posi = squeeze(posEntities(:,tdx,:));
    
    % draw each measurement
    measPlot = zeros(0,3);
    
    measDatai = measurementData{indsPlot(tdx)};
    
    for idx = 1:length(measDatai)
       measPosi = nan(3,3);
       % first row is self
       % second row is origin
       % third row is just NaNs to make plotting easier
       
       posSelf = squeeze(posEntities([entityUniqueIDs.TypeID] == [measDatai(idx).SelfUID.TypeID] & ...
           [entityUniqueIDs.EntityID] == [measDatai(idx).SelfUID.EntityID],tdx,:));
       
       posOrigin =  squeeze(posEntities([entityUniqueIDs.TypeID] == [measDatai(idx).OriginUID.TypeID] & ...
           [entityUniqueIDs.EntityID] == [measDatai(idx).OriginUID.EntityID],tdx,:));
       
        measPosi = [posSelf'; posOrigin'; nan(1,3)];
        
        measPlot = [measPlot; measPosi];
    end
    figure(1); clf; hold on;    
    plot3(posi(:,1),posi(:,2),posi(:,3),'o','linewidth',2)
    plot3(measPlot(:,1),measPlot(:,2),measPlot(:,3),'linewidth',2)
    surf(xEarth,yEarth,zEarth,cEarth)
    
    axis([   -10000000    10000000    -10000000     10000000   -10000000    10000000])
%     axis

    view(45+71, 18)
    
    drawnow
end

end