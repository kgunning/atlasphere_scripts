%% toy problem to think about covariances

%% just two objects in 1d- range between them
if 0 
% just static
cov0 = 0.1;
qPos = 0.1;
covMeas = 0.1;

nObj = 2;
nEpochs = 100;
nDim = 1;

% build a vector of true states
truePos = nan(nEpochs,nObj,nDim);
truePos(1,:,:) = 0;

truePos = cumsum(randn(nEpochs,nObj,nDim)*qPos,1);
truePos(:,2) = truePos(:,2)+0;
truePos(:,1) = truePos(:,1)-0;
meas = permute(diff(permute(truePos,[2 1 3])),[2 1 3]);


nStates = nObj*nDim;

estPos = nan(nStates,nEpochs);
covPos = zeros(nStates,nStates,nEpochs);
% initialize the position
for odx = 1:nObj
    indsi = ((odx-1)*nDim+1):(odx*nDim);
    estPos(indsi,1) = truePos(1,odx,:)+randn*cov0;
    covPos(indsi,indsi,1) = eye(nDim)*cov0.^2;
end

for tdx = 2:nEpochs
    state = estPos(:,tdx-1);
    cov   = squeeze(covPos(:,:,tdx-1));
    measi = meas(tdx);
    
    %% time update assumes stationary
    Q = zeros(nStates,nStates);
    for odx = 1:nObj
        indsi = ((odx-1)*nDim+1):(odx*nDim);
        Q(indsi,indsi) = eye(nDim)*qPos.^2;
    end
    cov = cov+Q;
    
    %% measurement update
    H = [-1 1];
    r = covMeas.^2;
    
    % predict the measurement
    measPred = state(2)-state(1);
    
    y = measi-measPred;
    
    K = cov*H'*inv(H*cov*H'+r);
    state = state+K*y;
    cov = (eye(nStates)-K*H)*cov;
    
    %% save stuff
    estPos(:,tdx) = state;
    covPos(:,:,tdx) = cov;
end


% Plot the positions
if 1
    for tdx = 1:nEpochs
        figure(1); clf; hold on;
        
        if 1
            plot(truePos(tdx,1,1),0,'b.')
            plot(truePos(tdx,2,1),0,'r.')
            
            plot(estPos(1,tdx),0,'bo')
            plot(estPos(2,tdx),0,'ro')
            
            % covariance
            [xc,yc] = circle(estPos(1,tdx),0,2*covPos(1,1,tdx).^(1/2));
            plot(xc,yc,'b')
            
            [xc,yc] = circle(estPos(2,tdx),0,2*covPos(2,2,tdx).^(1/2));
            plot(xc,yc,'r')
            
            %     ylim([-5 5])
            xlim([-5 5])
            grid on
            axis equal
            drawnow;
            pause(0.025)
            
        else
            plot(truePos(tdx,1,1),truePos(tdx,2,1),'ko')
%             plot(truePos(tdx,2,1),0,'r.')
            
            plot(estPos(1,tdx),estPos(2,tdx),'r.')
            
            error_ellipse('C',squeeze(covPos(:,:,tdx)),'mu',estPos(:,tdx),'style','r','conf',0.95);

            xlim([-5 5])
            ylim([-5 5])
            grid on
            drawnow;
            pause(0.025)
            
        end
        
    end
    
end

end



%% ring of objects in 2d- just range
% just static
cov0 = 0.1;
qPos = 0.05;
covMeas = 0.1;

nObj = 6;
nEpochs = 100;
nDim = 2;

% build a vector of true states
truePos = zeros(nEpochs,nObj,nDim);
truePos(1,:,:) = 0;

truePos = cumsum(randn(nEpochs,nObj,nDim)*qPos,1);
% offset each one 
r0 = 3;
dAngle = 2*pi/nObj;
for odx = 1:nObj
   offseti = [r0*cos(dAngle*odx) r0*sin(dAngle*odx)];
   truePos(:,odx,:) = truePos(:,odx,:)+permute(offseti,[3 1 2]);
end

measInfo = [1:nObj;
circshift(1:nObj,1)];
nMeas = size(measInfo,2);

measFull = nan(nEpochs,nMeas);
for idx =1 :nMeas
   measFull(:,idx) = sqrt(sum((truePos(:,measInfo(1,idx),:)-truePos(:,measInfo(2,idx),:)).^2,3));
end

nStates = nObj*nDim;

estPos = nan(nStates,nEpochs);
covPos = zeros(nStates,nStates,nEpochs);
stateObjInds = nan(nStates,1);
% initialize the position
for odx = 1:nObj
    indsi = ((odx-1)*nDim+1):(odx*nDim);
    stateObjInds(indsi) = odx;
    estPos(indsi,1) = truePos(1,odx,:)+randn*cov0;
    covPos(indsi,indsi,1) = eye(nDim)*cov0.^2;
end


for tdx = 2:nEpochs
    state = estPos(:,tdx-1);
    cov   = squeeze(covPos(:,:,tdx-1));
    measi = measFull(tdx,:)';
    
    %% time update assumes stationary
    Q = zeros(nStates,nStates);
    for odx = 1:nObj
        indsi = ((odx-1)*nDim+1):(odx*nDim);
        Q(indsi,indsi) = eye(nDim)*qPos.^2;
    end
    cov = cov+Q;
    
    %% measurement update'
    H = zeros(nMeas,nStates);
    R = eye(nMeas)*covMeas.^2;
    
    measPred = nan(nMeas,1);
    
    for mdx = 1:nMeas
       pos1 = state(stateObjInds == measInfo(1,mdx));
       pos2 = state(stateObjInds == measInfo(2,mdx));
       measPred(mdx) = sqrt(sum((pos2-pos1).^2));
       
       los = (pos2-pos1)./norm(pos2-pos1);
       
       H(mdx,stateObjInds == measInfo(1,mdx)) = -los;
       H(mdx,stateObjInds == measInfo(2,mdx)) = los;
    end
    
    % predict the measurement
%     measPred = state(2)-state(1);
    
    y = measi-measPred;
    
    K = cov*H'*inv(H*cov*H'+R);
    state = state+K*y;
    cov = (eye(nStates)-K*H)*cov;
    
    %% save stuff
    estPos(:,tdx) = state;
    covPos(:,:,tdx) = cov;
end


% Plot the positions
if 1
    colors = 'ymcrgbwk';
    for tdx = 1:nEpochs
        figure(1); clf; hold on;
        
        for odx =1 :nObj
            plot(truePos(tdx,odx,1),truePos(tdx,odx,2),[colors(odx) '.'],'MarkerSize',10)
            
            estPosi = estPos(stateObjInds == odx,tdx);
            
            plot(estPosi(1),estPosi(2),[colors(odx) '*'],'MarkerSize',10)
            
            error_ellipse('C',squeeze(covPos(stateObjInds == odx,stateObjInds == odx,tdx)),...
            'mu',estPosi,'style',colors(odx),'conf',0.95);
        end
        
        for mdx = 1:nMeas
            plot([truePos(tdx,measInfo(1,mdx),1) truePos(tdx,measInfo(2,mdx),1)],...
                [truePos(tdx,measInfo(1,mdx),2) truePos(tdx,measInfo(2,mdx),2)],['k'])
        end
        
        ylim([-5 5])
        xlim([-5 5])
        
        grid on
        drawnow;
        pause(0.025)
    end
end


























