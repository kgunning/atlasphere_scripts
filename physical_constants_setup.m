%% PHYSICAL CONSTANTS
%
%       Written by:           Tyler Reid
%       Lab:                  Stanford GPS Lab
%       Project Start Date:   March 28, 2011
%       Last updated:         November 22, 2011
%
%% DESCRIPTION
%
% Loading this file enters all of the physical constants needed to perform
% all of the neccessary calculations into the global workspace.  All
% constants are given in SI units.
%
%% DEFINE CONSTANTS

global mu omega_e R_e J_2 Earth_E2 c mu_sun mu_moon...
    Earth_Orient_Date dUT1 LOD xp yp dPsi dEps AU2m...
    l m Clm Slm

% Earth's Mean Equitorial Radius
R_e     = 6.378136e6;        % [m]

% Earth Gravitational Parameter mu = G*M_earth
mu      = 3.986004415e14;      % [m^3/s^2]

% Sun Gravitational Parameter mu = G*M_earth
mu_sun  = 1.32712440019e20;    % [m^3/s^2]

% Moon Gravitational Parameter mu = G*M_earth
mu_moon = 4.9027779e12;        % [m^3/s^2]

% Mean Angular Velocity of the Earth
omega_e = 7.29211585530e-5;    % [rad/s]

% J_2 - second zonal harmonic
J_2 =      1.0826300e-3;        % [-]

% Earth's shape - eccentricity^2
Earth_E2 = 0.006694385000;      % [-]

% speed of light in a vacuum
c        = 299792458;           % [m/s]  

% conversion to astronomical units
AU2m = 149597870.7e3;         % [m/AU]

%% EARTH ORIENTATION DATA

% Earth orientation data:  [almanac data]
% from:
%http://www.iers.org/nn_11252/SharedDocs/MetaDaten/1378__FINALS__ALL__IAU19
%80.html

% excel data file is in the following form:
% [MJD;X;sigma_X;Y;sigma_Y;UT1-UTC;sigma_UT1-UTC;LOD;sigma_LOD;dPsi;sigma_dPsi;dEpsilon;sigma_dEpsilon;bulB/X;bulB/Y;bulB/UT1-UTC;bulB/dPsi;bulB/dEpsilon

% read file
% A = xlsread('finals.xls');
% 
% % assign variables
% Earth_Orient_Date = A(:,1);       % modified julian date
% dUT1              = A(:,6);       % difference between UTC&UT1: UT1 = UTC + DUT1 [sec]
% LOD               = A(:,8)/1000;  % excess length of day [sec]
% xp                = A(:,2);       % polar motion coefficient [arc sec]
% yp                = A(:,4);       % polar motion coefficient [arc sec]
% dPsi              = A(:,10)/1000; % delta psi correction to gcrf [arc sec]
% dEps              = A(:,12)/1000; % delta eps correction to gcrf [arc sec]
% 
% % to save memory, clear A
% clear A

%% EGM96 GRAVITY MODEL DATA

% read data file
egm96_data_file = importdata('egm96_to360.ascii');

% see read me file for more information, also see:
% http://cddis.nasa.gov/926/egm96/getit.html
l       = egm96_data_file(:,1);
m       = egm96_data_file(:,2);
Clm_bar = egm96_data_file(:,3); % normalize Cnm
Slm_bar = egm96_data_file(:,4); % normalize Snm

Clm = zeros(size(Clm_bar));
Slm = zeros(size(Slm_bar));

% un-normalize these coefficients
for i = 1:length(l)
   
    L = l(i);
    M = m(i);
    
    if M == 0
        k = 1;
    else
        k = 2;
    end
        
    conv = sqrt( factorial(L+M) / factorial(L-M) / (2*L+1) / k );
    
    Clm(i) = Clm_bar(i)/conv;
    Slm(i) = Slm_bar(i)/conv;
    
end


% to save space 
clear egm96_data_file
clear Clm_bar
clear Slm_bar

%% JPL PLANETARY EPHEMERIS DATA 

