% Just trying to get simple propagation working with real data
global Clm Slm l


if ~exist('sats','var')
    % ECI one sat
    dirname = 'C:\Users\Kaz\Documents\atlasphere data\ECI\';
    sats = atlas.tools.importGMATData(dirname);
    
    world = atlas.World([sats]);
    
    posTrueEci = sats.TruthData.Position.Data;
    velTrueEci = sats.TruthData.Velocity.Data;
    t = sats.TruthData.Position.Time;
    
end


Nepochs = length(t);

% try to rotate back to ECI?
% posTrueEci = nan(size(posTrueEcef));
% velTrueEci = nan(size(posTrueEcef));
% [posTrueEcef,velTrueEci] = atlas.tools.ecef2eci(t(:)',posTrueEcef(:,:)',velTrueEcef(:,:)');
% posTrueEci = posTrueEci';
% velTrueEci = velTrueEci';

if 0
    figure(1); clf; hold on;
    plot3(posTrueEcef(:,1),posTrueEcef(:,2),posTrueEcef(:,3))
    
    figure(2); clf; hold on;
    plot3(posTrueEci(:,1),posTrueEci(:,2),posTrueEci(:,3))
end

%%
% read data file
egm96_data_file = importdata('C:\Users\Kaz\Documents\atlasphere\atlasphere\+atlas\+internal\egm96_to360.ascii');
global mu omega_e R_e J_2 Earth_E2 c mu_sun mu_moon...
    Earth_Orient_Date dUT1 LOD xp yp dPsi dEps AU2m...
    l m Clm Slm

% Earth's Mean Equitorial Radius
R_e     = 6.378136e6;        % [m]

% Earth Gravitational Parameter mu = G*M_earth
mu      = 3.986004415e14;      % [m^3/s^2]

% Sun Gravitational Parameter mu = G*M_earth
mu_sun  = 1.32712440019e20;    % [m^3/s^2]

% Moon Gravitational Parameter mu = G*M_earth
mu_moon = 4.9027779e12;        % [m^3/s^2]

% Mean Angular Velocity of the Earth
omega_e = 7.29211585530e-5;    % [rad/s]

% J_2 - second zonal harmonic
J_2 =      1.0826300e-3;        % [-]

% Earth's shape - eccentricity^2
Earth_E2 = 0.006694385000;      % [-]

% speed of light in a vacuum
c        = 299792458;           % [m/s]  

% conversion to astronomical units
AU2m = 149597870.7e3;         % [m/AU]
% see read me file for more information, also see:
% http://cddis.nasa.gov/926/egm96/getit.html
l       = egm96_data_file(:,1);
m       = egm96_data_file(:,2);
Clm_bar = egm96_data_file(:,3); % normalize Cnm
Slm_bar = egm96_data_file(:,4); % normalize Snm

Clm = zeros(size(Clm_bar));
Slm = zeros(size(Slm_bar));

% un-normalize these coefficients
for i = 1:length(l)
   
    L = l(i);
    M = m(i);
    
    if M == 0
        k = 1;
    else
        k = 2;
    end
        
    conv = sqrt( factorial(L+M) / factorial(L-M) / (2*L+1) / k );
    
    Clm(i) = Clm_bar(i)/conv;
    Slm(i) = Slm_bar(i)/conv;
end


%%
% Simple propagation
pos0 = posTrueEci(1,:)';
vel0 = velTrueEci(1,:)';
dt = t(2)-t(1);
[~, posPropEci, velPropEci] = atlas.internal.generateSimpleCircleOrbit(pos0,vel0,t);

posPropEci = squeeze(posPropEci)';
velPropEci = squeeze(velPropEci)';

%% 
dPosEci = posPropEci-posTrueEci;
dVelEci = velPropEci-velTrueEci;

% figure(3); clf; hold on;
% plot3(posPropEci(:,1),posPropEci(:,2),posPropEci(:,3),'.')
% plot3(posTrueEci(:,1),posTrueEci(:,2),posTrueEci(:,3))



figure(4); clf; hold on;
plot((dPosEci))




