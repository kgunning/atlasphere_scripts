% Developing ECI2ECEF (and ECEF2ECI) for real data


if ~exist('sats','var')
    dirnameEcef = 'C:\Users\Kaz\Documents\atlasphere data\ECEF\';
    satsEcef = atlas.tools.importGMATData(dirnameEcef);
    
    posTrueEcef = satsEcef.TruthData.Position.Data;
    velTrueEcef = satsEcef.TruthData.Velocity.Data;
    t = satsEcef.TruthData.Position.Time;
    
    
    dirnameEci = 'C:\Users\Kaz\Documents\atlasphere data\ECI\';
    satsEci = atlas.tools.importGMATData(dirnameEci);
    
    posTrueEci = satsEci.TruthData.Position.Data;
    velTrueEci = satsEci.TruthData.Velocity.Data;
    t = satsEci.TruthData.Position.Time;
end


Nepochs = length(t);


%%
if 0
    figure(1); clf; hold on;
    plot3(posTrueEcef(:,1),posTrueEcef(:,2),posTrueEcef(:,3))
    plot3(posTrueEci(:,1),posTrueEci(:,2),posTrueEci(:,3))
end

% ok, now try to do the rotation lol
jd = juliandate(datetime(t,'ConvertFrom','posixtime'));

% rotate everything?
%     [posRotEci,velRotEci] = atlas.internal.ECEFtoECI(jd-59/86400,posTrueEcef',velTrueEcef');

[posRotEci, velRotEci] = atlas.internal.ecef2eciMICE(t,posTrueEcef,velTrueEcef);

%{
posRotEci = nan(size(posTrueEcef));
velRotEci = nan(size(posTrueEcef));

for idx = 1:10:length(jd)
    % time for SPICE
    et = cspice_str2et( datestr(datetime(t(idx),'ConvertFrom','posixtime'),'yyyy-mmm-dd HH:MM:SS' ))+0.4243;
    
    Ri = cspice_sxform('ITRF93','J2000',et);
    posVel = Ri*[posTrueEcef(idx,:)'; velTrueEcef(idx,:)'];
    posRotEci(idx,:) = posVel(1:3);
    velRotEci(idx,:) = posVel(4:6);    
end
%}

%Compare
dposEci = posRotEci-posTrueEci;
dvelEci = velRotEci-velTrueEci;

if 1
    figure(1); clf; hold on;
    %     plot3(posRotEci(:,1),posRotEci(:,2),posRotEci(:,3))
    %     plot3(posTrueEci(:,1),posTrueEci(:,2),posTrueEci(:,3))
    plot(dposEci,'.')
    %     ylim([-10 10])
    
    figure(2); clf; hold on;
    plot(dvelEci,'.')
end